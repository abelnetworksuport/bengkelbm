@extends("layouts.master")

@section("page-header")
    @component("layouts.partials.slots.page-header",
    [ 'icon' => 'icon-user-plus', 'title' => 'Work Order Sementara', 'subtitle' => 'Buat Work Order', 'breadcrumbs' => [
        'Module', 'WO Sementara', 'Buat Work Order'
    ] ])

    @endcomponent
@endsection

@section("js-initial-data")
    <script>
        window.__INITIAL_WORKORDER__ = '{!! addslashes(json_encode(['workOrder' => $workOrder ])) !!}';
    </script>
@endsection

@section("content")
    <work-order-create></work-order-create>
@endsection