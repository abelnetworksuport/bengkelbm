@extends("layouts.master")

@section("page-header")
    @component("layouts.partials.slots.page-header",
    [ 'icon' => 'icon-user-plus', 'title' => 'Work Order Sementara', 'subtitle' => 'Daftar Work Order Sementara', 'breadcrumbs' => [
        'Module', 'WO Sementara', 'Daftar WO Sementara'
    ] ])

    @endcomponent
@endsection

@section("content")
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Daftar Work Order Sementara</h5>
        </div>

        <grid-w-o-temp></grid-w-o-temp>
    </div>
@endsection