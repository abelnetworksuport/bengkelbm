<?php
/** @var TYPE_NAME $minutes_estimated */
$hour    = floor(($minutes_estimated / 60) % 60);
$minutes = $minutes_estimated % 60;
?>

@if ($minutes_estimated > 0)
    <a href="javascript:" class="badge badge-primary"
       data-content="{!! json_encode(["workOrderId" => $id, "data" => [ "hours" => $hour, "minutes" => $minutes]]) !!}">
        {{ $hour }} Jam, {{ $minutes }} Menit
    </a>
@elseif
    <a href="javascript:" class="badge badge-warning"
       data-content="{!! json_encode(["workOrderId" => $id]) !!}">
        BUAT ESTIMASI SELESAI
    </a>
@endif
                          