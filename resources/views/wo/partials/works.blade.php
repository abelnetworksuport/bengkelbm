<ul class="list-inline list-inline-condensed mb-0 mt-2 mt-sm-0">
    <li class="list-inline-item">
        <a href="javascript:" class="badge bg-pink badge-icon show-modal" data-work-order='{{ $id }}'
           title="Data Masalah Problem"><i class="icon-aid-kit"></i></a>
    </li>
    <li class="list-inline-item">
        <a href="javascript:" class="badge bg-warning badge-icon modal-services" data-work-order='{{ $id }}'
           title="Data Jasa & Service"><i class="icon-wrench3"></i></a>
    </li>
    <li class="list-inline-item">
        <a href="javascript:" class="badge bg-purple badge-icon modal-spareparts" data-work-order='{{ $id }}'
           title="Data Bahan & Sparepart"><i class="icon-puzzle3"></i></a>
    </li>
    <li class="list-inline-item">
        <a href="javascript:" class="badge bg-violet badge-icon modal-mechanics" data-work-order='{{ $id }}'
           title="Data Mekanik"><i class="icon-person"></i></a>
    </li>
</ul>