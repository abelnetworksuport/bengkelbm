@can("update work-order-status")
    <a href="javacript:" class="badge {{ $status->colour }} show-modal"
       data-content="{!! json_encode(["work_order_id" => $id, "status_id" => $status_id ]) !!}"
       data-event="work-order::status">{{ $status->name }}</a>
@endcan