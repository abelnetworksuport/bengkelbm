@extends("layouts.master")

@section("page-header")
    @component("layouts.partials.slots.page-header",
    [ 'icon' => 'icon-truck', 'title' => 'Tambah Kendaraan Baru', 'subtitle' => "Kendataan Baru untuk customer {$owner->fullname}", 'breadcrumbs' => [
        'Module', 'Kendaraan', 'Tambah Kendaraan Baru'
    ] ])

    @endcomponent
@endsection

@section("js-initial-data")
    <script>
        window.__INITIAL_STATE__ = '{!! addslashes(json_encode($owner)) !!}';
    </script>
@endsection

@section("content")
    <div class="card">
        <vehicle-add></vehicle-add>
    </div>
@endsection