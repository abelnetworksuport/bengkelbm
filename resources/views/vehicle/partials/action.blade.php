<div class="list-icons list-icons-extended">
    <a href="javascript:" class="badge badge-icon bg-violet vehicle-modal" data-vehicle="{{ $id }}">
        <i class="icon-pencil3"></i>
    </a>
    @can("add work-order")
        <a href="/work-order/add?vehicle={{ $id }}" class="badge badge-icon bg-warning" title="BUAT WORK ORDER BARU">
            <i class="icon-plus22"></i>
        </a>
    @endcan
</div>