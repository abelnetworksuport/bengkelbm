<ul class="list-inline list-inline-condensed mb-0 mt-2 mt-sm-0">
    <li class="list-inline-item">
        <a href="javascript:" class="text-default vehicle-modal" data-vehicle="{{ $id }}" title="Edit Data Kendaraan"><i class="icon-pencil3 text-blue-800"></i></a>
    </li>
    <li class="list-inline-item">
        <a href="/work-order/add?vehicle={{ $id }}" class="text-default" title="BUAT WORK ORDER BARU"><i class="icon-plus3 text-green-800"></i></a>
    </li>
</ul>