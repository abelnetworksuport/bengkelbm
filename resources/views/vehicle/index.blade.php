@extends("layouts.master")

@section("page-header")
    @component("layouts.partials.slots.page-header",
    [ 'icon' => 'icon-user-plus', 'title' => 'Data Kendaraan', 'subtitle' => 'Daftar Kendaraan', 'breadcrumbs' => [
        'Module', 'Kendaraan', 'Daftar Kendaraan'
    ] ])
    @endcomponent
@endsection

@section("content")
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Daftar Kendaraan</h5>

            <div class="header-elements">
                <a href="{{ route("employees.export") }}" class="btn bg-violet btn-labeled btn-labeled-left btn-sm"><b><i class="icon-file-excel"></i></b> Download Excel</a>
            </div>
        </div>

        <div class="card-body"></div>

        <vehicle-grid ajax="{{ route("vehicles.data") }}"></vehicle-grid>
    </div>
@endsection