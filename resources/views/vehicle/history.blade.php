@extends("layouts.master")

@section("content")
    <div class="timeline timeline-left">
        <div class="timeline-container">
            @foreach ($workOrders->workOrders as $workOrder)
                <div class="timeline-row">
                    <div class="timeline-icon">
                        <img src="{{ asset("assets/global/images/placeholders/placeholder.jpg") }}" alt="Point">
                    </div>

                    <div class="row">
                        <div class="col-lg-6">
                            <div class="card">
                                <div class="card-header header-elements-sm-inline">
                                    <h6 class="card-title">Keluhan/Masalah & Service</h6>
                                    <div class="header-elements">
                                        <span><i class="icon-checkmark-circle mr-2 text-success"></i> # WO: </span>
                                    </div>
                                </div>

                                <div class="card-body">
                                    <table class="table table-bordered table-striped">
                                        @foreach($workOrder->problems as $problem)
                                            <tr>
                                                <td>
                                                    <span class="font-weight-semibold">{{ $problem->name }}</span>
                                                    <div class="text-muted">Deskripsi: </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </table>

                                    <table class="table table-bordered table-striped">
                                        @foreach($workOrder->services as $service)
                                            <tr>
                                                <td>
                                                    <span class="font-weight-semibold">{{ $service->service->name }}</span>
                                                    <div class="text-muted">Deskripsi: </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </div>

                                <div class="card-footer bg-transparent d-sm-flex justify-content-sm-between align-items-sm-center border-top-0 pt-0 pb-3">
                                    <ul class="list-inline mb-0">
                                        <li class="list-inline-item"><a href="#" class="text-default"><i class="icon-user mr-2"></i> Service Advisor: </a></li>
                                        <li class="list-inline-item"><a href="#" class="text-default"><i class="icon-comment-discussion mr-2"></i> 71</a></li>
                                    </ul>

                                    <a href="#" class="d-inline-block text-default mt-2 mt-sm-0">Detail <i class="icon-arrow-right14 ml-2"></i></a>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="card">
                                <div class="card-header header-elements-sm-inline">
                                    <h6 class="card-title">Spare Parts/Bahan</h6>
                                    <div class="header-elements">
                                        <span><i class="icon-checkmark-circle mr-2 text-success"></i> 3 hours ago</span>
                                        <div class="list-icons ml-3">
                                            <div class="list-icons-item dropdown">
                                                <a href="#" class="list-icons-item caret-0 dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-arrow-down12"></i>
                                                </a>

                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Hide user posts</a>
                                                    <a href="#" class="dropdown-item"><i class="icon-user-block"></i> Block user</a>
                                                    <a href="#" class="dropdown-item"><i class="icon-user-minus"></i> Unfollow user</a>
                                                    <div class="dropdown-divider"></div>
                                                    <a href="#" class="dropdown-item"><i class="icon-embed"></i> Embed post</a>
                                                    <a href="#" class="dropdown-item"><i class="icon-blocked"></i> Report this post</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="card-body">
                                    <div class="card-img-actions mb-3">
                                        <img class="card-img img-fluid" src="../../../../global_assets/images/placeholders/cover.jpg" alt="">
                                        <div class="card-img-actions-overlay card-img">
                                            <a href="blog_single.html" class="btn btn-outline bg-white text-white border-white border-2 btn-icon rounded-round">
                                                <i class="icon-link"></i>
                                            </a>
                                        </div>
                                    </div>

                                    <h6 class="mb-3">
                                        <i class="icon-comment-discussion mr-2"></i>
                                        <a href="#">Melanie Watson</a> commented:
                                    </h6>

                                    <blockquote class="blockquote blockquote-bordered py-2 pl-3 mb-0">
                                        <p class="mb-2 font-size-base">Pernicious drooled tryingly over crud peaceful gosh yet much following brightly mallard hey gregariously far gosh until earthworm python some impala belched darn a sighed unicorn much changed and astride cat and burned grizzly when jeez wonderful the outside tedious.</p>
                                        <footer class="blockquote-footer">Melanie, <cite title="Source Title">12:56 am</cite></footer>
                                    </blockquote>
                                </div>

                                <div class="card-footer bg-transparent d-sm-flex justify-content-sm-between align-items-sm-center border-top-0 pt-0 pb-3">
                                    <ul class="list-inline mb-0">
                                        <li class="list-inline-item"><a href="#" class="text-default"><i class="icon-eye4 mr-2"></i> 438</a></li>
                                        <li class="list-inline-item"><a href="#" class="text-default"><i class="icon-comment-discussion mr-2"></i> 71</a></li>
                                    </ul>

                                    <a href="#" class="d-inline-block text-default mt-2 mt-sm-0">Read post <i class="icon-arrow-right14 ml-2"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection