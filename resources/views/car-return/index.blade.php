@extends("layouts.master")

@section("page-header")
    @component("layouts.partials.slots.page-header",
    [ 'icon' => 'icon-user-plus', 'title' => 'Tambah Car Return', 'subtitle' => 'Daftar Work Order', 'breadcrumbs' => [
        'Module', 'Car Return', 'Tambah Car Return'
    ] ])

    @endcomponent
@endsection

@section("content")
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Daftar Work Order Car Return</h5>
        </div>

        <work-order-grid ajax="{{ route("work-order.data") }}?car-return=1"></work-order-grid>
    </div>
@endsection