@extends("layouts.master")

@section("page-header")
    @component("layouts.partials.slots.page-header",
    [ 'icon' => 'icon-user-plus', 'title' => 'Tambah Car Return', 'subtitle' => 'Daftar Work Order', 'breadcrumbs' => [
        'Module', 'Car Return', 'Tambah Car Return'
    ] ])

    @endcomponent
@endsection

@section("content")
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Daftar Work Order</h5>
        </div>

        <div class="card-body">
            Anda dapat menambahkan Car Return dari Work Order yang telah selesai.
        </div>

        <grid-car-return ajax="{{ route("car-return.data") }}"></grid-car-return>
    </div>
@endsection