<?php
/** @var TYPE_NAME $minutes_estimated */
$hour    = floor(($minutes_estimated / 60) % 60);
$minutes = $minutes_estimated % 60;
?>

<a href='javascript:' class='badge badge-primary'>{{ $hour }} Jam, {{ $minutes }} Menit</a>
