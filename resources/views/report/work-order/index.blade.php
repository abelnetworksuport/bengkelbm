@extends("layouts.master")

@section("page-header")
    @component("layouts.partials.slots.page-header",
    [ 'icon' => 'icon-stats-bars2', 'title' => 'Report Work Order', 'subtitle' => 'Laporan', 'breadcrumbs' => [
        'Module', 'Report', 'Work Order'
    ] ])
    @endcomponent
@endsection


@push("scripts")
    <script src="{{ asset("assets/global/js/plugins/ui/moment/moment.min.js") }}"></script>
    <script src="{{ asset("assets/global/js/plugins/pickers/daterangepicker.js") }}"></script>
    <script src="{{ asset("assets/global/js/plugins/forms/selects/bootstrap_multiselect.js") }}"></script>
@endpush

@push("inline-scripts")
    <script>
        // Display week numbers
        $('.multiselect').multiselect({
            nonSelectedText: 'Service Advisor'
        });

        $('.daterange-weeknumbers').daterangepicker(
            {
                startDate: moment().subtract(29, 'days'),
                endDate: moment(),
                minDate: '01/01/2014',
                maxDate: '12/31/2019',
                dateLimit: { days: 60 },
                ranges: {
                    'Hari ini': [moment(), moment()],
                    'Kemarin': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    '7 Hari Terakhir': [moment().subtract(6, 'days'), moment()],
                    '30 Hari Terakhir': [moment().subtract(29, 'days'), moment()],
                    'Bulan ini': [moment().startOf('month'), moment().endOf('month')],
                    'Bulan Lalu': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
                opens: 'right',
                applyClass: 'btn-sm bg-slate',
                cancelClass: 'btn-sm btn-light'
            },

            function(start, end) {
                return start.format('D MMMM YYYY') + ' &nbsp; - &nbsp; ' + end.format('D MMMM YYYY');
            }
        );
    </script>
@endpush

@section('content')
    <div class="d-md-flex align-items-md-start">

        <!-- Left sidebar component -->
        <div class="sidebar sidebar-light bg-transparent sidebar-component sidebar-component-left border-0 shadow-0 sidebar-expand-md">
            <!-- Sidebar content -->
            <div class="sidebar-content">
                <!-- Filter -->
                <div class="card">
                    <div class="card-header bg-transparent header-elements-inline">
                        <span class="text-uppercase font-size-sm font-weight-semibold">Rentang Tanggal</span>
                        <div class="header-elements">
                            <div class="list-icons">
                                <a class="list-icons-item" data-action="collapse"></a>
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        <form action="{{ route('report::wo.data') }}" method="get">
                            <div class="form-group form-group-feedback form-group-feedback-left">
                                <input type="search" name="periods" class="form-control daterange-weeknumbers" placeholder="Rentang Tanggal WO">
                                <div class="form-control-feedback">
                                    <i class="icon-calendar text-muted"></i>
                                </div>
                            </div>

                            <div class="form-group form-group-feedback form-group-feedback-left">
                                <select class="form-control multiselect" name="service_advisors" multiple="multiple" data-fouc>
                                    @foreach($serviceAdvisors as $service_advisor)
                                        <option value="{{ $service_advisor->id }}">{{ $service_advisor->name }}</option>
                                    @endforeach
                                </select>
                                <div class="form-control-feedback">
                                    <i class="icon-users text-muted"></i>
                                </div>
                            </div>

                            <button type="submit" class="btn bg-blue btn-block">
                                <i class="icon-stats-bars font-size-base mr-2"></i>
                                Buat Laporan
                            </button>
                        </form>
                    </div>
                </div>
                <!-- /filter -->

            </div>
            <!-- /sidebar content -->
        </div>
        <!-- /left sidebar component -->
        <!-- Right content -->
        <div class="flex-fill overflow-auto">

        </div>
        <!-- /right content -->
    </div>
@endsection