<div class="list-icons list-icons-extended">
    <a href="javascript:" class="badge badge-icon bg-violet show-modal" data-url='{{ route('employees.edit', $id) }}' data-event="employee::edit"><i class="icon-pencil3"></i></a>
    <a href="javascript:" class="badge badge-icon bg-warning show-modal" data-content='{!! json_encode([
    'url' => route('employees.destroy', $id)
    ]) !!}' data-event="employee::remove"><i class="icon-trash"></i></a>
</div>