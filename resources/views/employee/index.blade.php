@extends("layouts.master")

@section("page-header")
    @component("layouts.partials.slots.page-header",
    [ 'icon' => 'icon-user-tie', 'title' => 'Data Karyawan', 'subtitle' => 'Daftar Daftar Karyawan', 'breadcrumbs' => [
        'Module', 'Karyawan', 'Daftar Karyawan'
    ] ])

        <a href="javascript:" class="btn btn-labeled btn-labeled-right bg-primary show-modal" data-event="employee::add">Tambah Karyawan <b><i class="icon-plus2"></i></b></a>
    @endcomponent
@endsection

@section("content")
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Daftar Karyawan</h5>
            <div class="header-elements">
                <a href="{{ route("employees.export") }}" class="btn bg-violet btn-labeled btn-labeled-left btn-sm"><b><i class="icon-file-excel"></i></b> Download Excel</a>
            </div>
        </div>

        <div class="card-body"></div>

        <grid-employees ajax="{{ route('employees.data') }}"></grid-employees>
    </div>
@endsection