@extends('layouts.master')

@section('title', 'Dashboard')

@section('page-header')
    @component("layouts.partials.slots.page-header", [
        'icon' => 'icon-home',
        'title' => 'Dashboard',
        'subtitle' => 'Dashboard ' . $currentUser->roles->pluck("name")[0],
        'breadcrumbs' => [
            'Module', 'Dasboard'
        ]
    ])
        @hasrole('Service Advisor (SA)')
            <header-search></header-search>
        @endhasrole
    @endcomponent
@endsection

@section('content')
    @foreach(array_chunk($widgets, 4) as $_widgets)
        <div class="row">
            @foreach($_widgets as $widget)
                <div class="col-md-3">
                    <widget
                            text="{{ $widget['name'] }}"
                            color="{{ $widget['colour'] }}"
                            total="{{ (int)$widget['total'] }}"
                            icon="{{ $widget['icon'] }}"
                            id="{{ $widget['id'] }}"
                    ></widget>
                </div>
            @endforeach
        </div>
    @endforeach

    @hasanyrole('Control Tower (CT)|Kepala Bengkel|Final Inspection (FI)|Sparepart|Admin Mekanik')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <dashboard-work-order ajax="{{ route('work-order.data') }}"></dashboard-work-order>
            </div>
        </div>
    </div>
    @else
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <dashboard-work-order ajax="{{ route('work-order.data') }}"></dashboard-work-order>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">
                        <h5>Top Brand Kendaraan</h5>
                    </div>
                    <div class="card-body">
                        <top-brand-vehicle :chart='{!! json_encode($topBrands) !!}'></top-brand-vehicle>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-5">
                <new-customer :customers='{!! json_encode($customers) !!}'></new-customer>
            </div>
            <div class="col-md-7">
                <div class="card">
                    <div class="card-body">
                        <total-vehicles-days></total-vehicles-days>
                    </div>
                </div>
            </div>
        </div>
    @endhasanyrole
@endsection