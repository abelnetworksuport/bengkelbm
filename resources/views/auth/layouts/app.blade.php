<!DOCTYPE html>
<html lang="en">
<head>
    <title>Login | Bengkel Bavaria Mercindo Motor</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="{{ asset("assets/icons/favicon.ico") }}"/>
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset("assets/icons/apple-touch-icon.png") }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset("assets/icons/favicon-32x32.png") }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset("assets/icons/favicon-16x16.png") }}">
    <link rel="manifest" href="{{ asset("assets/icons/site.webmanifest") }}">
    <link rel="mask-icon" href="{{ asset("assets/icons/safari-pinned-tab.svg") }}" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset("auth/vendor/bootstrap/css/bootstrap.min.css") }}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset("auth/fonts/font-awesome-4.7.0/css/font-awesome.min.css") }}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset("auth/fonts/iconic/css/material-design-iconic-font.min.css") }}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset("auth/vendor/animate/animate.css") }}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset("auth/vendor/css-hamburgers/hamburgers.min.css") }}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset("auth/vendor/animsition/css/animsition.min.css") }}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset("auth/css/util.css") }}">
    <link rel="stylesheet" type="text/css" href="{{ asset("auth/css/main.css") }}">
    <!--===============================================================================================-->
</head>
<body>

<div class="limiter" id="app">
    @yield("content")
</div>


<div id="dropDownSelect1"></div>

<!--===============================================================================================-->
<script src="{{ asset("auth/vendor/jquery/jquery-3.2.1.min.js") }}"></script>
<!--===============================================================================================-->
<script src="{{ asset("auth/vendor/animsition/js/animsition.min.js") }}"></script>
<!--===============================================================================================-->
<script src="{{ asset("auth/vendor/bootstrap/js/popper.js") }}"></script>
<script src="{{ asset("auth/vendor/bootstrap/js/bootstrap.min.js") }}"></script>
<!--===============================================================================================-->
<script src="https://cdn.jsdelivr.net/npm/vue"></script>
<script src="https://cdn.jsdelivr.net/npm/vee-validate@latest/dist/vee-validate.js"></script>

<script>
    Vue.use(VeeValidate);

    new Vue({
        el: '#app',
        methods: {
            handleSubmit (e) {
                this.$validator.validate().then(result => {
                    if (!result) {
                        e.preventDefault();
                        return false;
                    }
                    return true;
                });
            }
        }
    });
</script>
</body>
</html>