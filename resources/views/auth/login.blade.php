@extends('auth.layouts.app')

@section("content")
    <div class="container-login100" style="background-image: url('{{ asset("auth/images/bg-01.jpg") }}');">
        <div class="wrap-login100">
            <form method="post" class="login100-form" @submit="handleSubmit" autocomplete="off" action="{{ route('login') }}">
                @csrf
                <span class="login100-form-logo" style="background-color: transparent">
                    <img src="{{ asset("assets/global/images/logo_bnm.png") }}" style="width: 250px;"/>
                </span>

                <span class="login100-form-title p-b-34 p-t-27">
						Log in
					</span>

                <div class="wrap-input100 validate-input" :class="{'input': true, 'alert-validate': errors.has('username') }" data-validate = "Masukkan Username Anda">
                    <input class="input100" v-validate="'required'" type="text" name="username" placeholder="Username" value="{{ old('username') }}" autofocus>
                    <span class="focus-input100" data-placeholder="&#xf207;"></span>
                </div>

                <div class="wrap-input100 validate-input" data-validate="Masukkan Password Anda" :class="{'input': true, 'alert-validate': errors.has('password') }">
                    <input class="input100" type="password" v-validate="'required'" name="password" placeholder="Password">
                    <span class="focus-input100" data-placeholder="&#xf191;"></span>
                </div>

                <div class="contact100-form-checkbox">
                    <a class="txt1" href="#">
                        Lupa Password?
                    </a>
                </div>

                <div class="container-login100-form-btn">
                    <button class="login100-form-btn">
                        Login
                    </button>
                </div>
            </form>
        </div>
    </div>

@endsection
