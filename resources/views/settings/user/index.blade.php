@extends("layouts.master")

@section("page-header")
    @component("layouts.partials.slots.page-header",
   [
   'icon' => 'icon-user-plus',
   'title' => 'Users Management',
   'subtitle' => 'User Lists',
   'breadcrumbs' => [
       'Module', 'Settings', 'Users Management'
   ]])

        <a href="javascript:" class="btn btn-labeled btn-labeled-right bg-primary show-modal" data-event="user::add">Tambah User <b><i class="icon-plus2"></i></b></a>
    @endcomponent
@endsection

@section("content")
    <div class="card">
        <grid-users></grid-users>
    </div>
@endsection