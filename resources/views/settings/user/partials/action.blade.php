<div class="list-icons list-icons-extended">
    <a href="javascript:" class="badge badge-icon bg-violet show-modal" data-url='{{ route('users.show', $id) }}' data-event="user::edit"><i class="icon-pencil3"></i></a>
    <a href="javascript:" class="badge badge-icon bg-teal show-modal" data-url='{{ route('users.show', $id) }}' data-event="user::reset-password"><i class="icon-key"></i></a>
    <a href="javascript:" class="badge badge-icon bg-warning show-modal" data-content='{!! json_encode([
    'url' => route('users.destroy', $uuid)
    ]) !!}' data-event="users::remove"><i class="icon-trash"></i></a>
</div>