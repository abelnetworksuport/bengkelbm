@extends("layouts.master")

@section("page-header")
    @component("layouts.partials.slots.page-header",
   [
   'icon' => 'icon-user-plus',
   'title' => 'Roles Management',
   'subtitle' => 'Role Lists',
   'breadcrumbs' => [
       'Module', 'Settings', 'Role Management'
   ]])

        <a href="javascript:" class="btn btn-labeled btn-labeled-right bg-primary show-modal" data-event="roles::add">Tambah Role <b><i class="icon-plus2"></i></b></a>
    @endcomponent
@endsection

@section("content")
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Data Role/Level</h5>
        </div>

        <div class="card-body">
        </div>

        <grid-roles></grid-roles>
    </div>
@endsection

@push('top-inline-scripts')
    {{--<script>--}}
        {{--window.__URLS = {--}}
            {{--data: "{{ route('api:roles') }}",--}}
            {{--store: "{{ route('roles.store') }}",--}}
            {{--update : "{{ route('roles.update') }}",--}}
            {{--permissions: "{{ route('api:permissions') }}",--}}
            {{--modules: "{{ route('api:modules') }}"--}}
        {{--}--}}
    {{--</script>--}}
@endpush