<div class="list-icons list-icons-extended">
    <a href="javascript:" class="badge badge-icon bg-violet show-modal" data-url='{{ route('roles.show', $id) }}' data-event="roles::edit"><i class="icon-pencil3"></i></a>
    <a href="javascript:" class="badge badge-icon bg-warning show-modal" data-content='{!! json_encode([
    'url' => route('roles.destroy', $id)
    ]) !!}' data-event="roles::remove"><i class="icon-trash"></i></a>
</div>