<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="base-url" content="{{ url('/') }}">

    <title>@yield('title', "B&M Motor") | Bengkel Bavaria Mercindo Motor</title>

    <link rel="icon" type="image/png" href="{{ asset("assets/icons/favicon.ico") }}"/>
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset("assets/icons/apple-touch-icon.png") }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset("assets/icons/favicon-32x32.png") }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset("assets/icons/favicon-16x16.png") }}">
    <link rel="manifest" href="{{ asset("assets/icons/site.webmanifest") }}">
    <link rel="mask-icon" href="{{ asset("assets/icons/safari-pinned-tab.svg") }}" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

    <!-- Global stylesheets -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet"
          type="text/css">

    <link href="{{ asset("assets/css/preload.css") }}" rel="stylesheet" type="text/css">
    <link href="{{ asset("assets/global/css/icons/icomoon/styles.css") }}" rel="stylesheet" type="text/css">
    <link href="{{ asset("assets/css/bootstrap.min.css") }}" rel="stylesheet" type="text/css">
    <link href="{{ asset("assets/css/bootstrap_limitless.min.css") }}" rel="stylesheet" type="text/css">
    <link href="{{ asset("assets/css/layout.min.css") }}" rel="stylesheet" type="text/css">
    <link href="{{ asset("assets/css/components.min.css") }}" rel="stylesheet" type="text/css">
    <link href="{{ asset("assets/css/colors.min.css") }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/noty/3.1.4/noty.min.css">

    <style>
        .navbar-brand img {
            height: 2rem;
        }

        .navbar-brand {
            padding-top: 0.5rem;
            padding-bottom: 0;
        }

        .table-xxs td, .table-xxs th {
            padding: .3rem 1.25rem;
        }

        .multiselect-selected-text {
            padding-left: 2.10rem
        }

        .form-group-feedback-left .form-control-feedback {
            left: 0;
            z-index: 9999!important;
        }

        .dataTables_processing {
            position: absolute;
            border: 1px solid #dce1e2;
            border-radius: 10px;
            width: 100px;
            height: 40px;
            padding: 10px;
            text-align: center;
            left: 50%;
            margin-left: -50px;
            top: 50%;

            background: rgba(255, 255, 255, 0.5);
            background: -moz-linear-gradient(left, rgba(255, 255, 255, 0.5) 0%, rgba(246, 246, 246, 0.5) 47%, rgba(237, 237, 237, 0.5) 100%);
            background: -webkit-linear-gradient(left, rgba(255, 255, 255, 0.5) 0%, rgba(246, 246, 246, 0.5) 47%, rgba(237, 237, 237, 0.5) 100%);
            background: -o-linear-gradient(left, rgba(255, 255, 255, 0.5) 0%, rgba(246, 246, 246, 0.5) 47%, rgba(237, 237, 237, 0.5) 100%);
            background: -ms-linear-gradient(left, rgba(255, 255, 255, 0.5) 0%, rgba(246, 246, 246, 0.5) 47%, rgba(237, 237, 237, 0.5) 100%);
            background: linear-gradient(to right, rgba(255, 255, 255, 0.5) 0%, rgba(246, 246, 246, 0.5) 47%, rgba(237, 237, 237, 0.5) 100%);
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffff', endColorstr='#ededed', GradientType=1);
        }
    </style>

    <!-- /global stylesheets -->

    <script>
        window.__CURRENT_USER__    = '{!! addslashes(json_encode(auth()->user())) !!}';
        window.__INITIAL_OPTIONS__ = '{!! addslashes(json_encode(['options' => $initialOptions])) !!}';
        window.__ROLE              = '{!! addslashes(json_encode(Auth::user()->getAllPermissions()->pluck('name') )) !!}';
        window.__INITIAL_STATE__   = '{!! addslashes(json_encode(['app' => 1])) !!}';
        window.__INITIAL_WORKORDER__   = '{!! addslashes(json_encode(['app' => 1])) !!}';
    </script>

    @stack("top-inline-scripts")

    @yield("js-initial-data")
</head>

<body class="navbar-top sidebar-xs">

<div id="preloader">
    <div id="preloader_spinner">
        <div class="spinner"></div>
    </div>
</div>

<!-- Main navbar -->
<div class="navbar navbar-expand-md navbar-dark fixed-top">
    <div class="navbar-brand">
        <a href="{{ url("dashboard") }}" class="d-inline-block">
            <img src="{{ asset("assets/global/images/logo_dark@1x.png") }}" alt="">
        </a>
    </div>

    <div class="d-md-none">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
            <i class="icon-tree5"></i>
        </button>
        <button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
            <i class="icon-paragraph-justify3"></i>
        </button>
    </div>

    <div class="collapse navbar-collapse" id="navbar-mobile">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a href="#" class="navbar-nav-link sidebar-control sidebar-main-toggle d-none d-md-block">
                    <i class="icon-paragraph-justify3"></i>
                </a>
            </li>
        </ul>

        <ul class="navbar-nav ml-auto">
            <li class="nav-item dropdown">
                <a href="#" class="navbar-nav-link">
                    <i class="icon-bell2"></i>
                    <span class="d-md-none ml-2">Notifications</span>
                    <span class="badge badge-mark border-white ml-auto ml-md-0"></span>
                </a>
            </li>

            <li class="nav-item dropdown dropdown-user">
                <a href="#" class="navbar-nav-link d-flex align-items-center dropdown-toggle" data-toggle="dropdown">
                    <img src="{{ asset("assets/global/images/image.png") }}" class="rounded-circle mr-2" height="34"
                         alt="">
                    <span>{{ $currentUser->name }} ({{ $currentUser->roles->pluck("name")[0] }})</span>
                </a>

                <div class="dropdown-menu dropdown-menu-right">
                    <a href="{{ route('logout') }}" class="dropdown-item" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><i
                                class="icon-switch2"></i> Logout</a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </li>
        </ul>
    </div>
</div>
<!-- /main navbar -->

<!-- Page content -->
<div class="page-content" id="app">

    <!-- Main sidebar -->
    <div class="sidebar sidebar-dark sidebar-main sidebar-fixed sidebar-expand-md">

        <!-- Sidebar mobile toggler -->
        <div class="sidebar-mobile-toggler text-center">
            <a href="#" class="sidebar-mobile-main-toggle">
                <i class="icon-arrow-left8"></i>
            </a>
            Menu Navigasi
            <a href="#" class="sidebar-mobile-expand">
                <i class="icon-screen-full"></i>
                <i class="icon-screen-normal"></i>
            </a>
        </div>
        <!-- /sidebar mobile toggler -->


        <!-- Sidebar content -->
        <div class="sidebar-content">
            <!-- Main navigation -->
            <div class="card card-sidebar-mobile">
                {!! Menu::render(config('navigation.name')) !!}
            </div>
            <!-- /main navigation -->

        </div>
        <!-- /sidebar content -->

    </div>
    <!-- /main sidebar -->

    <!-- Main content -->
    <div class="content-wrapper">

    @yield("page-header")

    <!-- Content area -->
        <div class="content">
            @yield("content")
        </div>
        <!-- /content area -->


        <!-- Footer -->
        <div class="navbar navbar-expand-lg navbar-light">
            <div class="text-center d-lg-none w-100">
                <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse"
                        data-target="#navbar-footer">
                    <i class="icon-unfold mr-2"></i>
                    Footer
                </button>
            </div>

            <div class="navbar-collapse collapse" id="navbar-footer">
                        <span class="navbar-text">
                            &copy; 2019. <a href="#">Bengkel Bavaria Mercindo Motor</a>
                        </span>

            </div>
        </div>
        <!-- /footer -->

    </div>
    <!-- /main content -->

</div>
<!-- /page content -->

<!-- Core JS files -->
<script src="https://js.pusher.com/4.3/pusher.min.js"></script>
<script src="{{ asset("assets/global/js/main/jquery.min.js") }}"></script>
<script src="{{ asset("assets/global/js/main/bootstrap.bundle.min.js") }}"></script>
<script src="{{ asset("assets/global/js/plugins/loaders/blockui.min.js") }}"></script>
<script src="{{ asset("assets/global/js/plugins/notifications/sweet_alert.min.js") }}"></script>
<!-- /core JS files -->

<script src="{{ asset("assets/global/js/plugins/tables/datatables/datatables.min.js") }}"></script>
<script src="{{ asset("assets/global/js/plugins/tables/datatables/extensions/fixed_columns.min.js") }}"></script>
<script src="{{ asset("assets/global/js/plugins/tables/datatables/extensions/scroller.min.js") }}"></script>
<script src="{{ asset("assets/global/js/plugins/notifications/jgrowl.min.js") }}"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/noty/3.1.4/noty.min.js"></script>

<script src="{{ asset("assets/global/js/plugins/forms/selects/select2.min.js") }}"></script>
<script src="{{ asset("assets/global/js/plugins/forms/styling/uniform.min.js") }}"></script>

@stack('scripts')

<!-- Theme JS files -->
<script src="{{ asset("assets/js/app.js") }}"></script>
<!-- /theme JS files -->

<script src="{{ asset("js/app.js") }}?time={{mt_rand(110000, 900000)}}" ></script>

@stack("inline-scripts")

<script>
    Pusher.logToConsole = true;

    let pusher = new Pusher('53ef5d03ae6029d9c955', {
        cluster: 'ap1',
        forceTLS: true
    });

    const role = "{{ str_slug($currentUser->roles->pluck("name")[0]) }}";
    const user_id = "{{ $currentUser->id }}";

    if (role === 'service-advisor-sa') {
        let channel_sa = pusher.subscribe(role + "-" + user_id + "-channel");

        channel_sa.bind("status-changed", function(data) {
            new Noty({
                theme: 'metroui',
                type: `${data.message.type}`,
                layout: 'topRight',
                text: `<h3>${data.message.title}</h3><p>${data.message.text}</p>`
            }).show();
        });
    }

    let channel = pusher.subscribe(role + "-channel");

    channel.bind("status-changed", function(data) {
        new Noty({
            theme: 'metroui',
            type: `${data.message.type}`,
            layout: 'topRight',
            text: `<h3>${data.message.title}</h3><p>${data.message.text}</p>`
        }).show();
    });
</script>

<script>
    (function () {
        $('.form-control-select2').select2();
        $('.form-check-input-styled').uniform();

        $(window).ready(function () { // makes sure the whole site is loaded
            $('#preloader_spinner').fadeOut(); // will first fade out the loading animation
            $('#preloader').delay(150).fadeOut('slow'); // will fade out the white DIV that covers the website.
            $('body').delay(150).css({'overflow': 'visible'});
        })
    })();
</script>
</body>
</html>
