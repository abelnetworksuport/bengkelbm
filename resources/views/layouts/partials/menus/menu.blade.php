@foreach ($items as $item)
    @if ($item->hasChilds())
        @include('layouts.partials.menus.item.dropdown', compact('item'))
    @else
        @include('layouts.partials.menus.item.item', compact('item'))
    @endif
@endforeach
