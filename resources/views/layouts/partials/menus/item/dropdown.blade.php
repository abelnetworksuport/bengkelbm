<li class="nav-item nav-item-submenu{{ $item->hasActiveOnChild() ? ' nav-item-expanded nav-item-open' : '' }}">
    <a href="javascript:" class="nav-link">
        {!! $item->getIcon() !!}
        <span>{{ $item->title }}</span>
    </a>
    <ul class="nav nav-group-sub" data-submenu-title="{{ $item->title }}">
        @foreach ($item->childs as $child)
            @if ($child->hasChilds())
                @include('layouts.partials.menus.child.dropdown', ['item' => $child])
            @else
                @include('layouts.partials.menus.child.item', ['item' => $child])
            @endif
        @endforeach
    </ul>
</li>
