@if ($item->isDivider())
    <li class="nav-item-divider"></li>
@elseif ($item->isHeader())
    <li class="nav-item-header">
        <div class="text-uppercase font-size-xs line-height-xs">{{ $item->title }}</div>
        <i class="icon-menu" title="{{ $item->title }}"></i>
    </li>
@else
    <li class="nav-item">
        <a class="nav-link{{ $item->isActive() ? ' active' : '' }}" href="{{ $item->getUrl() }}" {!! $item->getAttributes() !!}>
            {{ $item->title }}
        </a>
    </li>
@endif