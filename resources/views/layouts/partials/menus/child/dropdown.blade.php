<li class="nav-item nav-item-submenu{{ $item->hasActiveOnChild() ? ' nav-item-open' : '' }}">
    <a tabindex="-1" class="nav-link" href="javascript:">{{ $child->title }}</a>
    <ul class="nav nav-group-sub">
        @foreach ($child->childs as $item)
            @if ($item->hasChilds())
                @include('layouts.partials.menus.child.dropdown', ['child' => $item])
            @else
                @include('layouts.partials.menus.child.item', compact('item'))
            @endif
        @endforeach
    </ul>
</li>
