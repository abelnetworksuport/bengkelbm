<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4><i class="{{ $icon }} mr-2"></i> <span class="font-weight-semibold">{{ $title }}</span> - {{ $subtitle }}</h4>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        <div class="header-elements d-none">
            {{ $slot }}
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <?php
                    $first_breadcrumb = array_shift($breadcrumbs);
                    $last_breadcrumb = array_pop($breadcrumbs);
                ?>

                <a href="javascript:" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> {{ $first_breadcrumb }}</a>

                @foreach ($breadcrumbs as $key => $breadcrumb)
                    <a href="javascript:" class="breadcrumb-item">{{ $breadcrumb }}</a>
                @endforeach

                <span class="breadcrumb-item active">{{ $last_breadcrumb }}</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        <div class="breadcrumb-elements-item dropdown p-0">
            <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                <i class="icon-gear mr-1"></i>
                Settings
            </a>

            <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(96px, 40px, 0px);">
                <a href="#" class="dropdown-item"><i class="icon-file-excel"></i> Download Users List</a>
                <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>

            </div>
        </div>
    </div>
</div>
<!-- /page header -->