<div class="list-icons">
    <div class="dropdown">
        <a href="#" class="list-icons-item" data-toggle="dropdown" aria-expanded="false">
            <i class="icon-menu9"></i>
        </a>

        <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(22px, 19px, 0px);">
            <a href="/customer-detail" class="dropdown-item"><i class="icon-database-export"></i> Detail</a>
            <a href="#" class="dropdown-item"><i class="icon-truck"></i> Kendaraan</a>
            <a href="#" class="dropdown-item"><i class="icon-file-word"></i> Cetak Data</a>
        </div>
    </div>
</div>