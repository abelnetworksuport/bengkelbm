@extends("layouts.master")

@section("page-header")
    @component("layouts.partials.slots.page-header",
    [ 'icon' => 'icon-user-tie', 'title' => 'Daftar Jabatan', 'subtitle' => 'Daftar Jabatan', 'breadcrumbs' => [
        'Module', 'Master', 'Daftar Jabatan'
    ] ])

        <a href="javascript:" class="btn btn-labeled btn-labeled-right bg-primary show-modal" data-event="position::add">Tambah Jabatan/Unit <b><i class="icon-plus2"></i></b></a>
    @endcomponent
@endsection

@section("content")
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Daftar Jabatan/Unit</h5>
            <div class="header-elements">
                <ul class="nav nav-pills mb-0">
                    <li class="nav-item"><a href="{{ route("position.export") }}" class="btn bg-violet"><i class="icon-file-excel mr-2"></i> Export</a></li>
                </ul>
            </div>
        </div>

        <div class="card-body"></div>

        <grid-positions></grid-positions>
    </div>
@endsection