<div class="list-icons list-icons-extended">
    <a href="javascript:" class="badge badge-icon bg-violet show-modal" data-url='{{ route('positions.edit', $id) }}' data-event="position::edit"><i class="icon-pencil3"></i></a>
    <a href="javascript:" class="badge badge-icon bg-warning show-modal" data-content='{!! json_encode([
    'url' => route('positions.destroy', $id)
    ]) !!}' data-event="position::remove"><i class="icon-trash"></i></a>
</div>