@extends("layouts.master")

@section("page-header")
    @component("layouts.partials.slots.page-header",
    [ 'icon' => 'icon-crown', 'title' => 'Daftar Status Work Order', 'subtitle' => 'Daftar Status Work Order', 'breadcrumbs' => [
        'Module', 'Master', 'Daftar Status Work Order'
    ] ])

        <a href="javascript:" class="btn btn-labeled btn-labeled-right bg-primary show-modal" data-event="status::add">Tambah Status Work Order <b><i class="icon-plus2"></i></b></a>
    @endcomponent
@endsection

@section("content")
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Daftar Status Work Order</h5>
            <div class="header-elements">
                <ul class="nav nav-pills mb-0">
                    <li class="nav-item"><a href="{{ route("status.export") }}" class="btn bg-violet"><i class="icon-file-excel mr-2"></i> Download Excel</a></li>
                </ul>
            </div>
        </div>

        <div class="card-body"></div>

        <grid-statuses></grid-statuses>
    </div>
@endsection