<div class="list-icons list-icons-extended">
    <a href="javascript:" class="badge badge-icon bg-violet show-modal" data-url='{{ route('statuses.edit', $id) }}' data-event="status::edit"><i class="icon-pencil3"></i></a>
    <a href="javascript:" class="badge badge-icon bg-warning show-modal" data-content='{!! json_encode([
    'url' => route('statuses.destroy', $id)
    ]) !!}' data-event="status::remove"><i class="icon-trash"></i></a>
</div>