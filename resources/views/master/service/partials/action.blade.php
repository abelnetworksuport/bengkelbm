<div class="list-icons list-icons-extended">
    <a href="javascript:" class="badge badge-icon bg-violet show-modal" data-url='{{ route('services.edit', $id) }}' data-event="service::edit"><i class="icon-pencil3"></i></a>
    <a href="javascript:" class="badge badge-icon bg-warning show-modal" data-content='{!! json_encode([
    'url' => route('services.destroy', $id)
    ]) !!}' data-event="service::remove"><i class="icon-trash"></i></a>
</div>