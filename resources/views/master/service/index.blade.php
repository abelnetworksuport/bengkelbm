@extends("layouts.master")

@section("page-header")
    @component("layouts.partials.slots.page-header",
    [ 'icon' => 'icon-database', 'title' => 'Daftar Jasa & Service', 'subtitle' => 'Daftar Jasa & Service Bengkel', 'breadcrumbs' => [
        'Module', 'Master', 'Daftar Jasa & Service'
    ] ])

        <a href="javascript:" class="btn btn-labeled btn-labeled-right bg-primary show-modal" data-event="service::add">Tambah Jasa & Service <b><i class="icon-plus2"></i></b></a>
    @endcomponent
@endsection

@section("content")
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Daftar Jasa & Service</h5>
            <div class="header-elements">
                <ul class="nav nav-pills mb-0">
                    <li class="nav-item"><a href="{{ route("service.export") }}" class="btn bg-violet"><i class="icon-file-excel mr-2"></i> Export</a></li>
                </ul>
            </div>
        </div>

        <div class="card-body"></div>

        <grid-services></grid-services>
    </div>
@endsection