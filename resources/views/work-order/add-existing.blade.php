@extends("layouts.master")

@section("page-header")
    @component("layouts.partials.slots.page-header",
    [ 'icon' => 'icon-truck', 'title' => 'Create Work Order', 'subtitle' => 'Kendaraan & Owner Baru', 'breadcrumbs' => [
        'Module', 'Work Order', 'Create WO Kendaraan & Owner Baru'
    ] ])

    @endcomponent
@endsection

@section("js-initial-data")
    <script>
        window.__INITIAL_STATE__ = '{!! addslashes(json_encode([ 'options' => $master ])) !!}';
    </script>
@endsection

@section("content")
    <work-order-add-form
            url="{{ route('work-order.store') }}"
            url-redirect="{{ route("work-order.index") }}"
            url-print="{{ route('estimasi') }}"
    >

    </work-order-add-form>
@endsection