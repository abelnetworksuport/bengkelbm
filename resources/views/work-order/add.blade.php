@extends("layouts.master")

@section("page-header")
    @component("layouts.partials.slots.page-header",
    [ 'icon' => 'icon-user-plus', 'title' => 'Create Work Order', 'subtitle' => "Kendaraan {$data['vehicle']['plate_number']}", 'breadcrumbs' => [
        'Module', 'Work Order', "Create WO untuk kendaraan {$data['vehicle']['plate_number']}"
    ] ])

    @endcomponent
@endsection

@section("js-initial-data")
    <script>
        window.__INITIAL_STATE__ = '{!! addslashes(json_encode(['options' => $master, 'data' => $data ])) !!}';
    </script>
@endsection

@section("content")
    <work-order-form
            url="{{ route('work-order.store') }}"
            url-redirect="{{ route("work-order.index") }}"
            url-print="{{ route('estimasi') }}"
    ></work-order-form>
@endsection