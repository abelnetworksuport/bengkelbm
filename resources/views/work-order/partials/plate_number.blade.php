<a href="javascript" class="font-weight-bold show-modal" data-event="vehicle-owner:detail"
   data-url="{{ route("vehicles.show", $vehicle_id) }}">{{ $plate_number }}</a>
<div class="text-muted font-size-xs">Driver: {{ $driver }}</div>