@if ($status_id < 8)
    @hasrole("Service Advisor (SA)")
        @if($status_id !== "7")
            <span class="badge {{ $status['colour'] }}">{{ $status['name'] }}</span>
        @else
            <span class="badge {{ $status['colour'] }} modal-status"
                  data-wo-status="{!! json_encode([
                  "work_order_id" => $id,
                  "status_id" => $status_id,
                  "url" => [
                    "store" => url("work-order/status")
                    ]
                  ]) !!}">{{ $status['name'] }}</span>
        @endif
    @endhasrole

    @hasrole("Kepala Bengkel")
        @if($status_id !== "6")
            <span class="badge {{ $status['colour'] }}">{{ $status['name'] }}</span>
        @else
            <span class="badge {{ $status['colour'] }} modal-status"
                  data-wo-status="{!! json_encode([
                  "work_order_id" => $id,
                  "status_id" => $status_id,
                  "url" => [
                    "store" => url("work-order/status")
                    ]
                  ]) !!}">{{ $status['name'] }}</span>
        @endif
    @endhasrole

    @hasrole("")

    @endhasrole
@else
    <span class="badge {{ $status['colour'] }}">{{ $status['name'] }}</span>
@endif
