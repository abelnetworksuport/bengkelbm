<ul class="list-inline list-inline-condensed mb-0 mt-2 mt-sm-0">
    @can("print work-order")
    <li class="list-inline-item">
        <a href="{{ route("work-order::print", $id) }}" target="_blank" class="text-default" title="CETAK WO"><i class="icon-printer text-green-800"></i></a>
    </li>
    <li class="list-inline-item">
        <a href="{{ route("work-order::spk", $id) }}" target="_blank" class="text-default" title="CETAK WO"><i class="icon-printer4 text-violet"></i></a>
    </li>
    @endcan
    @if($status_id < 8)
        @can('add revision')
            <li class="list-inline-item">
                <a href="{{ route("work-order::revision", $id) }}" class="text-default" title="REVISI"><i class="icon-undo text-danger"></i></a>
            </li>
        @endcan
    @endif
</ul>