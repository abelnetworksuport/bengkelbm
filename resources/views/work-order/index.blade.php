@extends("layouts.master")

@section("page-header")
    @component("layouts.partials.slots.page-header",
    [ 'icon' => 'icon-user-plus', 'title' => 'Work Order', 'subtitle' => 'Daftar Work Order', 'breadcrumbs' => [
        'Module', 'Work Order', 'Daftar Work Order'
    ] ])
        @role("Service Advisor (SA)")
            <a href="{{ route("work-order.add") }}" class="btn btn-labeled btn-labeled-right bg-primary">Tambah Work Order Baru <b><i class="icon-plus2"></i></b></a>
        @endrole
    @endcomponent
@endsection

@section("content")
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Daftar Work Order</h5>
        </div>

        <div class="card-body">

        </div>

        <work-order-grid ajax="{{ route("work-order.data") }}"></work-order-grid>
    </div>
@endsection