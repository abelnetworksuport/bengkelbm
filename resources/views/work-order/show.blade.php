@extends("layouts.master")

@section("page-header")
    @component("layouts.partials.slots.page-header",
    [ 'icon' => 'icon-truck', 'title' => 'Work Order Detail', 'subtitle' => "Work Order No #", 'breadcrumbs' => [
        'Module', 'Work Order', 'Work Order Detail'
    ] ])

    @endcomponent
@endsection

@section("js-initial-data")
    <script>
        window.__INITIAL_STATE__ = '{!! addslashes(json_encode([ 'data' => $work_order ])) !!}';
    </script>
@endsection

@section("content")
    <work-order-detail></work-order-detail>
@endsection