@extends("layouts.master")

@section("page-header")
    @component("layouts.partials.slots.page-header",
    [ 'icon' => 'icon-user-plus', 'title' => 'Revisi Work Order', 'subtitle' => 'Revisi Work Order', 'breadcrumbs' => [
        'Module', 'Work Order', 'Revisi Work Order'
    ] ])
    @endcomponent
@endsection

@section("js-initial-data")
    <script>
        window.__INITIAL_WORKORDER__ = '{!! addslashes(json_encode(['workOrder' => $workOrder ])) !!}';
    </script>
@endsection

@section("content")
    <work-order-revision

    ></work-order-revision>
@endsection