@extends("layouts.master")

@section("page-header")
    @component("layouts.partials.slots.page-header",
    [ 'icon' => 'icon-user-plus', 'title' => 'Data Customer/Owner', 'subtitle' => 'Daftar Customer', 'breadcrumbs' => [
        'Module', 'Customer', 'Daftar Customer'
    ] ])

        <a href="#" class="btn btn-labeled btn-labeled-right bg-primary">Tambah Customer & Kendaraan <b><i class="icon-plus2"></i></b></a>
    @endcomponent
@endsection

@section("content")
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Daftar Customer</h5>
            <div class="header-elements">
                <a href="{{ route("owners.export") }}" class="btn bg-violet btn-labeled btn-labeled-left btn-sm"><b><i class="icon-file-excel"></i></b> Download Excel</a>
            </div>
        </div>

        <div class="card-body"></div>

       <owner-grid ajax="{{ route('owners.data') }}"></owner-grid>
    </div>
@endsection