@extends("layouts.master")

@section("page-header")
    @component("layouts.partials.slots.page-header",
    [ 'icon' => 'icon-user', 'title' => 'Tambah Customer & Kendaraan', 'subtitle' => "Customer & Kendataan Baru ", 'breadcrumbs' => [
        'Module', 'Owner', 'Tambah Owner & Kendaraan'
    ] ])

    @endcomponent
@endsection

@section("js-initial-data")

@endsection

@section("content")
    <div class="card">
        <add-new-vehicle></add-new-vehicle>
    </div>
@endsection