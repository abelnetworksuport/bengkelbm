<ul class="list-inline list-inline-condensed mb-0 mt-2 mt-sm-0">
    <li class="list-inline-item">
        <a href="javascript:" class="badge badge-icon bg-teal add-vehicle-modal" title="Tambah Kendaraan Baru" data-owner="{{ $id }}"><i class="icon-plus3"></i></a>
    </li>
</ul>