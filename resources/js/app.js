/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.baseUrl = document.head.querySelector('meta[name="base-url"]').content;

window.Vue = require('vue');

import VeeValidate, {Validator} from 'vee-validate';
import id from 'vee-validate/dist/locale/id';

Validator.localize('id', id);
Vue.use(VeeValidate);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('Widget', require('./components/dashboard/Widget'));
Vue.component('TotalVehiclesDays', require('./components/dashboard/TotalVehiclesDays'));
Vue.component('HeaderSearch', require('./components/dashboard/HeaderSearchComponent'));
Vue.component('NewCustomer', require('./components/dashboard/NewCostumer'));
Vue.component('GridWO', require('./components/dashboard/GridWO'));
Vue.component('TopBrandVehicle', require('./components/dashboard/TopBrandVehicle'));
Vue.component('DashboardWorkOrder', require('./components/dashboard/DashboardWorkOrder'));

Vue.component('VehicleGrid', require('./components/vehicle/VehicleGrid'));
Vue.component('AddNewVehicle', require('./components/vehicle/addNewVehicle'));
Vue.component('OwnerGrid', require('./components/owner/OwnerGrid'));
Vue.component('WorkOrderForm', require('./components/work-order/WorkOrderForm'));
Vue.component('WorkOrderAddForm', require('./components/work-order/WorkOrderAddForm'));
Vue.component('WorkOrderRevision', require('./components/work-order/WorkOrderRevision'));
Vue.component('WorkOrderGrid', require('./components/work-order/WorkOrderGrid'));
Vue.component('GridEmployees', require('./components/employee/GridEmployees'));
// Vue.component('WorkOrderDetail', require('./components/partials/WorkOrderDetail'));

Vue.component('GridCarReturn', require('./components/car-return/GridCarReturn'));
Vue.component('GridWOTemp', require('./components/wo-temp/GridWOTemp'));
Vue.component('WorkOrderCreate', require('./components/wo-temp/WorkOrderCreate'));

Vue.component("gridUsers", require('./components/settings/users/gridUsers'));
Vue.component("gridRoles", require('./components/settings/roles/gridRoles'));
Vue.component("GridServices", require('./components/master/service/GridServices'));
Vue.component("GridPositions", require('./components/master/position/GridPositions'));
Vue.component("GridStatuses", require('./components/master/status/GridStatuses'));

const app = new Vue({
    el: '#app'
});
