import axios from "axios";

export const WorkOrderStore = {
    state: {
        tab1: {
            vehicle: {
                vin_number: null,
                engine_number: null,
                plate_number: null,
                brand_id: null,
                brand_series: null,
                car_type_id: null,
                fuel_id: null,
                colour: null,
                transmision_id: null,
                build_year: null,
                millage: null,
                driver_name: null,
                driver_phone: null,
                salutation_id: null,
            },
            vehicle_text: {
                brand_id: null,
                car_type_id: null,
                fuel_id: null,
                transmision_id: null,
                salutation_id: null,
            },
            owner: {
                id: null,
                fullname: null,
                salutation_id: null,
                owner_group: null,
                insurance_name: null,
                address:null,
                province_id: null,
                district_id: null,
                phone_number: null,
                email: null
            },
            owner_text: {
                salutation_id: null,
                province_id: null,
                district_id: null
            }
        },
        tab2: {
            problems: [
                { id: 1, problem: null, description: null }
            ]
        },
        tab3: {
            services: [
                { id: 1, service_id: null, service_text: '', estimated_price: 0, rate: null, fru: null }
            ],
            service_manuals: [

            ],
            parts: [
                { id: 1, part_name: null, part_text: '', quantities: 0, quantity_id: 0, qty_label: null, qty_text: '', estimated_price: 0}
            ],
            servicesTotalPrice: 0,
            partsTotalPrice: 0
        }
    },
};