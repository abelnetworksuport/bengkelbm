<?php
/**
 * Created By: Sugeng
 * Date: 09/10/18
 * Time: 11.09
 */
return [
    'model_defaults' => [
        'namespace'       => 'App\\Models',
        'output_path'     => app_path('Models'),
        'db_types'        => [
                                'enum' => 'string',
                             ]
    ],
];