<?php

return [
    /*  Default view for navigation */
    'view'  => 'layouts.partials.menus.limitless',

    /* Navigation name variable */
    'name'  => 'sidebar-nav',

    /* Lists Menu Navigasi */
    'menus' => [
        'superadmin'          => [
            [
                'header' => 'Modules'
            ],
            [
                'modules' => [
                    [
                        'url'   => 'dashboard',
                        'title' => 'Dashboard',
                        'icon'  => 'icon-home'
                    ]
                ]
            ],
            [
                'divider' => null
            ],
            [
                'modules' => [
                    [
                        'url'   => 'work-order',
                        'title' => 'Work Order (WO)',
                        'icon'  => 'icon-stack',
                        'child' => [
                            [
                                'url'   => 'work-order/add',
                                'title' => 'Buat WO Baru (Pelanggan Baru)'
                            ],
                            [
                                'url'   => 'vehicles',
                                'title' => 'Buat WO Baru (Pelanggan Lama)'
                            ],
                            [
                                'url'   => 'work-order',
                                'title' => 'Daftar WO'
                            ]
                        ]
                    ],
                    [
                        'url'   => 'car-return',
                        'title' => 'Car Return (CR)',
                        'icon'  => 'icon-redo2',
                        'child' => [
                            [
                                'url'   => 'car-return/create',
                                'title' => 'Buat CR Baru'
                            ],
                            [
                                'url'   => 'car-return',
                                'title' => 'Daftar CR'
                            ]
                        ]
                    ],
                    [
                        'url'   => 'Costumers',
                        'title' => 'Costumers',
                        'icon'  => 'icon-users',
                        'child' => [
                            [
                                'url'   => 'owners/add',
                                'title' => 'Customer & Kendaraan Baru'
                            ],
                            [
                                'url'   => 'owners',
                                'title' => 'Daftar Customer'
                            ]
                        ]
                    ],
                    [
                        'url'   => 'Kendaraan',
                        'title' => 'Kendaraan',
                        'icon'  => 'icon-truck',
                        'child' => [
                            [
                                'url'   => 'vehicles',
                                'title' => 'Daftar Kendaraan'
                            ]
                        ]
                    ],
                    [
                        'url'   => 'employees',
                        'title' => 'Karyawan',
                        'icon'  => 'icon-user-tie',
                        'child' => [
                            [
                                'url'   => 'employees',
                                'title' => 'Daftar Karyawan'
                            ]
                        ]
                    ],
                ]
            ],
            [
                'divider' => null
            ],
            [
                'header' => 'Settings'
            ],
            [
                'modules' => [
                    [
                        'url'   => 'javascript:',
                        'title' => 'Master Data',
                        'icon'  => 'icon-database',
                        'child' => [
                            [
                                'url'   => 'master/services',
                                'title' => 'Jasa & Service'
                            ],
                            [
                                'url'   => 'master/positions',
                                'title' => 'Jabatan'
                            ],
                            [
                                'url'   => 'master/statuses',
                                'title' => 'Status Work Order'
                            ],
                        ]
                    ],
                    [
                        'url'   => 'user-settings',
                        'title' => 'User Management',
                        'icon'  => 'icon-users',
                        'child' => [
                            [
                                'url'   => 'settings/users',
                                'title' => 'Users'
                            ],
                            [
                                'url'   => 'settings/roles',
                                'title' => 'Roles'
                            ]
                        ]
                    ]
                ]
            ]
        ],
        'service-advisor-sa'  => [
            [
                'header' => 'Modules'
            ],
            [
                'modules' => [
                    [
                        'url'   => 'dashboard',
                        'title' => 'Dashboard',
                        'icon'  => 'icon-home'
                    ]
                ]
            ],
            [
                'divider' => null
            ],
            [
                'modules' => [
                    [
                        'url'   => 'work-order',
                        'title' => 'Work Order (WO)',
                        'icon'  => 'icon-stack',
                        'child' => [
                            [
                                'url'   => 'work-order/add',
                                'title' => 'Buat WO Baru (Pelanggan Baru)'
                            ],
                            [
                                'url'   => 'vehicles',
                                'title' => 'Buat WO Baru (Pelanggan Lama)'
                            ],
                            [
                                'url'   => 'work-order',
                                'title' => 'Daftar WO'
                            ],
                            [
                                'url'   => 'work-order-temp',
                                'title' => 'Daftar WO Sementara'
                            ]
                        ]
                    ],
                    [
                        'url'   => 'car-return',
                        'title' => 'Car Return (CR)',
                        'icon'  => 'icon-redo2',
                        'child' => [
                            [
                                'url'   => 'car-return/create',
                                'title' => 'Buat CR Baru'
                            ],
                            [
                                'url'   => 'car-return',
                                'title' => 'Daftar CR'
                            ]
                        ]
                    ],
                    [
                        'url'   => 'Costumers',
                        'title' => 'Costumers',
                        'icon'  => 'icon-users',
                        'child' => [
                            [
                                'url'   => 'owners/add',
                                'title' => 'Customer & Kendaraan Baru'
                            ],
                            [
                                'url'   => 'owners',
                                'title' => 'Daftar Customer'
                            ]
                        ]
                    ],
                    [
                        'url'   => 'Kendaraan',
                        'title' => 'Kendaraan',
                        'icon'  => 'icon-truck',
                        'child' => [
                            [
                                'url'   => 'vehicles',
                                'title' => 'Daftar Kendaraan'
                            ]
                        ]
                    ]
                ]
            ],
            [
                'divider' => null
            ],
            [
                'modules' => [
                    [
                        'url'   => 'report',
                        'title' => 'Reports',
                        'icon'  => 'icon-stats-bars2',
                        'child' => [
                            [
                                'url'   => 'work-order/add',
                                'title' => 'Work Order'
                            ],
                            [
                                'url'   => 'vehicles',
                                'title' => 'Kendaraan'
                            ]
                        ]
                    ]
                ]
            ],

        ],
        'kepala-bengkel'      => [
            [
                'header' => 'Modules'
            ],
            [
                'modules' => [
                    [
                        'url'   => 'dashboard',
                        'title' => 'Dashboard',
                        'icon'  => 'icon-home'
                    ]
                ]
            ],
            [
                'divider' => null
            ],
            [
                'modules' => [
                    [
                        'url'   => 'work-order',
                        'title' => 'Work Order (WO)',
                        'icon'  => 'icon-stack',
                        'child' => [
                            [
                                'url'   => 'work-order',
                                'title' => 'Daftar WO'
                            ]
                        ]
                    ],
                    [
                        'url'   => 'car-return',
                        'title' => 'Car Return (CR)',
                        'icon'  => 'icon-redo2',
                        'child' => [
                            [
                                'url'   => 'car-return',
                                'title' => 'Daftar CR'
                            ]
                        ]
                    ]
                ]
            ]
        ],
        'control-tower-ct'    => [
            [
                'header' => 'Modules'
            ],
            [
                'modules' => [
                    [
                        'url'   => 'dashboard',
                        'title' => 'Dashboard',
                        'icon'  => 'icon-home'
                    ]
                ]
            ],
            [
                'divider' => null
            ],
            [
                'modules' => [
                    [
                        'url'   => 'work-order',
                        'title' => 'Work Order (WO)',
                        'icon'  => 'icon-stack',
                        'child' => [
                            [
                                'url'   => 'work-order',
                                'title' => 'Daftar WO'
                            ]
                        ]
                    ],
                    [
                        'url'   => 'car-return',
                        'title' => 'Car Return (CR)',
                        'icon'  => 'icon-redo2',
                        'child' => [
                            [
                                'url'   => 'car-return',
                                'title' => 'Daftar CR'
                            ]
                        ]
                    ]
                ]
            ]
        ],
        'final-inspection-fi' => [
            [
                'header' => 'Modules'
            ],
            [
                'modules' => [
                    [
                        'url'   => 'dashboard',
                        'title' => 'Dashboard',
                        'icon'  => 'icon-home'
                    ]
                ]
            ],
            [
                'divider' => null
            ],
            [
                'modules' => [
                    [
                        'url'   => 'work-order',
                        'title' => 'Work Order (WO)',
                        'icon'  => 'icon-stack',
                        'child' => [
                            [
                                'url'   => 'work-order',
                                'title' => 'Daftar WO'
                            ]
                        ]
                    ]
                ]
            ]
        ],
        'sparepart'           => [
            [
                'header' => 'Modules'
            ],
            [
                'modules' => [
                    [
                        'url'   => 'dashboard',
                        'title' => 'Dashboard',
                        'icon'  => 'icon-home'
                    ]
                ]
            ],
            [
                'divider' => null
            ],
            [
                'modules' => [
                    [
                        'url'   => 'work-order',
                        'title' => 'Work Order (WO)',
                        'icon'  => 'icon-stack',
                        'child' => [
                            [
                                'url'   => 'work-order',
                                'title' => 'Daftar WO'
                            ]
                        ]
                    ]
                ]
            ]
        ],
        'admin-mekanik'           => [
            [
                'header' => 'Modules'
            ],
            [
                'modules' => [
                    [
                        'url'   => 'dashboard',
                        'title' => 'Dashboard',
                        'icon'  => 'icon-home'
                    ]
                ]
            ],
            [
                'divider' => null
            ],
            [
                'modules' => [
                    [
                        'url'   => 'work-order',
                        'title' => 'Work Order (WO)',
                        'icon'  => 'icon-stack',
                        'child' => [
                            [
                                'url'   => 'work-order',
                                'title' => 'Daftar WO'
                            ]
                        ]
                    ]
                ]
            ]
        ],
        'body-repair'         => [
            [
                'header' => 'Modules'
            ],
            [
                'modules' => [
                    [
                        'url'   => 'dashboard',
                        'title' => 'Dashboard',
                        'icon'  => 'icon-home'
                    ]
                ]
            ],
            [
                'divider' => null
            ],
            [
                'modules' => [
                    [
                        'url'   => 'work-order',
                        'title' => 'Work Order (WO)',
                        'icon'  => 'icon-stack',
                        'child' => [
                            [
                                'url'   => 'work-order/add',
                                'title' => 'Buat WO Baru (Pelanggan Baru)'
                            ],
                            [
                                'url'   => 'vehicles',
                                'title' => 'Buat WO Baru (Pelanggan Lama)'
                            ],
                            [
                                'url'   => 'work-order',
                                'title' => 'Daftar WO'
                            ]
                        ]
                    ],
                    [
                        'url'   => 'car-return',
                        'title' => 'Car Return (CR)',
                        'icon'  => 'icon-redo2',
                        'child' => [
                            [
                                'url'   => 'car-return/create',
                                'title' => 'Buat CR Baru'
                            ],
                            [
                                'url'   => 'car-return',
                                'title' => 'Daftar CR'
                            ]
                        ]
                    ]
                ]
            ]
        ],
    ]
];
