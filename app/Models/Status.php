<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $code
 * @property string $name
 * @property string $condition
 * @property string $active
 * @property string $created_at
 * @property string $updated_at
 */
class Status extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['code', 'name', 'condition', 'active', 'created_at', 'updated_at'];

    protected $hidden = ['created_at', 'updated_at', 'active', 'condition'];

}
