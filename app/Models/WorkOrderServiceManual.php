<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $work_order_id
 * @property int $name
 * @property float $estimated_price
 * @property string $notes
 * @property string $created_at
 * @property string $updated_at
 * @property string $created_by
 */
class WorkOrderServiceManual extends Model
{
    protected $table = "work_order_service_manual";
    /**
     * @var array
     */
    protected $fillable = ['work_order_id', 'name', 'fru', 'rate', 'estimated_price', 'created_at', 'updated_at', 'created_by'];
}
