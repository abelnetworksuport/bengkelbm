<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $work_order_id
 * @property int $sparepart_id
 * @property float $estimated_price
 * @property float $actual_price
 * @property int $quantities
 * @property string $qty_label
 * @property int $service_status
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 */
class WorkOrderPartsTemp extends Model
{

    protected $table = "work_order_parts_temp";
    /**
     * @var array
     */
    protected $fillable = ['work_order_id', 'sparepart_id', 'part_name', 'estimated_price', 'actual_price', 'quantities', 'quantity_id', 'qty_label', 'service_status', 'status', 'created_at', 'updated_at'];

    public function sparepart()
    {
        return $this->belongsTo(Sparepart::class);
    }
}
