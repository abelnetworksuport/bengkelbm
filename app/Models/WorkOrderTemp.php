<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $code
 * @property int $vehicle_id
 * @property int $owner_id
 * @property int $service_advisor_id
 * @property string $driver
 * @property int $driver_salutation
 * @property string $latest_millage
 * @property float $service_price
 * @property float $part_price
 * @property int $minutes_estimasted
 * @property int $minutes_finished
 * @property string $car_return_from
 * @property string $created_at
 * @property string $updated_at
 * @property int revision
 */
class WorkOrderTemp extends Model
{
    protected $table = "work_orders_temp";

    /**
     * @var array
     */
    protected $fillable
        = ['code', 'vehicle_id', 'plate_number', 'status_id', 'owner_id', 'owner_name', 'service_advisor_id', 'driver', 'driver_salutation', 'driver_phone', 'latest_millage', 'service_price', 'part_price', 'minutes_estimasted', 'minutes_finished', 'car_return_from', 'created_at', 'updated_at',
           'service_discount', 'service_discount_flat', 'service_discount_percentage', 'cr_code', 'cr_type', 'services', 'spareparts', 'problems',
           'part_discount', 'part_discount_flat', 'part_discount_percentage', 'revision'];

    protected $dates
        = [
            "minutes_finished"
        ];

    protected $casts
        = [
            "created_at"       => 'datetime:d F Y H:i:s',
            "minutes_finished" => 'datetime:d F Y H:i:s',
            "services"         => 'array',
            'problems'         => 'array',
            'spareparts'       => 'array'
        ];

    public function owner()
    {
        return $this->belongsTo(Owner::class);
    }

    public function vehicle()
    {
        return $this->belongsTo(Vehicle::class);
    }

    public function serviceAdvisor()
    {
        return $this->belongsTo(User::class);
    }
}
