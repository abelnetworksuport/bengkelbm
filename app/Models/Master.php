<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $group_code
 * @property string $group_name
 * @property string $code
 * @property string $name
 * @property string $created_at
 * @property string $updated_at
 */
class Master extends Model
{
    const GENDER = "001";
    const SALUTATION = "002";
    const TRANSMISSION = "003";
    const FUEL = "004";
    const CAR_TYPE = "005";

    /**
     * @var array
     */
    protected $fillable = ['group_code', 'group_name', 'code', 'name'];

    protected $visible = ['code', 'name'];

    public static function constantName($value)
    {
        try {
            $class = new \ReflectionClass(__CLASS__);
        } catch (\ReflectionException $e) {

        }

        $constants = array_flip($class->getConstants());

        return strtolower($constants[$value]);
    }

}
