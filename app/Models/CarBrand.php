<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property string $build_country
 * @property string $created_at
 * @property string $updated_at
 */
class CarBrand extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['name', 'build_country', 'created_at', 'updated_at'];

    protected $visible = [
        'name', 'id'
    ];
}
