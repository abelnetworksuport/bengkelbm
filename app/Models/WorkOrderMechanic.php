<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $work_order_id
 * @property int $employee_id
 * @property int $working_minutes
 * @property string $created_at
 * @property string $updated_at
 */
class WorkOrderMechanic extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['work_order_id', 'mechanic_id', 'notes', 'working_minutes', 'created_at', 'updated_at'];

    public function mechanicName()
    {
        return $this->belongsTo(Employee::class, 'mechanic_id', 'id');
    }
}
