<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $vin_number
 * @property string $plate_number
 * @property string $engine_number
 * @property int $brand_id
 * @property string $brand_series
 * @property int $build_year
 * @property string $colour
 * @property int $millage
 * @property int $transmision_id
 * @property int $fuel_id
 * @property string $last_service
 * @property string $registered
 * @property string $notes
 * @property string $created_at
 * @property string $updated_at
 */
class Vehicle extends Model
{

    protected $table = "vehicles_temp";
    /**
     * @var array
     */
    protected $fillable = ['vin_number', 'plate_number', 'engine_number', 'brand_id', 'car_type_id', 'brand_series', 'build_year', 'colour', 'millage', 'transmision_id', 'fuel_id', 'last_service', 'registered', 'notes', 'created_at', 'updated_at'];

    protected $dates
        = [
            'registered', 'last_service'
        ];

    protected $appends
        = [
            'register_date'
        ];

    public function getRegisterDateAttribute()
    {
        return $this->registered->format("d F Y");
    }

    public function owners()
    {
        return $this->belongsToMany(Owner::class, 'vehicle_owners');
    }

    public function owner()
    {
        return $this->owners()->latest()->limit(1);
    }

    public function fuel()
    {
        return $this->belongsTo(Master::class, "fuel_id", "code")->where('group_code', Master::FUEL)->withDefault(['name' => '-']);
    }

    public function transmision()
    {
        return $this->belongsTo(Master::class, "transmision_id", "code")->where('group_code', Master::TRANSMISSION)->withDefault(['name' => '-']);
    }

    public function brand()
    {
        return $this->belongsTo(CarBrand::class)->withDefault(['name' => '-']);
    }

    public function brands()
    {
        return $this->belongsTo(CarBrand::class)->withDefault(['name' => '-']);
    }

    public function carType()
    {
        return $this->belongsTo(Master::class, "car_type_id", "code")->where('group_code', Master::CAR_TYPE)->withDefault(['name' => '-']);
    }

    public function workOrders()
    {
        return $this->hasMany(WorkOrder::class);
    }

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            $model->registered = Carbon::now();
        });
    }
}
