<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property float $price
 * @property string $active
 * @property string $created_at
 * @property string $updated_at
 */
class Sparepart extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['name', 'price', 'active', 'created_at', 'updated_at'];

}
