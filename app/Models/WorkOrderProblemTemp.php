<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property int $work_order_id
 * @property int $working_estimated
 * @property int $working_finished
 * @property string $solution
 * @property string $description
 * @property string $created_at
 * @property string $updated_at
 */
class WorkOrderProblemTemp extends Model
{
    protected $table = "work_order_problems_temp";

    /**
     * @var array
     */
    protected $fillable = ['name', 'work_order_id', 'working_estimated', 'working_finished', 'solution', 'description', 'created_at', 'updated_at'];

}
