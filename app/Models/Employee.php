<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $code
 * @property string $fullname
 * @property int $salutation_id
 * @property string $identity_card_number
 * @property string $birthday
 * @property string $initials
 * @property string $address
 * @property string $email
 * @property string $phone_number
 * @property int $position_id
 * @property string $start_working
 * @property string $created_at
 * @property string $updated_at
 */
class Employee extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['code', 'fullname', 'salutation_id', 'identity_card_number', 'birthday', 'initials', 'address', 'email', 'phone_number', 'position_id', 'is_mechanic', 'start_working'];

    protected $dates
        = [
            'birthday'
        ];

    protected $casts
        = [
            'is_mechanic'
        ];

    public function position()
    {
        return $this->belongsTo(Position::class);
    }

    public function gender()
    {
        return $this->belongsTo(Master::class, 'code', 'salutation_id')->where('group_code', Master::GENDER);
    }

    public function salutation()
    {
        return $this->belongsTo(Master::class, 'code', 'salutation_id')->where('group_code', Master::SALUTATION)->withDefault(['name' => '-']);
    }

    public function store(array $form)
    {
        return $this->create([
            'fullname'             => $form['fullname'],
            'salutation_id'        => $form['salutation_id'],
            'position_id'          => $form['position_id'],
            'initials'             => $form['initials'],
            'phone_number'         => $form['phone_number'],
            'identity_card_number' => $form['identity_card_number'],
            'address'              => $form['address'],
            'is_mechanic'          => ($form['position_id'] == 7) ? "1" : "0",
            'code'                 => $this->generateCode()
        ]);
    }

    public function updateData(array $form)
    {
        return $this->update([
            'fullname'             => $form['fullname'],
            'salutation_id'        => $form['salutation_id'],
            'position_id'          => $form['position_id'],
            'initials'             => $form['initials'],
            'phone_number'         => $form['phone_number'],
            'address'              => $form['address'],
            'is_mechanic'          => ($form['position_id'] == 7) ? "1" : "0",
            'identity_card_number' => $form['identity_card_number'],

        ]);
    }

    protected function generateCode()
    {
        $lastCode = $this->select('code')->orderBy('code', 'DESC')->first();

        $nextCode = (int)$lastCode->code + 1;

        return str_pad($nextCode, 3, '0', STR_PAD_LEFT);
    }
}
