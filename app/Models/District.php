<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $province_id
 * @property string $name
 */
class District extends Model
{
    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['province_id', 'name'];
    
    public function province()
    {
        return $this->belongsTo(Province::class);
    }

}
