<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $work_order_id
 * @property int $service_id
 * @property float $estimated_price
 * @property float $actual_price
 * @property string $assigned_mechanic
 * @property int $status
 * @property string $notes
 * @property string $created_at
 * @property string $updated_at
 */
class WorkOrderService extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['work_order_id', 'service_id', 'estimated_price', 'fru', 'rate', 'actual_price', 'assigned_mechanic', 'status', 'notes', 'created_at', 'updated_at'];

    public function service()
    {
        return $this->belongsTo(Service::class);
    }
}
