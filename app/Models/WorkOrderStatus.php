<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $work_order_id
 * @property string $timestamp
 * @property int $status
 * @property int $changed_by
 * @property string $notes
 * @property string $created_at
 * @property string $updated_at
 */
class WorkOrderStatus extends Model
{
    /**
     * @var array
     */
    protected $fillable = [ 'work_order_id', 'timestamp', 'status', 'changed_by', 'notes' ];

    public function statusName()
    {
        return $this->belongsTo(Status::class, 'status');
    }

}
