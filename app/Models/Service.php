<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property float $weight
 * @property int $estimated_price
 * @property int $user_created
 * @property int $user_updated
 * @property string $created_at
 * @property string $updated_at
 */
class Service extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['name', 'weight', 'estimated_price', 'user_created', 'user_updated', 'created_at', 'updated_at'];

    public function store(array $form)
    {
        return $this->create([
            'name'            => $form['name'],
            'estimated_price' => $form['estimated_price'],
            'weight'          => $form['weight'],
        ]);
    }

    public function updateData(array $form)
    {
        return $this->update([
            'name'            => $form['name'],
            'estimated_price' => $form['estimated_price'],
            'weight'          => $form['weight'],
        ]);
    }

}
