<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $customer_code
 * @property string $fullname
 * @property int $salutation_id
 * @property int $gender_id
 * @property string $address
 * @property int $province_id
 * @property int $district_id
 * @property int $zip_code
 * @property string $phone_number
 * @property string $mobile_phone
 * @property string $email
 * @property string $created_at
 * @property string $updated_at
 */
class Owner extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['customer_code', 'fullname', 'salutation_id', 'gender_id', 'address', 'province_id', 'district_id', 'zip_code', 'phone_number', 'mobile_phone', 'email', 'registered'];

    protected $dates = [
        "registered"
    ];

    protected $visible = [
        'id', 'customer_code', 'fullname', 'salutation_id', 'gender_id', 'address', 'province_id', 'district_id', 'zip_code', 'phone_number', 'mobile_phone', 'email', 'registered'
    ];

    public function salutation()
    {
        return $this->belongsTo(Master::class, 'salutation_id', 'code')->where('group_code', Master::SALUTATION)->withDefault([
            'name' => '-'
        ]);
    }

    public function gender()
    {
        return $this->belongsTo(Master::class, 'gender_id', 'code')->where('group_code', Master::GENDER);
    }

    public function province()
    {
        return $this->belongsTo(Province::class)->withDefault(['name' => '-']);
    }

    public function district()
    {
        return $this->belongsTo(District::class)->withDefault(['name' => '-']);
    }

    public function vehicles()
    {
        return $this->belongsToMany(Vehicle::class, 'vehicle_owners');
    }

    public function workOrders()
    {
        return $this->hasMany(WorkOrder::class);
    }

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            $model->registered = Carbon::now();
        });
    }
}
