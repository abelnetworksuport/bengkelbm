<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property string $initials
 * @property string $active
 * @property string $created_at
 * @property string $updated_at
 */
class Position extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['name', 'initials', 'active'];

    public function employees()
    {
        return $this->belongsTo(Employee::class);
    }
}
