<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $new_work_order_id
 * @property int $old_work_order_id
 * @property int $issued_by
 * @property string $description
 * @property string $issued_date
 * @property string $created_at
 * @property string $updated_at
 */
class CarReturn extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['new_work_order_id', 'old_work_order_id', 'issued_by', 'description', 'issued_date', 'created_at', 'updated_at'];

}
