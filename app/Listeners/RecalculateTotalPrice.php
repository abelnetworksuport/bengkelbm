<?php

namespace App\Listeners;

use App\Events\WorkOrderChangedEvent;

class RecalculateTotalPrice
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param WorkOrderChangedEvent $event
     * @return void
     */
    public function handle(WorkOrderChangedEvent $event)
    {
        $event->workOrder->service_price = $this->calculateServices($event->workOrder->services()->get()) + $this->calculateServiceManuals($event->workOrder->serviceManuals()->get());
        $event->workOrder->part_price = $this->calculateSpareparts($event->workOrder->parts()->get());
        $event->workOrder->save();
    }

    protected function calculateServices($services)
    {
        $total_price_service = 0;
        foreach ($services as $service) {
            $total_price_service += $service->estimated_price;
        }

        return $total_price_service;
    }

    protected function calculateServiceManuals($services)
    {
        $total_price_service = 0;
        foreach ($services as $service) {
            $total_price_service += $service->estimated_price;
        }

        return $total_price_service;
    }

    protected function calculateSpareparts($spareparts)
    {
        $total_price_sparepart = 0;
        foreach ($spareparts as $sparepart) {
            $total_price_sparepart += $sparepart->estimated_price;
        }

        return $total_price_sparepart;
    }
}
