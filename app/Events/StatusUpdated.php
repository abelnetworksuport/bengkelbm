<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class StatusUpdated implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $username;

    public $message;

    public $to;

    /**
     * Create a new event instance.
     *
     * @param $to
     * @param $username
     * @param $message
     */
    public function __construct($to, $username, array $message)
    {
        $this->username = $username;
        $this->message = $message;
        $this->to = str_slug($to);
    }

    public function broadcastAs()
    {
        return "status-changed";
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return ["{$this->to}-channel"];
    }
}
