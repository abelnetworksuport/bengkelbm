<?php

namespace App\Events;

use App\Models\WorkOrder;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;

class WorkOrderChangedEvent
{
    use SerializesModels;
    /**
     * @var WorkOrder
     */
    public $workOrder;

    /**
     * Create a new event instance.
     *
     * @param WorkOrder $workOrder
     */
    public function __construct(WorkOrder $workOrder)
    {
        $this->workOrder = $workOrder;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
