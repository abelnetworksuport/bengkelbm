<?php namespace App\Exports;

use App\Models\Employee;
use Illuminate\Database\Query\Builder;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class EmployeeExport implements FromQuery, WithMapping, WithHeadings, ShouldAutoSize
{
    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            'ID',
            'Kode',
            'Nama Karyawan',
            'Jabatan/Unit',
            'No. Identitas',
            'No. Telepon',
            'Mekanik',
            'Alamat'
        ];
    }

    /**
     * @param mixed $row
     *
     * @return array
     */
    public function map($row): array
    {
        return [
            $row->id,
            $row->code,
            $row->fullname,
            $row->position->name,
            $row->identity_card_number,
            $row->phone_number,
            ($row->is_mechanic) ? "YA" : "TIDAK",
            $row->address
        ];
    }

    /**
     * @return Builder
     */
    public function query()
    {
        return Employee::query()->with('position');
    }
}
