<?php namespace App\Exports;

use App\Models\Status;
use Illuminate\Database\Query\Builder;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class StatusExport implements FromQuery, WithMapping, WithHeadings, ShouldAutoSize
{
    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            'ID',
            'Nama Status WO',
            'Kode Status',
            'Warna'
        ];
    }

    /**
     * @param mixed $row
     *
     * @return array
     */
    public function map($row): array
    {
        return [
            $row->id,
            $row->name,
            $row->code,
            $row->colour
        ];
    }

    /**
     * @return Builder
     */
    public function query()
    {
        return Status::query();
    }
}
