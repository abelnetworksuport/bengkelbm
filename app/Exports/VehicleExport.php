<?php namespace App\Exports;

use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class VehicleExport implements WithMultipleSheets
{
    /**
     * @return array
     */
    public function sheets(): array
    {
        $sheets = [];

        $yearNow = date('Y');

        for ($year = 2006; $year <= $yearNow; $year++) {
            $sheets[] = new VehiclePerYearSheet($year);
        }

        return $sheets;
    }
}