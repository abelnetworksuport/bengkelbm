<?php namespace App\Exports;
use App\Models\Vehicle;
use Illuminate\Database\Query\Builder;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;

/**
 * Created By: Sugeng
 * Date: 2018-12-18
 * Time: 18:36
 */
class VehiclePerYearSheet implements FromQuery, WithTitle, WithMapping, WithHeadings, ShouldAutoSize
{
    protected $year;

    public function __construct(int $year)
    {
        $this->year = $year;
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            'ID',
            'Nomor Chasis',
            'No. Polisi',
            'No. Mesin',
            'Merk',
            'Tahun Pembuatan',
            'Warna',
            'Kilometer',
            'Transmisi',
            'Bahan Bakar',
            'Tipe Mobil',
            'Terakhir Service',
            'Tanggal Terdaftar',
            'Catatan'
        ];
    }

    /**
     * @param mixed $row
     *
     * @return array
     */
    public function map($row): array
    {
        return [
            $row->id,
            $row->vin_number,
            $row->plate_number,
            $row->engine_number,
            ($row->brand->name == '-') ? $row->brand_series : $row->brand->name . "/" . $row->brand_series,
            $row->build_year,
            $row->colour,
            $row->millage,
            $row->transmision->name,
            $row->fuel->name,
            $row->carType->name,
            $row->last_service,
            $row->registered->format("d F Y"),
            $row->notes
        ];
    }

    /**
     * @return Builder
     */
    public function query()
    {
        return Vehicle::query()->with('brand', 'transmision', 'carType', 'fuel')->whereYear('registered', $this->year);
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return 'Tahun ' . $this->year;
    }
}