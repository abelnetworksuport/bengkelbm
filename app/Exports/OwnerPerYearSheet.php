<?php namespace App\Exports;

use App\Models\Owner;
use Illuminate\Database\Query\Builder;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
/**
 * Created By: Sugeng
 * Date: 2018-12-18
 * Time: 18:50
 */
class OwnerPerYearSheet implements FromQuery, WithTitle, WithMapping, WithHeadings, ShouldAutoSize
{
    protected $year;

    public function __construct(int $year)
    {
        $this->year = $year;
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            'ID',
            'Nama Lengkap',
            'Customer Group',
            'Menggunakan Asuransi',
            'Alamat',
            'Telepon',
            'Email',
            'Tanggal Terdaftar',
        ];
    }

    /**
     * @param mixed $row
     *
     * @return array
     */
    public function map($row): array
    {
        return [
            $row->id,
            $row->fullname,
            $row->owner_group,
            $row->insurance_name,
            $row->address,
            $row->phone_number,
            $row->email,
            $row->registered->format("d F Y")
        ];
    }

    /**
     * @return Builder
     */
    public function query()
    {
        return Owner::query()->whereYear('registered', $this->year);
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return 'Tahun ' . $this->year;
    }
}