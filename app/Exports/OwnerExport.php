<?php namespace App\Exports;

use Maatwebsite\Excel\Concerns\WithMultipleSheets;

/**
 * Created By: Sugeng
 * Date: 2018-12-18
 * Time: 18:49
 */
class OwnerExport implements WithMultipleSheets
{

    /**
     * @return array
     */
    public function sheets(): array
    {
        $sheets = [];

        $yearNow = date('Y');

        for ($year = 2006; $year <= $yearNow; $year++) {
            $sheets[] = new OwnerPerYearSheet($year);
        }

        return $sheets;
    }
}