<?php namespace App\Exports\Reports;

use App\Models\WorkOrder;
use Illuminate\Database\Query\Builder;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

/**
 * Created By: Sugeng
 * Date: 2018-12-21
 * Time: 22:42
 */
class WorkOrderMonthly implements FromQuery, WithMapping, WithHeadings, ShouldAutoSize
{
    public $month = 0;

    public function forMonth(int $month)
    {
        $this->month = $month;

        return $this;
    }

    /**
     * @return Builder
     */
    public function query()
    {
        return WorkOrder::query()
                        ->with('problems', 'services', 'parts', 'mechanics')
                        ->whereMonth('created_at', $this->month)
                        ->orderBy('created_at');
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            'ID',
            'No. Work Order',
            'Tanggal Work Order',
            'Kendaraan',
            'Owner',
            'Driver',
            'No. Telepon Driver',
            'Total Jasa & Service',
            'Total Sparepart',
            'Waktu Estimasi',
            'Waktu Selesai'
        ];
    }

    /**
     * @param mixed $row
     *
     * @return array
     */
    public function map($row): array
    {
        return [
            $row->id,
            $row->code,
            $row->created_at->format('d F Y H:i:s'),
            $row->plate_number,
            $row->owner_name,
            $row->driver,
            $row->driver_phone,
            number_format($row->service_price),
            number_format($row->part_price),
            $this->parseMinutes($row->minutes_estimated),
            $row->minutes_finished->format('d F Y H:i:s')
        ];
    }

    protected function parseMinutes($minutes_estimated)
    {
        $hour    = floor(($minutes_estimated / 60) % 60);
        $minutes = $minutes_estimated % 60;

        return "{$hour} Jam, {$minutes} Menit";
    }
}