<?php namespace App\Transformers;

use App\Models\Sparepart;
use Flugg\Responder\Transformers\Transformer;

/**
 * Created By: Sugeng
 * Date: 07/12/18
 * Time: 16.54
 */
class PartNameTransformer extends Transformer
{
    public function transform(Sparepart $sparepart): array
    {
        return [
            "id" => (int) $sparepart->id,
            "name" => $sparepart->name,
            "options" => [
                [
                    "id" => (int) $sparepart->id,
                    "text" => $sparepart->name,
                ]
            ]
        ];
    }
}