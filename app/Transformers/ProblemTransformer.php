<?php namespace App\Transformers;

use App\Models\WorkOrderProblem;
use Flugg\Responder\Transformers\Transformer;

/**
 * Created By: Sugeng
 * Date: 07/12/18
 * Time: 16.40
 */
class ProblemTransformer extends Transformer
{
    public function transform(WorkOrderProblem $problem): array
    {
        return [
            "id"          => (int)$problem->id,
            "name"        => $problem->name,
            "description" => $problem->description
        ];
    }
}