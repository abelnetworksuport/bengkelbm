<?php namespace App\Transformers;

use App\Models\Service;
use Flugg\Responder\Transformers\Transformer;

/**
 * Created By: Sugeng
 * Date: 07/12/18
 * Time: 16.46
 */
class ServiceNameTransformer extends Transformer
{
    public function transform(Service $service): array
    {
        return [
            "id" => $service->id,
            "name" => $service->name,
            "options" => [
                [
                    "id" => $service->id,
                    "text" => $service->name
                ]
            ]
        ];
    }
}