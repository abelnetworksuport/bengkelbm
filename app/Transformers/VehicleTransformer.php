<?php namespace App\Transformers;

use App\Models\Vehicle;
use Flugg\Responder\Transformers\Transformer;

/**
 * Created By: Sugeng
 * Date: 07/12/18
 * Time: 16.22
 */
class VehicleTransformer extends Transformer
{
    protected $load = [
        "brand"       => BrandTransformer::class,
        "transmision" => TransmissionTransformer::class,
        "fuel"        => FuelTransformer::class,
        "carType"     => CarTypeTransformer::class,
        "workOrders"  => WorkOrderSlimTransformer::class
    ];

    protected $relations = [
        "owner" => OwnerTransformer::class
    ];

    public function transform(Vehicle $vehicle): array
    {
        return [
            "id"                 => (int)$vehicle->id,
            "vin_number"         => $vehicle->vin_number,
            "plate_number"       => $vehicle->plate_number,
            "engine_number"      => $vehicle->engine_number,
            "brand_id"           => $vehicle->brand_id,
            "brand_series"       => $vehicle->brand_series,
            "build_year"         => $vehicle->build_year,
            "colour"             => $vehicle->colour,
            "millage"            => $vehicle->millage,
            "transmision_id"     => $vehicle->transmision_id,
            "fuel_id"            => $vehicle->fuel_id,
            "car_type_id"        => $vehicle->car_type_id,
            "last_service"       => (!is_null($vehicle->last_service)) ? $vehicle->last_service->format("Y-m-d") : null,
            "last_service_human" => (!is_null($vehicle->last_service)) ? $vehicle->last_service->format("d F Y") : null,
            "registered"         => $vehicle->registered->format("Y-m-d"),
            "registered_human"   => $vehicle->registered->format("d F Y")
        ];
    }
}