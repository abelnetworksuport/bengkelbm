<?php namespace App\Transformers;

use App\Models\WorkOrderService;
use Flugg\Responder\Transformers\Transformer;

/**
 * Created By: Sugeng
 * Date: 07/12/18
 * Time: 16.43
 */
class ServiceTransformer extends Transformer
{
    protected $load = [
        "service" => ServiceNameTransformer::class
    ];

    public function transform(WorkOrderService $service): array
    {
        return [
            "id"              => (int)$service->id,
            "service_id"      => (int)$service->service_id,
            "estimated_price" => (int)$service->estimated_price,
            "is_active"       => (boolean)$service->status,
            "notes"           => $service->notes
        ];
    }
}