<?php namespace App\Transformers;

use App\Models\WorkOrder;
use Flugg\Responder\Transformers\Transformer;

/**
 * Created By: Sugeng
 * Date: 07/12/18
 * Time: 16.02
 */
class WorkOrderTransformer extends Transformer
{
    protected $load
        = [
            "owner"           => OwnerTransformer::class,
            "serviceAdvisor"  => ServiceAdvisorTransformer::class,
            "vehicle"         => VehicleTransformer::class,
            "problems"        => ProblemTransformer::class,
            "services"        => ServiceTransformer::class,
            "parts"           => SparepartsTransformer::class,
            "mechanics"       => MechanicTransformer::class,
            "statusLogs"      => WorkOrderStatusTransformer::class,
            "service_manuals" => ServiceManualTransformer::class
        ];

    public function transform(WorkOrder $workOrder): array
    {
        return [
            "id"                 => (int)$workOrder->id,
            "code"               => $workOrder->code,
            "plate_number"       => $workOrder->plate_number,
            "vin_number"         => $workOrder->vin_number,
            "engine_number"      => $workOrder->engine_number,
            "owner_id"           => $workOrder->owner_id,
            "service_advisor_id" => $workOrder->service_advisor_id,
            "status_id"          => $workOrder->status_id,
            "driver"             => $workOrder->driver,
            "driver_phone"       => $workOrder->driver_phone,
            "latest_millage"     => $workOrder->latest_millage,
            "service_price"      => (int)$workOrder->service_price,
            "service_discount"   => (int)$workOrder->service_discount,
            "part_discount"      => (int)$workOrder->part_discount,
            "part_price"         => (int)$workOrder->part_price,
            "total_price"        => ($workOrder->service_price + $workOrder->part_price) - ($workOrder->service_discount + $workOrder->part_discount),
            "minutes_estimated"  => (int)$workOrder->minutes_estimated,
            "estimated_time"     => [
                "hours"   => ($workOrder->minutes_estimated > 0) ? floor(($workOrder->minutes_estimated / 60) % 60) : 0,
                "minutes" => ($workOrder->minutes_estimated > 0) ? floor($workOrder->minutes_estimated % 60) : 0
            ],
            "car_return_from"    => $workOrder->car_return_from,
            "arrived_date"       => $workOrder->created_at->format("d F Y"),
            "arrived_time"       => $workOrder->created_at->format("H:i:s"),
            "revision"           => $workOrder->revision
        ];
    }

}