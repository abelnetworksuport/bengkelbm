<?php namespace App\Transformers;

use App\Models\WorkOrder;
use Flugg\Responder\Transformers\Transformer;

/**
 * Created By: Sugeng
 * Date: 2018-12-19
 * Time: 13:29
 */
class WorkOrderSlimTransformer extends Transformer
{
    protected $relations
        = [
            "problems"   => ProblemTransformer::class,
            "services"   => ServiceTransformer::class,
            "parts"      => SparepartsTransformer::class,
            "mechanics"  => MechanicTransformer::class,
            "statusLogs" => WorkOrderStatusTransformer::class
        ];

    protected $load
        = [
            "status" => StatusWOTransformer::class
        ];

    public function transform(WorkOrder $workOrder): array
    {
        return [
            "id"                 => (int)$workOrder->id,
            "code"               => $workOrder->code,
            "plate_number"       => $workOrder->plate_number,
            "vin_number"         => $workOrder->vin_number,
            "engine_number"      => $workOrder->engine_number,
            "owner_id"           => $workOrder->owner_id,
            "service_advisor_id" => $workOrder->service_advisor_id,
            "status_id"          => $workOrder->status_id,
            "driver"             => $workOrder->driver,
            "driver_phone"       => $workOrder->driver_phone,
            "latest_millage"     => $workOrder->latest_millage,
            "service_price"      => (int)$workOrder->service_price,
            "part_price"         => (int)$workOrder->part_price,
            "minutes_estimated"  => (int)$workOrder->minutes_estimated,
            "estimated_time"     => [
                "hours"   => ($workOrder->minutes_estimated > 0) ? floor(($workOrder->minutes_estimated / 60) % 60) : 0,
                "minutes" => ($workOrder->minutes_estimated > 0) ? floor($workOrder->minutes_estimated % 60) : 0
            ],
            "time_finished"      => (!empty($workOrder->minutes_finished)) ? $workOrder->minutes_finished->format('d F Y H:i:s') : "",
            "total_price"        => (int)$workOrder->service_price + (int)$workOrder->part_price,
            "car_return_from"    => $workOrder->car_return_from,
            "arrived_date"       => $workOrder->created_at->format("d F Y"),
            "arrived_time"       => $workOrder->created_at->format("H:i:s")
        ];
    }
}