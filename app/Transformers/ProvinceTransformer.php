<?php namespace App\Transformers;
use App\Models\Province;
use Flugg\Responder\Transformers\Transformer;

/**
 * Created By: Sugeng
 * Date: 07/12/18
 * Time: 17.09
 */
class ProvinceTransformer extends Transformer
{
    public function Transform(Province $province): array
    {
        return [
            "id" => (int) $province->id,
            "name" => $province->name,
            "options" => [
                [
                    "id" => (int) $province->id,
                    "text" => $province->name,
                ]
            ]
        ];
    }
}