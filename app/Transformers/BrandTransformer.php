<?php namespace App\Transformers;

use App\Models\CarBrand;
use Flugg\Responder\Transformers\Transformer;

/**
 * Created By: Sugeng
 * Date: 07/12/18
 * Time: 16.37
 */
class BrandTransformer extends Transformer
{
    public function transform(CarBrand $master): array
    {
        return [
            "id"   => $master->id,
            "name" => strtoupper($master->name),
            "option" => [
                [
                    "id"   => $master->id,
                    "text" => strtoupper($master->name),
                ]
            ]
        ];
    }
}