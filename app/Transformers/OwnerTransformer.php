<?php namespace App\Transformers;

use App\Models\Owner;
use Flugg\Responder\Transformers\Transformer;

/**
 * Created By: Sugeng
 * Date: 07/12/18
 * Time: 16.13
 */
class OwnerTransformer extends Transformer
{
    protected $load
        = [
            "salutation" => SalutationTransformer::class,
            "province"   => ProvinceTransformer::class,
            "district"   => DistrictTransformer::class,
        ];

    protected $relations
        = [
            "vehicles" => VehicleTransformer::class,
        ];

    public function transform(Owner $owner): array
    {
        return [
            "id"               => (int)$owner->id,
            "salutation_id"    => (int)$owner->salutation_id,
            "fullname"         => $owner->fullname,
            "is_insurance"     => $owner->use_insurance,
            "owner_group"      => $owner->owner_group,
            "insurance_name"   => $owner->insurance_name,
            "address"          => $owner->address,
            "zip_number"       => $owner->zip_code,
            "phone_number"     => $owner->phone_number,
            "province_id"      => $owner->province_id,
            "district_id"      => $owner->district_id,
            "email"            => $owner->email,
            "registered_human" => $owner->registered->format("d F Y")
        ];
    }
}