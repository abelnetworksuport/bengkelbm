<?php namespace App\Transformers;

use App\Models\WorkOrder;
use App\Transformers\Revision\ProblemRevisionTransform;
use App\Transformers\Revision\ServiceRevisionTransform;
use App\Transformers\Revision\SparepartRevisionTransform;
use Flugg\Responder\Transformers\Transformer;

/**
 * Created By: Sugeng
 * Date: 2019-01-11
 * Time: 04:24
 */
class WorkOrderRevisionTransformer extends Transformer
{
    protected $load
        = [
            "owner"           => OwnerTransformer::class,
            "vehicle"         => VehicleTransformer::class,
            "problems"        => ProblemRevisionTransform::class,
            "services"        => ServiceRevisionTransform::class,
            "parts"           => SparepartRevisionTransform::class,
            "service_manuals" => ServiceManualTransformer::class
        ];

    public function transform(WorkOrder $workOrder): array
    {
        return [
            "id"                          => (int)$workOrder->id,
            "code"                        => $workOrder->code,
            "plate_number"                => $workOrder->plate_number,
            "vin_number"                  => $workOrder->vin_number,
            "engine_number"               => $workOrder->engine_number,
            "owner_id"                    => $workOrder->owner_id,
            "service_advisor_id"          => $workOrder->service_advisor_id,
            "status_id"                   => $workOrder->status_id,
            "driver"                      => $workOrder->driver,
            "driver_phone"                => $workOrder->driver_phone,
            "latest_millage"              => $workOrder->latest_millage,
            "service_price"               => (int)$workOrder->service_price,
            "service_discount_flat"       => (int)$workOrder->service_discount_flat,
            "service_discount_percentage" => (int)$workOrder->service_discount_percentage,
            "service_discount"            => (int)$workOrder->service_discount,
            "part_discount_flat"          => (int)$workOrder->part_discount_flat,
            "part_discount_percentage"    => (int)$workOrder->part_discount_percentage,
            "part_discount"               => (int)$workOrder->part_discount,
            "part_price"                  => (int)$workOrder->part_price,
            "minutes_estimated"           => (int)$workOrder->minutes_estimated,
            "car_return_from"             => $workOrder->car_return_from,
            "arrived_date"                => $workOrder->created_at->format("d F Y"),
            "arrived_time"                => $workOrder->created_at->format("H:i:s")
        ];
    }

}