<?php namespace App\Transformers;

use App\Models\Position;
use Flugg\Responder\Transformers\Transformer;

/**
 * Created By: Sugeng
 * Date: 2018-12-18
 * Time: 10:50
 */
class PositionOptionTransformer extends Transformer
{
    public function transform(Position $position)
    {
        return [
            [
                "id"       => (int)$position->id,
                "text"     => "({$position->initials}) {$position->name}",
            ]
        ];
    }
}