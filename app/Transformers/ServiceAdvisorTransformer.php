<?php namespace App\Transformers;

use App\Models\User;
use Flugg\Responder\Transformers\Transformer;

/**
 * Created By: Sugeng
 * Date: 07/12/18
 * Time: 17.13
 */
class ServiceAdvisorTransformer extends Transformer
{
    public function transform(User $employee): array
    {
        return [
            "id"       => (int)$employee->id,
            "fullname" => $employee->name
        ];
    }
}