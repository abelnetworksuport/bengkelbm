<?php namespace App\Transformers;

use App\Models\Master;
use Flugg\Responder\Transformers\Transformer;

/**
 * Created By: Sugeng
 * Date: 07/12/18
 * Time: 16.36
 */
class CarTypeTransformer extends Transformer
{
    public function transform(Master $master): array
    {
        return [
            "id" => $master->code,
            "name" => $master->name
        ];
    }
}