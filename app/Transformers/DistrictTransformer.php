<?php namespace App\Transformers;

use App\Models\District;
use Flugg\Responder\Transformers\Transformer;

/**
 * Created By: Sugeng
 * Date: 07/12/18
 * Time: 17.12
 */
class DistrictTransformer extends Transformer
{
    public function transform(District $district)
    {
        return [
            "id" => (int) $district->id,
            "name" => $district->name,
            "options" => [
                [
                    "id" => (int) $district->id,
                    "text" => $district->name,
                ]
            ]
        ];
    }
}