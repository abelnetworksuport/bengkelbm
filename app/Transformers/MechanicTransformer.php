<?php namespace App\Transformers;

use App\Models\WorkOrderMechanic;
use Flugg\Responder\Transformers\Transformer;

/**
 * Created By: Sugeng
 * Date: 07/12/18
 * Time: 16.56
 */
class MechanicTransformer extends Transformer
{
    protected $load = [
        "mechanicName" => EmployeeTransformer::class
    ];

    public function transform(WorkOrderMechanic $mechanic): array
    {
        return [
            "id"              => (int)$mechanic->id,
            "mechanic_id"     => (int)$mechanic->mechanic_id,
            "working_minutes" => (int)$mechanic->working_minutes,
            "notes"           => $mechanic->notes
        ];
    }
}