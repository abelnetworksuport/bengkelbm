<?php namespace App\Transformers;

use App\Models\Employee;
use Flugg\Responder\Transformers\Transformer;

/**
 * Created By: Sugeng
 * Date: 07/12/18
 * Time: 16.58
 */
class EmployeeTransformer extends Transformer
{
    protected $load
        = [
            "position" => PositionOptionTransformer::class
        ];

    public function transform(Employee $employee): array
    {
        return [
            "id"                   => (int)$employee->id,
            "code"                 => $employee->code,
            "fullname"             => $employee->fullname,
            "salutation_id"        => $employee->salutation_id,
            "initials"             => $employee->initials,
            "address"              => $employee->address,
            "phone_number"         => $employee->phone_number,
            "position_id"          => $employee->position_id,
            "identity_card_number" => $employee->identity_card_number
        ];
    }
}