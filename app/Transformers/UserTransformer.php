<?php namespace App\Transformers;

use Flugg\Responder\Transformers\Transformer;
use App\Models\User;

/**
 * Created By: Sugeng
 * Date: 05/12/18
 * Time: 13.18
 */
class UserTransformer extends Transformer
{
    protected $load
        = [
            'roles'      => RoleTransformer::class,
        ];

    public function transform(User $user): array
    {
        $role = $user->roles()->first();

        return [
            'id'       => $user->id,
            'uuid'     => $user->uuid,
            'name'     => $user->name,
            'email'    => $user->email,
            'username' => $user->username,
            'role'     => $role->id
        ];
    }
}