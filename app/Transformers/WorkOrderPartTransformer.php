<?php namespace App\Transformers;

use App\Models\WorkOrderParts;
use Flugg\Responder\Transformers\Transformer;

/**
 * Created By: Sugeng
 * Date: 2019-02-19
 * Time: 13:51
 */
class WorkOrderPartTransformer extends Transformer
{
    public function transform(WorkOrderParts $part)
    {
        return [
            "id"              => (int)$part->id,
            "work_order_id"   => (int)$part->work_order_id,
            "part_id"         => $part->sparepart_id,
            "part_name"       => $part->part_name,
            "estimated_price" => $part->estimated_price,
            "quantities"      => $part->quantities,
            "quantity_id"     => $part->quantity_id,
            "qty_text"        => $part->qty_label,
            "label"           => [
                [
                    'id'   => $part->quantity_id,
                    'text' => $part->qty_label
                ]
            ],
            "qty_label"       => $part->qty_label,
            "notes"           => $part->notes
        ];
    }
}