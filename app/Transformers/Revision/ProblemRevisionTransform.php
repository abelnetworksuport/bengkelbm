<?php namespace App\Transformers\Revision;

use App\Models\WorkOrderProblem;
use Flugg\Responder\Transformers\Transformer;

/**
 * Created By: Sugeng
 * Date: 2019-01-11
 * Time: 04:27
 */
class ProblemRevisionTransform extends Transformer
{
    public function transform(WorkOrderProblem $problem): array
    {
        return [
            "id"            => (int)$problem->id,
            "work_order_id" => (int)$problem->work_order_id,
            "problem"       => $problem->name,
            "description"   => $problem->description
        ];
    }
}