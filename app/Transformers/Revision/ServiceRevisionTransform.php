<?php namespace App\Transformers\Revision;

use App\Models\WorkOrderService;
use Flugg\Responder\Transformers\Transformer;

/**
 * Created By: Sugeng
 * Date: 2019-01-11
 * Time: 04:28
 */
class ServiceRevisionTransform extends Transformer
{
    public function transform(WorkOrderService $service): array
    {
        $detail = $service->service()->first();

        return [
            "id"              => (int)$service->id,
            "work_order_id"   => (int)$service->work_order_id,
            "service_id"      => $service->service_id,
            "service"         => [
                [
                    'id'   => (isset($detail->id)) ? $detail->id : null,
                    'text' => (isset($detail->name)) ? $detail->name : null,
                ]
            ],
            'fru'             => (int)$service->fru,
            'rate'            => (int)$service->rate,
            "service_text"    => (isset($detail->name)) ? $detail->name : null,
            "estimated_price" => $service->estimated_price,
        ];
    }
}
