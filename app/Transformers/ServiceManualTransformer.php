<?php namespace App\Transformers;

use App\Models\WorkOrderServiceManual;
use Flugg\Responder\Transformers\Transformer;

/**
 * Created By: Sugeng
 * Date: 2019-01-11
 * Time: 03:02
 */
class ServiceManualTransformer extends Transformer
{
    public function transform(WorkOrderServiceManual $service): array
    {
        return [
            "id"              => (int)$service->id,
            "name"            => $service->name,
            "estimated_price" => (int)$service->estimated_price,
            "fru"             => (int)$service->fru,
            "rate"            => (int)$service->rate
        ];
    }
}