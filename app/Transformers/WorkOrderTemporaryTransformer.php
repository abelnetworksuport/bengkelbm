<?php namespace App\Transformers;
use App\Models\WorkOrderTemp;
use Flugg\Responder\Transformers\Transformer;

/**
 * Created By: Sugeng
 * Date: 2019-02-20
 * Time: 14:10
 */
class WorkOrderTemporaryTransformer extends Transformer
{
    protected $load
        = [
            "owner"           => OwnerTransformer::class,
            "vehicle"         => VehicleTransformer::class,
        ];

    public function transform(WorkOrderTemp $workOrder): array
    {
        return [
            "id"                          => (int)$workOrder->id,
            "code"                        => $workOrder->code,
            "plate_number"                => $workOrder->plate_number,
            "vin_number"                  => $workOrder->vin_number,
            "engine_number"               => $workOrder->engine_number,
            "owner_id"                    => $workOrder->owner_id,
            "service_advisor_id"          => $workOrder->service_advisor_id,
            "status_id"                   => $workOrder->status_id,
            "driver"                      => $workOrder->driver,
            "driver_name"                 => $workOrder->driver,
            "driver_phone"                => $workOrder->driver_phone,
            "latest_millage"              => $workOrder->latest_millage,
            "millage"                     => $workOrder->latest_millage,
            "service_price"               => (int)$workOrder->service_price,
            "service_discount_flat"       => (int)$workOrder->service_discount_flat,
            "service_discount_percentage" => (int)$workOrder->service_discount_percentage,
            "service_discount"            => (int)$workOrder->service_discount,
            "part_discount_flat"          => (int)$workOrder->part_discount_flat,
            "part_discount_percentage"    => (int)$workOrder->part_discount_percentage,
            "part_discount"               => (int)$workOrder->part_discount,
            "part_price"                  => (int)$workOrder->part_price,
            "minutes_estimated"           => (int)$workOrder->minutes_estimated,
            "arrived_date"                => $workOrder->created_at->format("d F Y"),
            "arrived_time"                => $workOrder->created_at->format("H:i:s"),
            "services"                    => $this->serviceFormat($workOrder->services),
            "problems"                    => $this->problemFormat($workOrder->problems),
            "parts"                       => $this->sparepartFormat($workOrder->spareparts),
            "service_manuals"             => []
        ];
    }

    protected function serviceFormat($services)
    {
        if (! is_array($services)) {
            $services = json_decode($services, true);
        }

        $result = [];
        foreach ($services as $service) {
            $result[] = [
                "id"              => (int)$service['id'],
                "service_id"      => (int)$service['id'],
                "service"         => [
                    [
                        'id'   => $service['id'],
                        'text' => $service['service_text'],
                    ]
                ],
                "fru"             => (isset($service['fru'])) ? $service['fru'] : 1,
                "rate"            => (isset($service['fru'])) ? str_replace('.', '', $service['rate']) : $service['estimated_price'],
                "service_text"    => $service['service_text'],
                "estimated_price" => str_replace('.', '', $service['estimated_price']),
                //"notes"           => $service['notes']
            ];
        }


        return $result;
    }

    protected function sparepartFormat($parts)
    {
        if (! is_array($parts)) {
            $parts = json_decode($parts, true);
        }

        $result = [];
        foreach ($parts as $part) {
            $result[] = [
                "id"              => (int)$part['id'],
                "part_name"       => $part['part_name'],
                "estimated_price" => str_replace('.', '', $part['estimated_price']),
                "quantities"      => $part['quantities'],
                "quantity_id"     => $part['quantity_id'],
                "qty_text"        => $part['qty_text'],
                "label"           => [
                    [
                        'id'   => $part['quantity_id'],
                        'text' => $part['qty_text']
                    ]
                ],
                "qty_label"       => $part['qty_text']
            ];
        }

        return $result;
    }

    protected function ProblemFormat($problems)
    {

        if (! is_array($problems)) {
            $problems = json_decode($problems, true);
        }

        $result = [];
        foreach ($problems as $problem) {
            $result[] = [
                "id"              => (int)$problem['id'],
                "problem"         => $problem['problem'],
                "description"     => $problem['description']
            ];
        }

        return $result;
    }
}