<?php namespace App\Transformers;

use App\Models\WorkOrderParts;
use Flugg\Responder\Transformers\Transformer;

/**
 * Created By: Sugeng
 * Date: 07/12/18
 * Time: 16.51
 */
class SparepartsTransformer extends Transformer
{
    protected $load = [
       // "sparepart" => PartNameTransformer::class
    ];

    public function transform(WorkOrderParts $parts): array
    {
        return [
            "id"              => (int)$parts->id,
            "sparepart_id"    => (int)$parts->sparepart_id,
            "part_name"       => $parts->part_name,
            "estimated_price" => (int)$parts->estimated_price,
            "quantities"      => (int)$parts->quantities,
            "qty_label"       => $parts->qty_label,
            "service_status"  => $parts->service_status
        ];
    }
}