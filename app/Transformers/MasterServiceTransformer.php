<?php namespace App\Transformers;

use App\Models\Service;
use Flugg\Responder\Transformers\Transformer;

/**
 * Created By: Sugeng
 * Date: 2018-12-18
 * Time: 12:05
 */
class MasterServiceTransformer extends Transformer
{
    public function transform(Service $service)
    {
        return [
            "id"              => (int)$service->id,
            "name"            => $service->name,
            "estimated_price" => $service->estimated_price,
            "weight"          => $service->weight
        ];
    }
}