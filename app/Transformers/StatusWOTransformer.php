<?php namespace App\Transformers;

use App\Models\Status;
use Flugg\Responder\Transformers\Transformer;

/**
 * Created By: Sugeng
 * Date: 2018-12-18
 * Time: 13:13
 */
class StatusWOTransformer extends Transformer
{
    public function transform(Status $status)
    {
        return [
            "id"     => (int)$status->id,
            "code"   => $status->code,
            "name"   => $status->name,
            "colour" => $status->colour
        ];
    }
}