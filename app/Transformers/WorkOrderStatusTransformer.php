<?php namespace App\Transformers;

use App\Models\WorkOrderStatus;
use Flugg\Responder\Transformers\Transformer;

/**
 * Created By: Sugeng
 * Date: 07/12/18
 * Time: 17.03
 */
class WorkOrderStatusTransformer extends Transformer
{
    protected $load
        = [
            "statusName" => StatusWOTransformer::class
        ];

    public function transform(WorkOrderStatus $status): array
    {
        return [
            "id"           => (int)$status->id,
            "status_id"    => (int)$status->status,
            "changed_by"   => $status->changed_by,
            "notes"        => $status->notes,
            "created_date" => $status->created_at->format("d F Y"),
            "created_time" => $status->created_at->format("H:i:s")
        ];
    }
}