<?php namespace App\Transformers;
/**
 * Created By: Sugeng
 * Date: 2018-12-18
 * Time: 09:05
 */
use Flugg\Responder\Transformers\Transformer;
use Spatie\Permission\Models\Role;

/**
 * Created By: Sugeng
 * Date: 05/12/18
 * Time: 13.31
 */
class RoleTransformer extends Transformer
{
    public function transform(Role $role): array
    {
        return [
            "id"      => (int)$role->id,
            "text"    => $role->name,
        ];
    }
}