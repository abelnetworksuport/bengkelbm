<?php
/**
 * Created By: Sugeng
 * Date: 12/11/18
 * Time: 09.08
 */
namespace App\Libraries\Traits;

trait HasUserCreateUpdate {
    protected static function bootHasUserCreateUpdate()
    {
        static::creating(function ($model) {
            try {
                $model->created_by = \Auth::user()->username;
            } catch (UnsatisfiedDependencyException $e) {
                abort(500, $e->getMessage());
            }
        });

        static::updating(function ($model) {
            try {
                $model->updated_by = \Auth::user()->username;
            } catch (UnsatisfiedDependencyException $e) {
                abort(500, $e->getMessage());
            }
        });
    }
}