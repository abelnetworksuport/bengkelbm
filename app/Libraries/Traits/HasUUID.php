<?php
/**
 * Created By: Sugeng
 * Date: 12/11/18
 * Time: 08.56
 */

namespace App\Libraries\Traits;

use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;
use Ramsey\Uuid\Uuid as Generator;

trait HasUUID
{
    protected static function bootHasUUID()
    {
        static::creating(function ($model) {
            $uuid_field_name = $model->getUUIDFieldName();
            try {
                $model->$uuid_field_name = Generator::uuid4()->toString();
            } catch (UnsatisfiedDependencyException $e) {
                abort(500, $e->getMessage());
            }
        });
    }

    public function getUUIDFieldName()
    {
        if (!empty($this->uuidFieldName)) {
            return $this->uuidFieldName;
        }

        return 'uuid';
    }

    public function scopeByUUID($query, $uuid)
    {
        return $query->where($this->getUUIDFieldName(), $uuid);
    }

    public static function findByUuid($uuid)
    {
        return static::byUUID($uuid)->first();
    }
}