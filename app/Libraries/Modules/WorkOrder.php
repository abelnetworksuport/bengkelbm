<?php namespace App\Libraries\Modules;

use App\Events\WorkOrderChangedEvent;
use App\Models\Owner;
use App\Models\WorkOrder as WorkOrderModel;
use Carbon\Carbon;

/**
 * Created By: Sugeng
 * Date: 06/11/18
 * Time: 14.50
 */
class WorkOrder
{
    protected $owner;
    protected $vehicle;
    protected $problems;
    protected $services;
    protected $parts;
    protected $service_manuals;
    protected $service_discount;
    protected $part_discount;
    protected $temp;

    public function __construct(array $request)
    {
        $this->owner            = $request['owner'];
        $this->vehicle          = $request['vehicle'];
        $this->problems         = $request['problems'];
        $this->services         = $request['services'];
        $this->service_manuals  = $request['service_manuals'];
        $this->parts            = $request['parts'];
        $this->service_discount = $request['serviceDiscount'];
        $this->part_discount    = $request['partDiscount'];
        $this->temp             = (isset($request['woTemp'])) ? $request['woTemp'] : "";
    }

    public function update($id)
    {
        $work_order = WorkOrderModel::find($id);
        $revision   = $work_order->revision + 1;

        if ($work_order) {
            $work_order->problems()->delete();
            $work_order->services()->delete();
            $work_order->serviceManuals()->delete();
            $work_order->parts()->delete();

            $work_order->problems()->createMany($this->prepareStoreProblems());
            $work_order->services()->createMany($this->prepareStoreServices());
            $work_order->serviceManuals()->createMany($this->prepareStoreServiceManual());
            $work_order->parts()->createMany($this->prepareStoreParts());
        }

        $work_order->update([
            'service_discount'            => $this->service_discount['value'],
            'service_discount_flat'       => $this->service_discount['flat'],
            'service_discount_percentage' => $this->service_discount['percentage'],
            'part_discount'               => $this->part_discount['value'],
            'part_discount_flat'          => $this->part_discount['flat'],
            'part_discount_percentage'    => $this->part_discount['percentage'],
            'revision'                    => $revision
        ]);

        event(new WorkOrderChangedEvent(WorkOrderModel::find($id)));

        \DB::table('work_order_revision')->insert([
            'work_order_id'        => $id,
            'work_order_code'      => $work_order->code,
            'service_advisor_id'   => auth()->user()->id,
            'service_advisor_name' => auth()->user()->name,
            'created_at'           => Carbon::now()
        ]);

        $workOrder = WorkOrderModel::with('vehicle', 'owner', 'services.service', 'serviceManuals', 'problems', 'serviceAdvisor', 'vehicle.brand')->find($id);

        $data = [
            "code"            => $workOrder->code,
            "created_at"      => $workOrder->created_at,
            "owner_name"      => $workOrder->owner_name,
            "vin_number"      => $workOrder->vehicle->vin_number,
            "engine_number"   => $workOrder->vehicle->engine_number,
            "plate_number"    => $workOrder->vehicle->plate_number,
            "address"         => substr($workOrder->owner->address, 0, 23),
            "colour"          => $workOrder->vehicle->colour,
            "phone_number"    => $workOrder->owner->phone_number,
            "brand"           => $workOrder->vehicle->brand->name,
            "brand_series"    => $workOrder->vehicle->brand_series,
            "driver"          => $workOrder->driver,
            "services"        => $workOrder->services,
            "service_manuals" => $workOrder->serviceManuals,
            "problems"        => $workOrder->problems,
            "millage"         => $workOrder->vehicle->millage,
            "service_advisor" => $workOrder->serviceAdvisor->name,
            "revision"        => $workOrder->revision
        ];


        $pdf        = new \App\Libraries\Reports\WorkOrder\SPK($data);
        $time_print = date("dmYHis");
        $filename   = "app/public/files/perintah-kerja-bengkel.{$time_print}.pdf";
        $pdf->render($filename, 'F');

        return response()->json([
            'status'  => 'success',
            'message' => "Work Order dengan nomor {$work_order->code} telah berhasil direvisi.",
            'data'    => [
                'wo_number' => $work_order->code,
                'url'       => url("download/perintah-kerja-bengkel.{$time_print}.pdf")
            ]
        ]);
    }

    public function create()
    {
        $owner = $this->createOrFindOwner();

        if ($owner) {
            $work_order_number = $this->workOrderCode();

            $work_order = WorkOrderModel::create([
                'code'                        => $work_order_number,
                'owner_id'                    => $owner->id,
                'owner_name'                  => $this->owner['fullname'],
                'vehicle_id'                  => $owner->vehicles[0]->id,
                'plate_number'                => $this->vehicle['plate_number'],
                'service_advisor_id'          => \Auth::user()->id,
                'status_id'                   => 1,
                'driver'                      => $this->vehicle['driver_name'],
                'driver_salutation'           => $this->vehicle['salutation_id'],
                'driver_phone'                => $this->vehicle['driver_phone'],
                'latest_millage'              => (empty($this->vehicle['millage'])) ? 0 : $this->vehicle['millage'],
                'service_discount'            => $this->service_discount['value'],
                'service_discount_flat'       => $this->service_discount['flat'],
                'service_discount_percentage' => $this->service_discount['percentage'],
                'part_discount'               => $this->part_discount['value'],
                'part_discount_flat'          => $this->part_discount['flat'],
                'part_discount_percentage'    => $this->part_discount['percentage']
            ]);

            if ($work_order) {
                $work_order->problems()->createMany($this->prepareStoreProblems());
                $work_order->services()->createMany($this->prepareStoreServices());
                $work_order->serviceManuals()->createMany($this->prepareStoreServiceManual());
                $work_order->parts()->createMany($this->prepareStoreParts());
            }

            event(new WorkOrderChangedEvent(WorkOrderModel::find($work_order->id)));

            \App\Models\WorkOrderTemp::where('id', $this->temp)->update(['status' => '0']);

            $workOrder = WorkOrderModel::with('vehicle', 'owner', 'services.service', 'serviceManuals', 'problems', 'serviceAdvisor', 'vehicle.brand')->find($work_order->id);

            $data = [
                "code"            => $workOrder->code,
                "created_at"      => $workOrder->created_at,
                "owner_name"      => $workOrder->owner_name,
                "vin_number"      => $workOrder->vehicle->vin_number,
                "engine_number"   => $workOrder->vehicle->engine_number,
                "plate_number"    => $workOrder->vehicle->plate_number,
                "address"         => substr($workOrder->owner->address, 0, 23),
                "colour"          => $workOrder->vehicle->colour,
                "phone_number"    => $workOrder->driver_phone,
                "brand"           => $workOrder->vehicle->brand->name,
                "brand_series"    => $workOrder->vehicle->brand_series,
                "driver"          => $workOrder->driver,
                "services"        => $workOrder->services,
                "service_manuals" => $workOrder->serviceManuals,
                "problems"        => $workOrder->problems,
                "millage"         => $workOrder->vehicle->millage,
                "service_advisor" => $workOrder->serviceAdvisor->name,
                "revision"        => null
            ];

            $pdf        = new \App\Libraries\Reports\WorkOrder\SPK($data);
            $time_print = date("dmYHis");
            $filename   = "app/public/files/perintah-kerja-bengkel.{$time_print}.pdf";
            $pdf->render($filename, 'F');

            return response()->json([
                'status'  => 'success',
                'message' => "Work Order dengan nomor {$work_order->code} telah berhasil dibuat.",
                'data'    => [
                    'wo_number' => $work_order->code,
                    'url'       => url("download/perintah-kerja-bengkel.{$time_print}.pdf")
                ]
            ]);
        }

        return response()->json(['status' => 'error', 'message' => 'Terjadi kesalahan. Tidak dapat membuat Work Order']);
    }

    protected function prepareStoreProblems()
    {
        $store = [];

        foreach ($this->problems as $problem) {
            $store[] = [
                'name'        => $problem['problem'],
                'description' => $problem['description']
            ];
        }

        return $store;
    }

    protected function prepareStoreServices()
    {
        $store = [];

        foreach ($this->services as $service) {
            $store[] = [
                'service_id'      => $service['service_id'],
                'estimated_price' => str_replace('.', '', $service['estimated_price']),
                'fru'             => $service['fru'],
                'rate'            => str_replace('.', '', $service['rate'])
            ];
        }

        return $store;
    }

    protected function prepareStoreServiceManual()
    {
        $store = [];

        foreach ($this->service_manuals as $service) {
            $store[] = [
                'name'            => $service['name'],
                'estimated_price' => str_replace('.', '', $service['estimated_price']),
                'fru'             => $service['fru'],
                'rate'            => str_replace('.', '', $service['rate'])
            ];
        }

        return $store;
    }

    protected function prepareStoreParts()
    {
        $store = [];

        foreach ($this->parts as $part) {
            $store[] = [
                'part_name'       => $part['part_name'],
                'estimated_price' => str_replace('.', '', $part['estimated_price']),
                'quantities'      => $part['quantities'],
                'quantity_id'     => (isset($part['quantity_id'])) ? $part['quantity_id'] : "",
                'qty_label'       => $part['qty_text']
            ];
        }

        return $store;
    }

    protected function createOrFindOwner()
    {
        if (!$this->owner['id']) {
            $owner = Owner::create($this->owner);
            $owner->vehicles()->create($this->vehicle);

            return $owner;
        }

        return Owner::with('vehicles')->find($this->owner['id']);
    }

    private function workOrderCode()
    {
        $wo        = WorkOrderModel::selectRaw("MAX(code) as code")->first();
        $last_code = $wo->code;

        return $last_code + 1;
    }
}