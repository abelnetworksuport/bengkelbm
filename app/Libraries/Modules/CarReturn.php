<?php namespace App\Libraries\Modules;

use App\Events\WorkOrderChangedEvent;
use App\Models\WorkOrder;

/**
 * Created By: Sugeng
 * Date: 2018-12-21
 * Time: 09:09
 */
class CarReturn
{
    protected $form = [];

    public function create(array $form)
    {
        $this->form = $form;
        return $this->initialWorkOrder($form['id']);
    }

    protected function initialWorkOrder($workOrderId)
    {
        $workOrder = WorkOrder::find($workOrderId);

        if (!$workOrder) return false;

        $workOrderNumber = $this->workOrderCode();

        $newWO = WorkOrder::create([
            'code'                        => $workOrderNumber,
            'owner_id'                    => $workOrder->owner_id,
            'owner_name'                  => $workOrder->owner_name,
            'vehicle_id'                  => $workOrder->vehicle_id,
            'plate_number'                => $workOrder->plate_number,
            'service_discount_flat'       => $workOrder->service_discount_flat,
            'service_discount_percentage' => $workOrder->service_discount_percentage,
            'service_discount'            => $workOrder->service_discount,
            'part_discount_flat'          => $workOrder->part_discount_flat,
            'part_discount_percentage'    => $workOrder->part_discount_percentage,
            'part_discount'               => $workOrder->part_discount,
            'service_advisor_id'          => auth()->user()->id,
            'status_id'                   => 1,
            'driver'                      => $this->form['driver_name'],
            'driver_salutation'           => $this->form['salutation_id'],
            'driver_phone'                => $this->form['driver_phone'],
            'latest_millage'              => $this->form['millage'],
            'car_return_from'             => $workOrderId,
            'cr_code'                     => $workOrder->code,
            'cr_type'                     => (count($this->form['type']) > 0) ? implode(", ", $this->form['type']) : ""
        ]);

        if ($newWO) {
            $newWO->problems()->createMany($this->prepareStoreProblems($workOrder));
            $newWO->services()->createMany($this->prepareStoreServices($workOrder));
            $newWO->serviceManuals()->createMany($this->prepareStoreServiceManuals($workOrder));
            $newWO->parts()->createMany($this->prepareStoreParts($workOrder));
            $newWO->mechanics()->createMany($this->prepareStoreMechanic($workOrder));
        }

        \App\Models\CarReturn::create([
            'new_work_order_id' => $workOrderNumber,
            'old_work_order_id' => $workOrderId,
            'description'       => $this->form['description'],
            'issued_by'         => auth()->user()->id
        ]);

        event(new WorkOrderChangedEvent(WorkOrder::find($newWO->id)));

        $workOrder = WorkOrder::with('vehicle', 'owner', 'services.service', 'serviceManuals', 'problems', 'serviceAdvisor', 'vehicle.brand')->find($newWO->id);

        $data = [
            "code"            => $workOrder->code,
            "created_at"      => $workOrder->created_at,
            "owner_name"      => $workOrder->owner_name,
            "vin_number"      => $workOrder->vehicle->vin_number,
            "engine_number"   => $workOrder->vehicle->engine_number,
            "plate_number"    => $workOrder->vehicle->plate_number,
            "address"         => substr($workOrder->owner->address, 0, 23),
            "colour"          => $workOrder->vehicle->colour,
            "brand"           => $workOrder->vehicle->brand->name,
            "brand_series"    => $workOrder->vehicle->brand_series,
            "phone_number"    => $workOrder->owner->phone_number,
            "driver"          => $workOrder->driver,
            "services"        => $workOrder->services,
            "service_manuals" => $workOrder->serviceManuals,
            "problems"        => $workOrder->problems,
            "millage"         => $workOrder->vehicle->millage,
            "service_advisor" => $workOrder->serviceAdvisor->name,
            "revision"        => null
        ];

        $pdf        = new \App\Libraries\Reports\WorkOrder\SPK($data);
        $time_print = date("dmYHis");
        $filename   = "app/public/files/perintah-kerja-bengkel.{$time_print}.pdf";
        $pdf->render($filename, 'F');

        return response()->json([
            'status'  => 'success',
            'message' => "Work Order dengan nomor {$workOrder->code} telah berhasil dibuat.",
            'data'    => [
                'wo_number' => $workOrder->code,
                'url'       => url("download/perintah-kerja-bengkel.{$time_print}.pdf")
            ]
        ]);
    }

    protected function prepareStoreProblems($workOrder)
    {
        $problems = $workOrder->problems()->get();

        $store = [];

        foreach ($problems as $problem) {
            $store[] = [
                'name' => $problem['name']
            ];
        }

        return $store;
    }

    protected function prepareStoreServices($workOrder)
    {
        $services = $workOrder->services()->get();

        $store = [];

        foreach ($services as $service) {
            $store[] = [
                'service_id'      => $service['service_id'],
                'estimated_price' => str_replace('.', '', $service['estimated_price']),
                'fru'             => $service['fru'],
                'rate'            => $service['rate']
            ];
        }

        return $store;
    }

    protected function prepareStoreServiceManuals($workOrder)
    {
        $services = $workOrder->serviceManuals()->get();

        $store = [];

        foreach ($services as $service) {
            $store[] = [
                'name'            => $service['name'],
                'estimated_price' => str_replace('.', '', $service['estimated_price']),
                'fru'             => $service['fru'],
                'rate'            => $service['rate']
            ];
        }

        return $store;
    }

    protected function prepareStoreParts($workOrder)
    {
        $parts = $workOrder->parts()->get();

        $store = [];

        foreach ($parts as $part) {
            $store[] = [
                'part_name'       => $part['part_name'],
                'estimated_price' => str_replace('.', '', $part['estimated_price']),
                'quantities'      => $part['quantities'],
                'quantity_id'     => $part['quantity_id'],
                'qty_label'       => $part['qty_label']
            ];
        }

        return $store;
    }

    protected function prepareStoreMechanic($workOrder)
    {
        $mechanics = $workOrder->mechanics()->get();

        $store = [];

        foreach ($mechanics as $mechanic) {
            $store[] = [
                'mechanic_id' => $mechanic['mechanic_id'],
                'notes'       => $mechanic['notes'],
            ];
        }

        return $store;
    }

    private function workOrderCode()
    {
        $wo        = WorkOrder::selectRaw("MAX(code) as code")->first();
        $last_code = $wo->code;

        return $last_code + 1;
    }
}