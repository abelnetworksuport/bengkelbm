<?php namespace App\Libraries\Modules;

use App\Models\Owner;
use App\Models\WorkOrderTemp as WorkOrderModel;

/**
 * Created By: Sugeng
 * Date: 06/11/18
 * Time: 14.50
 */
class WorkOrderTemp
{
    protected $owner;
    protected $vehicle;
    protected $problems;
    protected $services;
    protected $parts;
    protected $service_manuals;
    protected $service_discount;
    protected $part_discount;
    protected $data;

    public function __construct(array $request)
    {
        $this->owner            = $request['owner'];
        $this->vehicle          = $request['vehicle'];
        $this->problems         = $request['problems'];
        $this->services         = $request['services'];
        $this->service_manuals  = $request['service_manuals'];
        $this->parts            = $request['parts'];
        $this->service_discount = $request['serviceDiscount'];
        $this->part_discount    = $request['partDiscount'];
        $this->data             = $request;
    }

    public function create()
    {
        $owner = $this->createOrFindOwner();

        if ($owner) {
            $work_order_number = $this->workOrderCode();

            $work_order = WorkOrderModel::create([
                'code'                        => $work_order_number,
                'owner_id'                    => $owner->id,
                'owner_name'                  => $this->owner['fullname'],
                'vehicle_id'                  => $owner->vehicles[0]->id,
                'plate_number'                => $this->vehicle['plate_number'],
                'service_advisor_id'          => \Auth::user()->id,
                'status_id'                   => 1,
                'driver'                      => $this->vehicle['driver_name'],
                'driver_salutation'           => $this->vehicle['salutation_id'],
                'driver_phone'                => $this->vehicle['driver_phone'],
                'latest_millage'              => $this->vehicle['millage'],
                'service_discount'            => $this->service_discount['value'],
                'service_discount_flat'       => $this->service_discount['flat'],
                'service_discount_percentage' => $this->service_discount['percentage'],
                'part_discount'               => $this->part_discount['value'],
                'part_discount_flat'          => $this->part_discount['flat'],
                'part_discount_percentage'    => $this->part_discount['percentage'],
                'problems'                    => json_encode($this->problems),
                'services'                    => json_encode($this->services),
                'spareparts'                  => json_encode($this->parts)
            ]);

            return $this->freeInspection($work_order);
        }

        return response()->json(['status' => 'error', 'message' => 'Terjadi kesalahan. Tidak dapat membuat Work Order']);
    }

    protected function createOrFindOwner()
    {
        if (!$this->owner['id']) {
            $owner = Owner::create($this->owner);
            $owner->vehicles()->create($this->vehicle);

            return $owner;
        }

        return Owner::with('vehicles')->find($this->owner['id']);
    }

    private function workOrderCode()
    {
        $wo        = WorkOrderModel::selectRaw("MAX(code) as code")->first();
        $last_code = $wo->code;

        return $last_code + 1;
    }

    public function freeInspection($workOrder)
    {
        $pdf = new \App\Libraries\Reports\WorkOrder\CustomerApproval($this->data);
        $pdf->render();

        return response()->json(['status' => 'success', 'url' => url('download/estimasi-wo.pdf'), 'id' => $workOrder->id]);
    }
}