<?php /** @noinspection PhpUndefinedMethodInspection */

namespace App\Libraries;

use App\Models\User;
use Menu as MenuBuilder;

/**
 * Created By: Sugeng
 * Date: 07/10/18
 * Time: 06.53
 */
class Menu
{
    public function buildFor(User $user)
    {
        $slug = str_slug($user->roles->pluck('name')[0]);

        $navigations = config("navigation.menus.{$slug}");

        MenuBuilder::create(config('navigation.name'), function($menu) use ($navigations) {
            $menu->setView(config('navigation.view'));

            foreach ($navigations as $navigation) {
                foreach ($navigation as $key => $modules)
                if ($this->isHeader($key))
                    $menu->header($modules);
                else if ($this->isDivider($key))
                    $menu->divider();
                else if ($this->isModule($key)) {
                    foreach ($modules as $module) {
                        if (isset($module['child'])) {
                            $menu->dropdown($module['title'], function ($sub) use ($module) {
                                foreach ($module['child'] as $child) {
                                    $sub->url($child['url'], $child['title']);
                                }
                            }, 0, [ 'icon' => $module['icon']]);
                        } else {
                            $menu->url($module['url'], $module['title'], [ 'icon' => $module['icon'] ]);
                        }
                    }
                }
            }
        });
    }

    private function isHeader($name)
    {
        return $name === 'header';
    }

    private function isModule($name)
    {
        return $name === 'modules';
    }

    private function isDivider($name)
    {
        return $name === 'divider';
    }
}