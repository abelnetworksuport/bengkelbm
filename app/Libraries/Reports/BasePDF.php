<?php namespace App\Libraries\Reports;

use TCPDF;

/**
 * Created By: Sugeng
 * Date: 09/11/18
 * Time: 09.01
 */
class BasePDF extends TCPDF
{
    public function Header()
    {
        $image_logo = public_path('assets/global/images/logo_bnm.png');
        $this->Image($image_logo, 5, 5, 50, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
        $this->SetFont('linlibertine_rb', 'B', 25);
        $this->Text(60, 5, "PT. BAVARIA MERCINDO MOTOR");
        $this->SetFont('linlibertine_r', '', 16);
        $this->Text(60, 13, "Sales, Service and Parts");
        $this->SetFont('linlibertine_r', '', 10);
        $this->Text(60, 22, "Jl. Ciputat Raya No 25 Tanah Kusir Kebayoran Lama Selatan, Jakarta 12240 (Kostrad)");
        $this->Text(60, 26, "Ph. 72787434 - 36, 7294907, 7294711, Fax. 72895271, 24 Hrs Service 7293378");
        $this->Text(60, 30, "Email: bm_motor@yahoo.com");
        $this->Line(5, 36, 205, 36, ['width' => 0.2]);
        $this->Line(5, 37, 205, 37, ['width' => 0.5]);
    }

    public function Footer() {
        // Position at 15 mm from bottom
        $this->SetY(-10);
        // Set font
        $this->SetFont('helvetica', 'I', 7);
        // Page number
        $this->Cell(50, 10, "Print Date " . date("d F Y H:m:i"));
        $this->Cell(0, 10, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'R', 0, '', 0, false, 'T', 'M');
    }
}