<?php namespace App\Libraries\Reports\WorkOrder;

use App\Libraries\Reports\BasePDF;

/**
 * Created By: Sugeng
 * Date: 09/12/18
 * Time: 19.39
 */
class SPK
{
    protected $pdf;
    protected $data;
    protected $services;
    protected $problems;
    protected $service_manuals;

    public function __construct(array $data)
    {
        $this->pdf             = new BasePDF();
        $this->data            = $data;
        $this->services        = $data['services'];
        $this->service_manuals = $data['service_manuals'];
        $this->problems        = $data['problems'];
    }

    public function render($name = 'perintah-kerja-bengkel.pdf', $mode = "I")
    {
        $this->pdf->SetCreator(PDF_CREATOR);
        $this->pdf->SetAuthor('Sugeng. S');
        $this->pdf->SetTitle('Estimasi Perbaikan Kendaraan');
        $this->pdf->SetSubject('Perbaikan Kendaraan');
        $this->pdf->SetTopMargin(40);
        $this->pdf->SetAutoPageBreak(TRUE, 0);

        $this->pdf->AddPage();

        $this->pdf->SetFont('librebodoni', 'B', 13);

        $revisi = ($this->data['revision'] > 0) ? "REVISI {$this->data['revision']}" : "";

        $this->pdf->Cell(0, 5, "PERINTAH KERJA BENGKEL $revisi", 0, 0, 'C');
        $this->pdf->Ln(10);

        $this->headerInfo();

        if ($mode == 'F') {
            $this->pdf->Output(storage_path($name), $mode);
        } else if ($mode == 'I') {
            $this->pdf->Output($name, "D");
        }
    }

    protected function headerInfo()
    {
        $this->pdf->SetFont('librebodoni', 'B', 8);
        $this->pdf->Cell(15, 5, "No");
        $this->pdf->Cell(3, 5, ':', 0, 0, 'C');
        $this->pdf->Cell(40, 5, $this->data['code']);

        $this->pdf->Cell(28, 5, "No. Chassis");
        $this->pdf->Cell(3, 5, ':', 0, 0, 'C');
        $this->pdf->Cell(40, 5, $this->data['vin_number']);

        $this->pdf->Cell(28, 5, "Hal.");
        $this->pdf->Cell(3, 5, ':', 0, 0, 'C');
        $this->pdf->Cell(0, 5, $this->pdf->getAliasNumPage() . "/" . $this->pdf->getAliasNbPages());
        $this->pdf->Ln(5);

        $this->pdf->Cell(15, 5, "Owner");
        $this->pdf->Cell(3, 5, ':', 0, 0, 'C');
        $this->pdf->Cell(40, 5, substr($this->data['owner_name'], 0, 20));

        $this->pdf->Cell(28, 5, "No. Polisi");
        $this->pdf->Cell(3, 5, ':', 0, 0, 'C');
        $this->pdf->Cell(40, 5, $this->data['plate_number']);

        $this->pdf->Cell(28, 5, "Tgl&Jam Masuk");
        $this->pdf->Cell(3, 5, ':', 0, 0, 'C');
        $this->pdf->Cell(40, 5, $this->data['created_at']);
        $this->pdf->Ln(5);

        $this->pdf->Cell(15, 5, "Alamat");
        $this->pdf->Cell(3, 5, ':', 0, 0, 'C');
        $this->pdf->Cell(40, 5, substr($this->data['address'], 0, 16));

        $this->pdf->Cell(28, 5, "No. Mesin");
        $this->pdf->Cell(3, 5, ':', 0, 0, 'C');
        $this->pdf->Cell(40, 5, $this->data['engine_number']);

        $this->pdf->Cell(28, 5, "SA");
        $this->pdf->Cell(3, 5, ':', 0, 0, 'C');
        $this->pdf->Cell(0, 5, $this->data['service_advisor']);
        $this->pdf->Ln(5);

        $this->pdf->Cell(15, 5, "Telepon");
        $this->pdf->Cell(3, 5, ':', 0, 0, 'C');
        $this->pdf->Cell(40, 5, $this->data['phone_number']);

        $this->pdf->Cell(28, 5, "KM");
        $this->pdf->Cell(3, 5, ':', 0, 0, 'C');
        $this->pdf->Cell(40, 5, $this->data['millage']);

        $this->pdf->Cell(28, 5, "Warna");
        $this->pdf->Cell(3, 5, ':', 0, 0, 'C');
        $this->pdf->Cell(0, 5, $this->data['colour']);
        $this->pdf->Ln(5);

        $this->pdf->Cell(15, 5, "Driver");
        $this->pdf->Cell(3, 5, ':', 0, 0, 'C');
        $this->pdf->Cell(40, 5, substr($this->data['driver'], 0, 20));

        $this->pdf->Cell(28, 5, "Merk/Type");
        $this->pdf->Cell(3, 5, ':', 0, 0, 'C');
        $this->pdf->Cell(40, 5, "{$this->data['brand']}/{$this->data['brand_series']}");

        $this->pdf->Cell(28, 5, "Tgl&Jam Keluar");
        $this->pdf->Cell(3, 5, ':', 0, 0, 'C');
        $this->pdf->Cell(40, 5, "{$this->data['created_at']}");

        $this->pdf->Ln(5);

        $this->pdf->Rect(10, 78, 190, 31);

        $this->pdf->SetFont('librebodoni', 'B', 12);
        $this->pdf->Text(12, 80, "FUEL");
        $this->pdf->Line(15, 98, 44, 98);
        $this->pdf->Text(13, 98, "E");
        $this->pdf->Text(43, 98, "F");

        $this->pdf->SetFont('librebodoni', '', 15);
        $this->pdf->Text(27, 98, "1/2");

        $this->pdf->SetFont('librebodoni', 'B', 7);

        $this->pdf->Text(50, 80, "Service Book");
        $this->pdf->Rect(85, 81, 2, 2);
        $this->pdf->Text(50, 86, "Spare Wheel");
        $this->pdf->Rect(85, 87, 2, 2);
        $this->pdf->Text(50, 92, "Jack");
        $this->pdf->Rect(85, 93, 2, 2);
        $this->pdf->Text(50, 98, "Tools");
        $this->pdf->Rect(85, 99, 2, 2);
        $this->pdf->Text(50, 104, "Radio/Tape");
        $this->pdf->Rect(85, 105, 2, 2);

        $this->pdf->Text(95, 80, "Belonging");
        $this->pdf->Rect(130, 81, 2, 2);
        $this->pdf->Text(95, 86, "STNK");
        $this->pdf->Rect(130, 87, 2, 2);
        $this->pdf->Text(95, 92, "CASH");
        $this->pdf->Rect(130, 93, 2, 2);
        $this->pdf->Text(95, 98, "eToll/eMoney");
        $this->pdf->Rect(130, 99, 2, 2);

        $this->pdf->Text(141, 80, "NOTE:");

        $this->pdf->Line(48, 78, 48, 109);
        $this->pdf->Line(92, 78, 92, 109);
        $this->pdf->Line(138, 78, 138, 109);

        $this->pdf->Rect(10, 109, 190, 10);
        $this->pdf->Rect(10, 109, 190, 10);
        $this->pdf->Rect(10, 119, 190, 170);
        $this->pdf->Rect(10, 119, 190, 170);

        $this->pdf->SetFont('librebodoni', 'B', 10);
        $this->pdf->Text(10, 111, "No.");
        $this->pdf->Line(20, 109, 20, 240);

        $this->pdf->Text(30, 111, "No. Perkerjaan");
        $this->pdf->Line(80, 109, 80, 240);

        $this->pdf->Text(80, 111, "C/W/I");
        $this->pdf->Line(94, 109, 94, 240);

        $this->pdf->Text(130, 111, "Uraian Pekerjaan");

        $ln = 126;
        $this->pdf->SetFont('librebodoni', 'B', 8);

        $this->pdf->Text(96, 120, "*** MASALAH/KELUHAN ***");
        $i = 1;
        foreach ($this->problems as $problem) {
            $this->pdf->Text(96, $ln, "{$i}. {$problem->name}");
            $ln += 6;
            $i++;
        }

        $ln += 6;
        $this->pdf->Text(96, $ln, "*** PERBAIKAN/JASA/SERVICE ***");
        $ln += 6;
        $i  = 1;

        if (count($this->services) > 0) {
            foreach ($this->services as $service) {
                if (isset($service->service->name)) {
                    $this->pdf->Text(96, $ln, "{$i}. {$service->service->name}");
                    $ln += 6;
                    if (!empty($service->service->description)) {
                        $this->pdf->Text(96, $ln, " {$service->service->description}");
                        $ln += 6;
                    }
                    $i++;
                }
            }
        }

        foreach ($this->service_manuals as $service_manual) {
            $this->pdf->Text(96, $ln, "{$i}. {$service_manual->name}");
            $ln += 6;
            $i++;
        }

        $this->pdf->Line(10, 240, 200, 240);

        $this->pdf->Image(public_path('assets/global/images/vehicle.png'), 15, 245, 45);
        $this->pdf->Text(12, 282, "Catatan: Silang Bagian Rusak");

        $this->pdf->SetFont('librebodoni', '', 6);
        $this->pdf->Text(140, 245, "Harap dilakukan perbaikan pada kendaraan");
        $this->pdf->Text(140, 249, "kami sesuai dengan S.P.K dan kami setujui");
        $this->pdf->Text(140, 253, "atas peraturan & kondisi yang disepakati");


        $this->pdf->Text(100, 277, auth()->user()->name);
        $this->pdf->Text(100, 282, "Service Advisor");

        $this->pdf->Text(150, 277, $this->data['owner_name']);
        $this->pdf->Text(150, 282, "Pelanggan/Driver/Owner");
    }
}
