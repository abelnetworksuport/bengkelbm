<?php namespace App\Libraries\Reports\WorkOrder;

/**
 * Created By: Sugeng
 * Date: 09/11/18
 * Time: 09.01
 */
class CustomerApproval
{
    protected $pdf;
    protected $data;

    public function __construct(array $data)
    {
        $this->pdf = new Approval();
        $this->data = $data;
    }

    public function render()
    {
        $this->pdf->SetCreator(PDF_CREATOR);
        $this->pdf->SetAuthor('Sugeng. S');
        $this->pdf->SetTitle('Estimasi Perbaikan Kendaraan');
        $this->pdf->SetSubject('Perbaikan Kendaraan');
        $this->pdf->SetTopMargin(40);

        $this->pdf->AddPage();

        $this->pdf->SetFont('librebodoni', 'B', 13);
        $this->pdf->Cell(0, 5, "ESTIMASI PERBAIKAN KENDARAAN", 0, 0, 'C');
        $this->pdf->Ln(10);

        $this->HeaderPage();

        $service_price = $this->ServiceCell();

        $parts_price = $this->partsCell();

        $total = $service_price + $parts_price;

        $this->TotalCell($total);


        $this->pdf->Output(storage_path("app/public/files/estimasi-wo.pdf"), 'F');
    }

    protected static function terbilang($bilangan)
    {
            $angka = ["", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas"];
            if ($bilangan < 12)
                return " " . $angka[$bilangan];
            elseif ($bilangan < 20)
                return self::terbilang($bilangan - 10) . " belas";
            elseif ($bilangan < 100)
                return self::terbilang($bilangan / 10) . " puluh" . self::terbilang($bilangan % 10);
            elseif ($bilangan < 200)
                return "seratus" . self::terbilang($bilangan - 100);
            elseif ($bilangan < 1000)
                return self::terbilang($bilangan / 100) . " ratus" . self::terbilang($bilangan % 100);
            elseif ($bilangan < 2000)
                return "seribu" . self::terbilang($bilangan - 1000);
            elseif ($bilangan < 1000000)
                return self::terbilang($bilangan / 1000) . " ribu" . self::terbilang($bilangan % 1000);
            elseif ($bilangan < 1000000000)
                return self::terbilang($bilangan / 1000000) . " juta" . self::terbilang($bilangan % 1000000);
    }

    protected function ServiceCell(): int
    {
        $this->pdf->SetFont('librebodoni', 'B', 8);

        $this->pdf->Cell(10, 5, "No.", 1, 0, 'C');
        $this->pdf->Cell(130, 5, "Jasa/Service", 1, 0, 'C');
        $this->pdf->Cell(0, 5, "Jumlah (Rp.)", 1, 0, 'C');
        $this->pdf->Ln(5);

        $this->pdf->SetFont('librebodoni', '', 7.5);

        $i=0; $total = 0;
        foreach ($this->data['services'] as $service) {
            if (!empty($service['service_text'])) {
                $i++;

                $price = str_replace('.', '', $service['estimated_price']);

                $this->pdf->Cell(10, 5, $i, "RL", 0, 'C');
                $this->pdf->Cell(130, 5, $service['service_text'], "R", 0, 'L');
                $this->pdf->Cell(0, 5, number_format($price), "R", 0, 'R');
                $this->pdf->Ln(5);

                $total += $price;
            }
        }

        foreach ($this->data['service_manuals'] as $service) {
            if (!empty($service['name'])) {
                $i++;

                $price = str_replace('.', '', $service['estimated_price']);

                $this->pdf->Cell(10, 5, $i, "RL", 0, 'C');
                $this->pdf->Cell(130, 5, $service['name'], "R", 0, 'L');
                $this->pdf->Cell(0, 5, number_format($price), "R", 0, 'R');
                $this->pdf->Ln(5);

                $total += $price;
            }
        }

        $potongan = ($this->data['serviceDiscount']['value'] > 0) ?
            $this->data['serviceDiscount']['value'] :
            0;

        $potongan_formated = 0;
        if ($potongan > 0) {
            $total = $total - $potongan;
            $potongan_formated = number_format($potongan);
        };

        $this->pdf->SetFont('librebodoni', 'B', 7);
        $this->pdf->Cell(70, 5, "Total Jasa/Service", 1, 0, 'C');
        $this->pdf->Cell(70, 5, "Potongan Rp. {$potongan_formated}", 1, 0, 'C');
        $this->pdf->Cell(0, 5, number_format($total), 1, 0, 'R');
        $this->pdf->Ln(10);

        return $total;
    }

    protected function HeaderPage(): void
    {
        $this->pdf->SetFont('librebodoni', 'B', 8);
        $this->pdf->Cell(20, 5, "TANGGAL");
        $this->pdf->Cell(3, 5, ':', 0, 0, 'C');
        $this->pdf->Cell(90, 5, date("d F Y"));

        $this->pdf->Cell(25, 5, "HAL.");
        $this->pdf->Cell(3, 5, ':', 0, 0, 'C');
        $this->pdf->Cell(0, 5, $this->pdf->getAliasNumPage() . "/" . $this->pdf->getAliasNbPages());
        $this->pdf->Ln(5);

        $this->pdf->Cell(20, 5, "NO. WO");
        $this->pdf->Cell(3, 5, ':', 0, 0, 'C');
        $this->pdf->Cell(90, 5, '');

        $this->pdf->Cell(25, 5, "MODEL/TYPE");
        $this->pdf->Cell(3, 5, ':', 0, 0, 'C');
        $this->pdf->Cell(0, 5, "{$this->data['vehicle_text']['brand_id']}/{$this->data['vehicle']['brand_series']}");
        $this->pdf->Ln(5);

        $this->pdf->Cell(20, 5, "OWNER");
        $this->pdf->Cell(3, 5, ':', 0, 0, 'C');
        $this->pdf->Cell(90, 5, "{$this->data['owner']['fullname']}");

        $this->pdf->Cell(25, 5, "NO. POLISI");
        $this->pdf->Cell(3, 5, ':', 0, 0, 'C');
        $this->pdf->Cell(0, 5, "{$this->data['vehicle']['plate_number']}");
        $this->pdf->Ln(5);

        $this->pdf->Cell(20, 5, "ALAMAT");
        $this->pdf->Cell(3, 5, ':', 0, 0, 'C');
        $this->pdf->Cell(90, 5, "{$this->data['owner']['address']}");

        $this->pdf->Cell(25, 5, "NO. RANGKA");
        $this->pdf->Cell(3, 5, ':', 0, 0, 'C');
        $this->pdf->Cell(0, 5, "{$this->data['vehicle']['vin_number']}");
        $this->pdf->Ln(5);

        $this->pdf->Cell(20, 5, "U/P");
        $this->pdf->Cell(3, 5, ':', 0, 0, 'C');
        $this->pdf->Cell(90, 5, '');

        $this->pdf->Cell(25, 5, "KM.");
        $this->pdf->Cell(3, 5, ':', 0, 0, 'C');
        $this->pdf->Cell(0, 5, "{$this->data['vehicle']['millage']}");
        $this->pdf->Ln(5);

        $this->pdf->Cell(20, 5, "DRIVER.");
        $this->pdf->Cell(3, 5, ':', 0, 0, 'C');
        $this->pdf->Cell(90, 5, "{$this->data['vehicle']['driver_name']}");

        $this->pdf->Cell(25, 5, "TELEPON.");
        $this->pdf->Cell(3, 5, ':', 0, 0, 'C');
        $this->pdf->Cell(0, 5, "{$this->data['vehicle']['driver_phone']}");
        $this->pdf->Ln(7);
    }

    protected function partsCell(): int
    {
        $this->pdf->SetFont('librebodoni', 'B', 8);
        $this->pdf->Cell(10, 5, "No.", 1, 0, 'C');
        $this->pdf->Cell(110, 5, "Parts & Bahan", 1, 0, 'C');
        $this->pdf->Cell(20, 5, "Qty", 1, 0, 'C');
        $this->pdf->Cell(0, 5, "Jumlah (Rp.)", 1, 0, 'C');
        $this->pdf->Ln(5);

        $this->pdf->SetFont('librebodoni', '', 7);

        $i=0; $total = 0;
        foreach ($this->data['parts'] as $part) {
            if (!empty($part['part_name'])) {
                $i++;
                $price = str_replace('.', '', $part['estimated_price']);

                $this->pdf->Cell(10, 5, $i, "RL", 0, 'C');
                $this->pdf->Cell(110, 5, $part['part_name'], "R", 0, 'L');
                $this->pdf->Cell(20, 5, "{$part['quantities']} {$part['qty_text']}", "R", 0, 'C');
                $this->pdf->Cell(0, 5, number_format($price), "R", 0, 'R');
                $this->pdf->Ln(5);

                $total += $price;
            }
        }

        $potongan = ($this->data['partDiscount']['value'] > 0) ?
            $this->data['partDiscount']['value'] :
            0;

        $potongan_formated = 0;
        if ($potongan > 0) {
            $total = $total - $potongan;
            $potongan_formated = number_format($potongan);
        };

        $this->pdf->SetFont('librebodoni', 'B', 8);
        $this->pdf->Cell(70, 5, "Total Parts & Bahan", 1, 0, 'C');
        $this->pdf->Cell(70, 5, "Potongan Rp. {$potongan_formated}", 1, 0, 'C');
        $this->pdf->Cell(0, 5, number_format($total), 1, 0, 'R');
        $this->pdf->Ln(10);

        return $total;
    }

    protected function seal($total)
    {
        if ($total > 1000000) {
            return 6000;
        }

        return 3000;
    }

    protected function TotalCell($total): void
    {

        $seal = $this->seal($total);
        $grand_total = $total + $seal;

        $this->pdf->SetFont('librebodoni', 'B', 8);
        $this->pdf->Cell(130, 6, "Total (Jasa + Parts & Bahan)", "TL", 0, 'L');
        $this->pdf->Cell(10, 6, "Rp.", "T", 0, 'R');
        $this->pdf->Cell(0, 6, number_format($total), 'RTL', 0, 'R');
        $this->pdf->Ln(6);
        $this->pdf->Cell(130, 6, "Materai Rp. ".number_format($seal).",-", "TL", 0, 'L');
        $this->pdf->Cell(10, 6, "Rp.", "T", 0, 'R');
        $this->pdf->Cell(0, 6, number_format($seal), 'RTL', 0, 'R');
        $this->pdf->Ln(6);
        $this->pdf->Cell(130, 6, "Grand Total", "TL", 0, 'L');
        $this->pdf->Cell(10, 6, "Rp.", "T", 0, 'R');
        $this->pdf->Cell(0, 6, number_format($grand_total), 'RTL', 0, 'R');
        $this->pdf->Ln(6);
        $this->pdf->Cell(23, 8, "Terbilang : ", "TLB", 0, 'L');
        $this->pdf->Cell(0, 8, ucfirst(self::terbilang($grand_total)) . " Rupiah", "RTB", 0, 'L');
        $this->pdf->Ln(12);
    }
}