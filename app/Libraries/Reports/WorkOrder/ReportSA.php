<?php namespace App\Libraries\Reports\WorkOrder;
/**
 * Created By: Sugeng
 * Date: 2019-01-11
 * Time: 03:07
 */
class ReportSA
{
    protected $pdf;
    protected $data;
    protected $owner;
    protected $services;
    protected $parts;
    protected $problems;
    protected $workOrder;
    protected $vehicle;
    protected $service_manuals;

    public function __construct(array $data)
    {
        $this->pdf             = new ForSA();
        $this->owner           = $data['owner'];
        $this->vehicle         = $data['vehicle'];
        $this->services        = $data['services'];
        $this->service_manuals = $data['service_manuals'];
        $this->parts           = $data['parts'];
        $this->problems        = $data['problems'];
        $this->workOrder       = $data;
    }

    public function render($name = "report-service-advisor.pdf", $mode = 'F')
    {
        $this->pdf->SetCreator(PDF_CREATOR);
        $this->pdf->SetAuthor('Sugeng. S');
        $this->pdf->SetTitle('Report');
        $this->pdf->SetSubject('Perbaikan Kendaraan');
        $this->pdf->SetTopMargin(40);

        $this->pdf->AddPage();

        $this->pdf->SetFont('librebodoni', 'B', 15);
        $this->pdf->Cell(0, 5, "REPORT SERVICE ADVISOR", 0, 0, 'C');
        $this->pdf->Ln(10);

        $this->HeaderPage();

        $service_price = $this->ServiceCell();

        $parts_price = $this->partsCell();

        $total = $service_price + $parts_price;

        $this->TotalCell($total);

        if ($mode == 'F') {
            $this->pdf->Output(storage_path($name), $mode);
        } else if ($mode == 'I') {
            $this->pdf->Output($name, 'D');
        }
    }

    protected static function terbilang($bilangan)
    {
        $angka = ["", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas"];
        if ($bilangan < 12)
            return " " . $angka[$bilangan];
        elseif ($bilangan < 20)
            return self::terbilang($bilangan - 10) . " belas";
        elseif ($bilangan < 100)
            return self::terbilang($bilangan / 10) . " puluh" . self::terbilang($bilangan % 10);
        elseif ($bilangan < 200)
            return "seratus" . self::terbilang($bilangan - 100);
        elseif ($bilangan < 1000)
            return self::terbilang($bilangan / 100) . " ratus" . self::terbilang($bilangan % 100);
        elseif ($bilangan < 2000)
            return "seribu" . self::terbilang($bilangan - 1000);
        elseif ($bilangan < 1000000)
            return self::terbilang($bilangan / 1000) . " ribu" . self::terbilang($bilangan % 1000);
        elseif ($bilangan < 1000000000)
            return self::terbilang($bilangan / 1000000) . " juta" . self::terbilang($bilangan % 1000000);
    }

    protected function ServiceCell(): int
    {
        $this->pdf->SetFont('librebodoni', 'B', 8);

        $this->pdf->Cell(10, 5, "No.", 1, 0, 'C');
        $this->pdf->Cell(130, 5, "Jasa/Service", 1, 0, 'C');
        $this->pdf->Cell(0, 5, "Jumlah (Rp.)", 1, 0, 'C');
        $this->pdf->Ln(5);

        $this->pdf->SetFont('librebodoni', '', 8);

        $i     = 0;
        $total = 0;
        foreach ($this->services as $service) {
            $i++;

            $price = str_replace('.', '', $service['estimated_price']);
            $name = (isset($service['service'])) ? $service['service']['name'] : "";

            $this->pdf->Cell(10, 5, $i, "RL", 0, 'C');
            $this->pdf->Cell(130, 5, $name, "R", 0, 'L');
            $this->pdf->Cell(0, 5, number_format($price), "R", 0, 'R');
            $this->pdf->Ln(5);

            $total += $price;
        }

        foreach ($this->service_manuals as $service) {
            $i++;

            $price = str_replace('.', '', $service['estimated_price']);

            $this->pdf->Cell(10, 5, $i, "RL", 0, 'C');
            $this->pdf->Cell(130, 5, $service['name'], "R", 0, 'L');
            $this->pdf->Cell(0, 5, number_format($price), "R", 0, 'R');
            $this->pdf->Ln(5);

            $total += $price;
        }

        $potongan = ($this->workOrder['service_discount'] > 0) ?
            $this->workOrder['service_discount'] :
            0;

        $potongan_formated = 0;
        if ($potongan > 0) {
            $total = $total - $potongan;
            $potongan_formated = number_format($potongan);
        };

        $this->pdf->SetFont('librebodoni', 'B', 8);
        $this->pdf->Cell(70, 5, "Total Jasa/Service", 1, 0, 'C');
        $this->pdf->Cell(70, 5, "Potongan Rp. {$potongan_formated}", 1, 0, 'C');
        $this->pdf->Cell(0, 5, number_format($total), 1, 0, 'R');
        $this->pdf->Ln(10);

        return $total;
    }

    protected function HeaderPage(): void
    {
        $this->pdf->SetFont('librebodoni', 'B', 8);
        $this->pdf->Cell(20, 5, "TANGGAL");
        $this->pdf->Cell(3, 5, ':', 0, 0, 'C');
        $this->pdf->Cell(90, 5, date("d F Y"));

        $this->pdf->Cell(25, 5, "HAL.");
        $this->pdf->Cell(3, 5, ':', 0, 0, 'C');
        $this->pdf->Cell(0, 5, $this->pdf->getAliasNumPage() . "/" . $this->pdf->getAliasNbPages());
        $this->pdf->Ln(5);

        $this->pdf->Cell(20, 5, "NO. WO");
        $this->pdf->Cell(3, 5, ':', 0, 0, 'C');
        $this->pdf->Cell(90, 5, $this->workOrder['code']);

        $this->pdf->Cell(25, 5, "MODEL/TYPE");
        $this->pdf->Cell(3, 5, ':', 0, 0, 'C');
        $this->pdf->Cell(0, 5, "{$this->vehicle['brand']['name']}/{$this->vehicle['brand_series']}");
        $this->pdf->Ln(5);

        $this->pdf->Cell(20, 5, "OWNER");
        $this->pdf->Cell(3, 5, ':', 0, 0, 'C');
        $this->pdf->Cell(90, 5, "{$this->owner['fullname']}");

        $this->pdf->Cell(25, 5, "No. POLISI");
        $this->pdf->Cell(3, 5, ':', 0, 0, 'C');
        $this->pdf->Cell(0, 5, "{$this->vehicle['plate_number']}");
        $this->pdf->Ln(5);

        $this->pdf->Cell(20, 5, "ALAMAT");
        $this->pdf->Cell(3, 5, ':', 0, 0, 'C');
        $this->pdf->Cell(90, 5, substr("{$this->owner['address']}", 0, 50));

        $this->pdf->Cell(25, 5, "NO. RANGKA");
        $this->pdf->Cell(3, 5, ':', 0, 0, 'C');
        $this->pdf->Cell(0, 5, "{$this->vehicle['vin_number']}");
        $this->pdf->Ln(5);

        $this->pdf->Cell(20, 5, "U/P");
        $this->pdf->Cell(3, 5, ':', 0, 0, 'C');
        $this->pdf->Cell(90, 5, '');

        $this->pdf->Cell(25, 5, "KM.");
        $this->pdf->Cell(3, 5, ':', 0, 0, 'C');
        $this->pdf->Cell(0, 5, "{$this->workOrder['latest_millage']}");
        $this->pdf->Ln(5);

        $this->pdf->Cell(20, 5, "DRIVER.");
        $this->pdf->Cell(3, 5, ':', 0, 0, 'C');
        $this->pdf->Cell(90, 5, "{$this->workOrder['driver']}");

        $this->pdf->Cell(25, 5, "TELEPON.");
        $this->pdf->Cell(3, 5, ':', 0, 0, 'C');
        $this->pdf->Cell(0, 5, "{$this->workOrder['driver_phone']}");
        $this->pdf->Ln(7);
    }

    protected function partsCell(): int
    {
        $this->pdf->SetFont('librebodoni', 'B', 8);
        $this->pdf->Cell(10, 5, "No.", 1, 0, 'C');
        $this->pdf->Cell(110, 5, "Parts & Bahan", 1, 0, 'C');
        $this->pdf->Cell(20, 5, "Qty", 1, 0, 'C');
        $this->pdf->Cell(0, 5, "Jumlah (Rp.)", 1, 0, 'C');
        $this->pdf->Ln(5);

        $this->pdf->SetFont('librebodoni', '', 8);

        $i     = 0;
        $total = 0;
        foreach ($this->parts as $part) {
            $i++;
            $price = str_replace('.', '', $part['estimated_price']);

            $this->pdf->Cell(10, 5, $i, "RL", 0, 'C');
            $this->pdf->Cell(110, 5, $part['part_name'], "R", 0, 'L');
            $this->pdf->Cell(20, 5, "{$part['quantities']} {$part['qty_label']}", "R", 0, 'C');
            $this->pdf->Cell(0, 5, number_format($price), "R", 0, 'R');
            $this->pdf->Ln(5);

            $total += $price;
        }

        $potongan = ($this->workOrder['part_discount'] > 0) ?
                $this->workOrder['part_discount'] :
            0;

        $potongan_formated = 0;
        if ($potongan > 0) {
            $total = $total - $potongan;
            $potongan_formated = number_format($potongan);
        };

        $this->pdf->SetFont('librebodoni', 'B', 8);
        $this->pdf->Cell(70, 5, "Total Jasa/Service", 1, 0, 'C');
        $this->pdf->Cell(70, 5, "Potongan Rp. {$potongan_formated}", 1, 0, 'C');
        $this->pdf->Cell(0, 5, number_format($total), 1, 0, 'R');
        $this->pdf->Ln(10);

        return $total;
    }

    protected function seal($total)
    {
        if ($total > 1000000) {
            return 6000;
        }

        return 3000;
    }

    protected function TotalCell($total): void
    {

        $seal = $this->seal($total);
        $grand_total = $total + $seal;

        $this->pdf->SetFont('librebodoni', 'B', 8);
        $this->pdf->Cell(130, 6, "Total (Jasa + Parts & Bahan)", "TL", 0, 'L');
        $this->pdf->Cell(10, 6, "Rp.", "T", 0, 'R');
        $this->pdf->Cell(0, 6, number_format($total), 'RTL', 0, 'R');
        $this->pdf->Ln(6);
        $this->pdf->Cell(130, 6, "Materai Rp. ".number_format($seal).",-", "TL", 0, 'L');
        $this->pdf->Cell(10, 6, "Rp.", "T", 0, 'R');
        $this->pdf->Cell(0, 6, number_format($seal), 'RTL', 0, 'R');
        $this->pdf->Ln(6);
        $this->pdf->Cell(130, 6, "Grand Total", "TL", 0, 'L');
        $this->pdf->Cell(10, 6, "Rp.", "T", 0, 'R');
        $this->pdf->Cell(0, 6, number_format($grand_total), 'RTL', 0, 'R');
        $this->pdf->Ln(6);
        $this->pdf->Cell(23, 8, "Terbilang : ", "TLB", 0, 'L');
        $this->pdf->Cell(0, 8, ucfirst(self::terbilang($grand_total)) . " Rupiah", "RTB", 0, 'L');
        $this->pdf->Ln(12);
    }
}