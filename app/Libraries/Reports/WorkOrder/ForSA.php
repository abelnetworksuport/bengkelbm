<?php namespace App\Libraries\Reports\WorkOrder;

use App\Libraries\Reports\BasePDF;

/**
 * Created By: Sugeng
 * Date: 2019-01-11
 * Time: 03:08
 */
class ForSA extends BasePDF
{
    public $isLastPage = false;

    public function lastPage($resetmargins = false)
    {
        $this->setPage($this->getNumPages(), $resetmargins);
        $this->isLastPage = true;
    }

    public function Footer()
    {
        if ($this->isLastPage) {
            $this->SetY(-50);
            $this->ownerSignCell();
        }

        // Position at 15 mm from bottom
        $this->SetY(-10);
        // Set font
        $this->SetFont('librebodoni', 'I', 7);
        // Page number
        $this->Cell(50, 10, "Print time " . date('d F Y H:m:i'));
        $this->Cell(0, 10, 'Page ' . $this->getAliasNumPage() . '/' . $this->getAliasNbPages(), 0, false, 'R', 0, '', 0, false, 'T', 'M');
    }

    protected function ownerSignCell(): void
    {
        $this->SetFont('librebodoni', 'BI', 10);

        $this->Cell(130, 5, "PT. Bavaria Mercindo Motor", 1, 0, 'C');
        $this->Cell(0, 5, "SERVICE ADVISOR", 1, 0, 'C');
        $this->Ln(5);
        $this->Cell(130, 5, " ", "RL", 0, 'C');
        $this->Cell(0, 5, "Jakarta, " . date("d F Y"), "R", 0, 'C');
        $this->Ln(5);


        for ($i = 1; $i <= 3; $i++) {
            $this->Cell(130, 5, " ", "RL", 0, 'R');
            $this->Cell(0, 5, " ", "RL", 0, 'R');
            $this->Ln(5);
        }

        $this->SetFont('librebodoni', 'B', 8);
        $this->Cell(5, 6, " ", "L");
        $this->Cell(30, 6, " ");
        $this->Cell(30, 6, " ");
        $this->Cell(35, 6, " ");
        $this->Cell(30, 6, " ", "R");
        $this->Cell(0, 6, auth()->user()->name, "R", 0, "C");

        $this->Ln(4);

        $this->SetFont('librebodoni', '', 9);
        $this->Cell(5, 6, " ", "LB");
        $this->Cell(30, 6, " ", "B");
        $this->Cell(30, 6, " ", "B");
        $this->Cell(35, 6, " ", "B");
        $this->Cell(30, 6, " ", "B");
        $this->Cell(0, 6, "(SERVICE ADVISOR)", "LRB", 0, "C");
        $this->Ln(6);
    }
}