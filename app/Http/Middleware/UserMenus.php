<?php

namespace App\Http\Middleware;

use App\Libraries\Menu;
use Closure;
use Illuminate\Support\Facades\Auth;

class UserMenus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param null $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            $user = Auth::user();

            view()->share('currentUser', $user);

            (new Menu())->buildFor($user);

            view()->share('initialOptions', $this->initialOptions());
        }

        return $next($request);
    }

    protected function initialOptions()
    {
        $masters = \App\Models\Master::whereIn('group_code', [
            \App\Models\Master::GENDER,
            \App\Models\Master::TRANSMISSION,
            \App\Models\Master::SALUTATION,
            \App\Models\Master::FUEL,
            \App\Models\Master::CAR_TYPE
        ])->orderBy('group_code')->get();

        $result = [];
        foreach ($masters as $master) {
            $result[\App\Models\Master::constantName($master->group_code)][] = [
                'id' => $master->code,
                'text' => $master->name
            ];
        }

        return $result;
    }
}
