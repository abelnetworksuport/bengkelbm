<?php namespace App\Http\Controllers;

use App\Exports\OwnerExport;
use App\Models\Master;
use App\Models\Owner;
use App\Transformers\OwnerTransformer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

/**
 * Created By: Sugeng
 * Date: 24/10/18
 * Time: 21.36
 */
class OwnerController extends BaseController
{
    public function index()
    {
        return view('owner.index');
    }

    public function autocomplete(Request $request)
    {
        $field = $request->get('field');

        $owner = Owner::where($field, 'LIKE', "%{$request->get('term')}%")
                      ->groupBy($field)
                      ->take(25)
                      ->get();

        if ($owner) {
            return responder()->success($owner, function ($owner) use ($field) {
                return [
                    "text" => $owner->{$field}
                ];
            })->respond();
        }
    }

    public function vehicle($ownerId, Request $request)
    {
        if ($owner = Owner::find($ownerId)) {
            $form = $request->get('form');

            $owner->vehicles()->create($form);

            return responder()->success($owner)->meta([
                "title" => "Tambah kendaraan untuk {$owner->fullname}",
                "message" => "Kendaraan {$form['plate_number']} berhasil ditambah untuk Owner {$owner->fullname}"
            ])->respond();
        }
    }

    public function show($id)
    {
        if ($owner = Owner::find($id)) {
            return responder()->success($owner, new OwnerTransformer())->with('vehicles')->respond();
        }
    }

    public function add()
    {
        $masters = Master::whereIn('group_code', [
            \App\Models\Master::GENDER,
            \App\Models\Master::TRANSMISSION,
            \App\Models\Master::SALUTATION,
            \App\Models\Master::FUEL,
            \App\Models\Master::CAR_TYPE
        ])->orderBy('group_code')->get();

        $result = [];
        foreach ($masters as $master) {
            $result[\App\Models\Master::constantName($master->group_code)][] = [
                'id'   => $master->code,
                'text' => $master->name
            ];
        }

        return view('owner.add', ['master' => $result]);
    }

    public function store(Request $request)
    {
        $owner               = $request->get('owner');
        $owner['registered'] = Carbon::now();

        $vehicle = $request->get('vehicle');

        $owner['registered'] = $vehicle['registered'] = Carbon::now();

        $db_owner = Owner::create($owner);
        $db_owner->vehicles()->create($vehicle);

        return response()->json(['status' => 'success', 'message' => "Data Owner dan Kendaraan berhasil disimpan."]);
    }

    public function update($id, Request $request)
    {
        if ($owner = Owner::find($id)) {

            $form = $request->get('form');
            $owner->update($form);

            return responder()->success($owner, new OwnerTransformer())->meta([
                "title" => "Update Data Owner",
                "message" => "Owner {$owner->fullname} berhasil diupdate"
            ])->respond();
        }
    }

    public function vehicles($id)
    {
        $owner    = Owner::find($id);
        $vehicles = $owner->vehicles()->get();

        return $vehicles;
    }

    public function data()
    {
        $query = Owner::select([
            "id",
            "fullname",
            "owner_group",
            "insurance_name",
            "address",
            "phone_number",
            "registered"
        ])->withCount('vehicles');

        return DataTables::of($query)
                         ->addColumn('fullname', function ($q) {
                             return "<a href=\"javascript:\" class=\"font-weight-semibold owner-edit-modal\" data-owner='{$q->id}'>{$q->fullname}</a>
                        <div class=\"text-muted font-size-sm\">
								Tgl. terdaftar: {$q->registered->format('d F Y')}
						</div>";
                         })
                         ->addColumn('group', function ($q) {
                             return "<a href=\"javascript:\" class=\"font-weight-semibold\">{$q->owner_group}</a>
                        <div class=\"text-muted font-size-sm\">
								Asuransi: {$q->insurance_name}
						</div>";
                         })
                         ->editColumn('vehicles_count', function ($query) {
                             return "<a href='javascript:' class='badge badge-success vehicle-modal' data-owner='{$query->id}'>{$query->vehicles_count} Kendaraan</a>";
                         })
                         ->filterColumn('registered_date', function ($query, $keyword) {
                             $query->whereRaw("DATE_FORMAT(registered, '%d %F %Y') like ?", ["%$keyword%"]);
                         })
                         ->filterColumn('fullname', function ($query, $keyword) {
                             $sql = "fullname like ?";
                             $query->whereRaw($sql, ["%{$keyword}%"]);
                         })
                         ->orderColumn('fullname', 'registered $1')
                         ->addColumn('action', 'owner.partials.grid-button-action')
                         ->rawColumns(['vehicles_count', 'fullname', 'group', 'action'])
                         ->make(true);
    }

    public function export()
    {
        return \Excel::download(new OwnerExport, 'owners.xlsx');
    }
}