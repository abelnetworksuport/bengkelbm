<?php namespace App\Http\Controllers;

use App\Events\StatusUpdated;
use App\Events\WorkOrderChangedEvent;
use App\Models\WorkOrder;
use App\Models\WorkOrderMechanic;
use Illuminate\Http\Request;

/**
 * Created By: Sugeng
 * Date: 04/12/18
 * Time: 17.46
 */
class WorkOrderMechanicController extends BaseController
{
    public function show($wo_id)
    {
        if ($wo_id) {
            $mechanics = WorkOrderMechanic::where('work_order_id', $wo_id)->with("mechanicName")->get();

            if (!$mechanics) {
                $default = [
                    "work_order_id" => $wo_id,
                    "mechanic_id"   => '',
                    "mechanic"      => [
                        [
                            "id"   => '',
                            "text" => ''
                        ]
                    ],
                    "notes"         => ''
                ];

                return responder()->success([
                    $default
                ])->respond();
            }

            return responder()->success($mechanics, function ($mechanic) {
                return [
                    "id"            => (int)$mechanic->id,
                    "work_order_id" => (int)$mechanic->work_order_id,
                    "mechanic_id"   => $mechanic->mechanic_id,
                    "mechanic"      => [
                        [
                            "id"   => $mechanic->mechanicName->id,
                            "text" => $mechanic->mechanicName->fullname
                        ]
                    ],
                    "notes"         => $mechanic->notes
                ];
            })->respond();
        }
    }

    public function store(Request $request)
    {
        $mechanics = $request->get('form');

        if (sizeof($mechanics) > 0 && !is_null($request->get('workOrderId'))) {
            WorkOrderMechanic::where('work_order_id', $request->get('workOrderId'))->delete();

            foreach ($mechanics as $mechanic) {
                WorkOrderMechanic::create([
                    'work_order_id' => $request->get('workOrderId'),
                    'mechanic_id'   => $mechanic['mechanic_id'],
                    'notes'         => $mechanic['notes']
                ]);
            }

            $wo = WorkOrder::find($request->get('workOrderId'));

            event(new StatusUpdated(str_slug("Service Advisor (SA)"), "tes", ['title' => "INFO MEKANIK", "text" => "Mekanik untuk WO #{$wo->code} sudah ditambahkan.", "type" => 'info']));

            event(new WorkOrderChangedEvent($wo));

            return responder()->success()->meta([
                'title'   => "Tambah Mekanik",
                'message' => "Data mekanik untuk WO No. #{$wo->code} berhasil diupdate/tambah"
            ])->respond();
        }
    }
}