<?php namespace App\Http\Controllers\Reports;

use App\Exports\Reports\WorkOrderMonthly;
use App\Http\Controllers\BaseController;
use App\Models\User;
use Illuminate\Http\Request;

/**
 * Created By: Sugeng
 * Date: 2018-12-21
 * Time: 23:04
 */
class WorkOrderReportController extends BaseController
{
    public function index(Request $request)
    {
        $serviceAdvisors = User::role('Service Advisor (SA)')->get();

        return view('report.work-order.index', ['serviceAdvisors' => $serviceAdvisors]);
    }

    public function monthly(Request $request)
    {
        $filename = "work-order-monthly-{$request->get('month')}";
        return (new WorkOrderMonthly())->forMonth($request->get('month'))->download("{$filename}.xlsx");
    }
}