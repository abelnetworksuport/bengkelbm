<?php namespace App\Http\Controllers;

use App\Events\StatusUpdated;
use App\Models\WorkOrder;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Status;

/**
 * Created By: Sugeng
 * Date: 03/12/18
 * Time: 14.48
 */
class WorkOrderStatusController extends BaseController
{
    public function store(Request $request)
    {
        $workOrder = WorkOrder::find($request->get('work_order_id'));

        if ($workOrder->minutes_estimated <= 0) {
            return responder()->success()->meta([
                "status"  => "error",
                'title'   => "ESTIMASI SELESAI BELUM DISET",
                'message' => "Status Work order {$workOrder->code} tidak dapat dirubah. Set ESTIMASI SELESAI terlebih dahulu."
            ])->respond();
        }

        $mechanics = $workOrder->mechanics()->get();
        if (count($mechanics) <= 0) {
            return responder()->success()->meta([
                "status"  => "error",
                'title'   => "MEKANIK BELUM DISET",
                'message' => "Status Work order {$workOrder->code} tidak dapat dirubah. Set MEKANIK terlebih dahulu."
            ])->respond();
        }

        $workOrder->statusLogs()->create([
            'status'     => $request->get('status_id'),
            'notes'      => $request->get('notes'),
            'changed_by' => \Auth::user()->name
        ]);

        if ($request->get('status_id') == 8) {
            $workOrder->minutes_finished = Carbon::now();
        }

        $workOrder->status_id = $request->status_id = $request->get('status_id');
        $workOrder->save();

        event(new StatusUpdated(str_slug("Service Advisor (SA)"), "tes", ['title' => "INFO STATUS", "text" => "Status untuk WO #{$workOrder->code} sudah dirubah.", "type" => 'info']));

        return responder()->success()->meta([
            'title'   => "PERUBAHAN WORK ORDER STATUS",
            'message' => "Status Work order {$workOrder->code} berhasil dirubah",
            'payload' => $this->workOrderWidget()
        ])->respond();
    }

    protected function workOrderWidget()
    {
        $statuses = Status::get();

        $result = [];
        foreach ($statuses as $status) {
            $result[] = [
                'name'   => $status->name,
                'colour' => $status->colour,
                'total'  => WorkOrder::where('status_id', $status->id)->count(),
                'icon'   => $status->icon,
                'id'     => $status->id
            ];

        }

        return $result;
    }
}