<?php namespace App\Http\Controllers;

use App\Libraries\Modules\WorkOrder as WorkOrderModules;
use App\Models\Vehicle;
use App\Models\WorkOrder;
use App\Transformers\VehicleTransformer;
use App\Transformers\WorkOrderTransformer;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

/**
 * Created By: Sugeng
 * Date: 2018-12-19
 * Time: 14:46
 */
class WorkOrdersController extends BaseController
{
    /**
     * @var WorkOrder
     */
    protected $order;

    public function __construct(WorkOrder $order)
    {
        parent::__construct();

        $this->order = $order;
    }

    public function index()
    {
        return view('work-order.index');
    }

    public function show($id)
    {
        return responder()->success($this->order->find($id), new WorkOrderTransformer())->toArray();
    }

    public function create(Request $request)
    {
        if ($request->get('vehicle')) {
            return $this->createFromExistVehicle($request->get('vehicle'));
        }

        return view('wo.create');
    }

    public function store(Request $request)
    {
        $workOrder = new WorkOrderModules($request->all());
        return $workOrder->create();
    }

    public function data(DataTables $dataTables, Request $request)
    {
        $serviceAdvisor = auth()->user()->hasRole('Service Advisor (SA)');

        $query = $this->order->with('owner', 'vehicle', 'status')
                          ->when($serviceAdvisor, function($query) {
                              return $query->where('service_advisor_id', auth()->user()->id);
                          })
                          ->when($request->get('status'), function ($query, $request) {
                              return $query->where('status_id', $request->get('status'));
                          });

        return $dataTables->eloquent($query)
                          ->editColumn('minutes_estimated', function ($q) {
                              $hour    = floor(($q->minutes_estimated / 60) % 60);
                              $minutes = $q->minutes_estimated % 60;

                              return ($q->minutes_estimated > 0) ?
                                  "<a href='javascript:' class='badge badge-primary  modal-estimated' data-content='{ \"workOrderId\": \"{$q->id}\", \"data\" : { \"hours\": \"{$hour}\", \"minutes\": \"{$minutes}\" } }'>{$hour} Jam, {$minutes} Menit</a>"
                                  : "<a href='javascript:' class='badge badge-warning modal-estimated' data-content='{\"workOrderId\": \"{$q->id}\"}'>BUAT ESTIMASI</a>";
                          })
                          ->editColumn('minutes_finished', function ($q) {
                              return $q->minutes_finished;
                          })
                          ->addColumn('detail', 'work-order.partials.wo-detail')
                          ->addColumn('status', function ($q) {
                              $parsed = json_encode([
                                  "work_order_id" => $q->id,
                                  "status_id"     => $q->status_id,
                                  "url"           => [
                                      "store" => url("work-order/status")
                                  ]
                              ]);

                              return "<a href='javacript:' class='badge {$q->status->colour} modal-status' data-wo-status='{$parsed}'>{$q->status->name}</a>";
                          })
                          ->addColumn('owner', function ($q) {
                              return $q->owner->fullname;
                          })
                          ->addColumn('registered_date', function ($q) {
                              return $q->created_at->format('d F Y');
                          })
                          ->addColumn('brand_series', function ($q) {
                              return $q->vehicle->brand_series;
                          })
                          ->addColumn('clock_in', function ($q) {
                              return $q->created_at->diffForHumans();
                          })
                          ->editColumn('code', function ($q) {
                              $url = route("work-order.show", $q->id);

                              return "<a href=\"javacript:\" class=\"font-weight-semibold modal-detail\" data-work-order='{$url}'>{$q->code}</a>
                        <div class=\"text-muted font-size-sm\">
								{$q->created_at->format('d F Y')}
						</div>";
                          })
                          ->editColumn('plate_number', function ($q) {
                              return "<a href='javascript:' class='font-weight-bold'>{$q->plate_number}</a>
                        <div class='text-muted font-size-xs'>
                            Driver: {$q->driver}
                        </div>";
                          })
                          ->addColumn('car_return', function ($q) {
                              return "<a href='javascript:' class='badge badge-icon bg-pink'><i class='icon-arrow-left16'></i></a>";
                          })
                          ->addColumn('action', "work-order.partials.action")
                          ->orderColumn('registered_date', 'created_at $1')
                          ->rawColumns(['status', 'minutes_estimated', 'detail', 'code', 'plate_number', 'action', 'car_return'])
                          ->make(true);
    }

    protected function createFromExistVehicle($vehicleId)
    {
        $vehicle = responder()->success(Vehicle::find($vehicleId), new VehicleTransformer())->with("owner")->toCollection();
        return $vehicle;
    }
}