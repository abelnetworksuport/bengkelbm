<?php namespace App\Http\Controllers;

use App\Exports\VehicleExport;
use App\Models\Owner;
use App\Models\Vehicle;
use App\Transformers\VehicleTransformer;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

/**
 * Created By: Sugeng
 * Date: 20/10/18
 * Time: 08.32
 */
class VehicleController extends BaseController
{
    public function index()
    {
        return view()->make('vehicle.index');
    }

    public function autocomplete(Request $request)
    {
        $field = $request->get('field');

        $vehicle = Vehicle::where($field, 'LIKE', "%{$request->get('term')}%")
                          ->groupBy($field)
                          ->take(25)
                          ->get();

        if ($vehicle) {
            return responder()->success($vehicle, function ($vehicle) use ($field) {
                return [
                    "text" => $vehicle->{$field}
                ];
            })->respond();
        }
    }

    public function show($id)
    {
        $vehicle = Vehicle::find($id);
        return responder()->success($vehicle, new VehicleTransformer())->with('owner')->respond();
    }

    public function edit($id)
    {
        $vehicle = Vehicle::find($id);
        return responder()->success($vehicle, new VehicleTransformer())->respond();
    }

    public function update(Request $request)
    {
        $form = $request->get('form');
        if ($vehicle = Vehicle::find($form['id'])) {

            $vehicle->update($form);

            return responder()->success()->meta([
                'title'   => "Update Data Kendaraan",
                'message' => "Kendaraan {$vehicle->plate_number} berhasil diupdate"
            ]);
        }
    }

    public function store(Request $request)
    {
        $form = $request->get('form');

        if (isset($form['owner']['id'])) {
            $owner = Owner::find($form['owner']['id']);
        } else {
            $owner = Owner::create($form['owner']);
        }

        if ($owner) {
            $vehicle = $owner->vehicles()->create($form['vehicle']);

            return response()->json([
                'status'  => 'success',
                'title'   => 'Simpan data Kendaraan',
                'message' => "Data Owner dan Kendaraan berhasil disimpan.",
                'payload' => $vehicle
            ]);
        }

        return response()->json([
            'status'  => 'error',
            'title'   => 'Simpan data Kendaraan',
            'message' => "Data Owner dan Kendaraan gagal disimpan.",
            'payload' => $form
        ]);
    }


    public function data(DataTables $dataTables)
    {
        $query = Vehicle::select([
            "owners.id", "owners.fullname", "vehicles_temp.*"
        ])->join("vehicle_owners", 'vehicle_owners.vehicle_id', 'vehicles_temp.id')
            ->join("owners", 'owners.id', 'vehicle_owners.owner_id');

        return $dataTables->of($query)
                          ->addColumn('owner', function ($q) {
                              foreach ($q->owners as $owner) {
                                  return "<a href='javascript:' class='owner-detail' data-owner='{$owner->id}'><span class='badge badge-info'>{$owner->fullname}</span></a>";
                              }
                          })
                          ->editColumn('plate_number', function ($q) {
                              $url = route('vehicles.show', $q->id);

                              return "<a href='javascript:' class='font-weight-semibold show-modal' data-event='vehicle-owner:detail' data-url='{$url}'><span class='badge badge-success'>{$q->plate_number}</span></a>";
                          })
                          ->addColumn('registered_date', function ($q) {
                              return $q->registered->format('d F Y');
                          })
                          ->addColumn('action', 'vehicle.partials.action')
                          ->filterColumn('owner', function ($query, $keyword) {
                              $query->whereRaw("owners.fullname LIKE ?", ["%$keyword%"]);
                          })
                          ->filterColumn('registered_date', function ($query, $keyword) {
                              $query->whereRaw("DATE_FORMAT(vehicles_temp.registered, '%d %F %Y') like ?", ["%$keyword%"]);
                          })
                          ->filter(function ($query) {
                              if (request()->has('plate_number')) {
                                  $query->where('plate_number', 'like', request('name') . "%");
                              }
                          }, true)
                          ->orderColumn('registered_date', 'vehicles_temp.registered $1')
                          ->rawColumns(['owner', 'plate_number', 'action'])
                          ->make(true);
    }

    public function export()
    {
        return \Excel::download(new VehicleExport, "vehicles.xlsx");
    }

    public function history($vehicle_id)
    {
        $workOrders = Vehicle::with('workOrders.services.service', 'workOrders.problems', 'workOrders.parts', 'workOrders.serviceManuals')->latest()->findOrFail($vehicle_id);

        return view('vehicle.history', [ 'workOrders' => $workOrders ]);
    }
}