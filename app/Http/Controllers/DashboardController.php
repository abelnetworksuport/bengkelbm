<?php namespace App\Http\Controllers;

use App\Models\Status;
use App\Models\Vehicle;
use App\Models\WorkOrder;

/**
 * Created By: Sugeng
 * Date: 09/10/18
 * Time: 10.27
 */
class DashboardController extends BaseController
{
    public function index()
    {
        $widgets = $this->workOrderWidget();

        return view('dashboard.index')->with([
            'widgets'    => $widgets,
            'customers'  => $this->newCustomers(),
            'workOrders' => $this->workOrderGrid(),
            'topBrands'  => $this->topBrands()
        ]);
    }

    protected function vehicleBar()
    {
        $year = date("Y");

        $query = \DB::select(\DB::raw("
            SELECT DATE_FORMAT(registered, '%m') as periode, COUNT(id) as total FROM vehicles_temp WHERE YEAR(registered)=:year GROUP BY DATE_FORMAT(registered, '%Y-%m') ORDER BY registered
        "), ['year' => $year]);

        $months = array_fill(0, 12, 0);

        foreach ($query as $item) {
            $months[(int)$item->periode] = $item->total;
        }

        return $months;
    }

    protected function newCustomers()
    {
        $vehicles = Vehicle::with('owners')->latest('registered')->take(8)->get();

        $result = [];
        foreach ($vehicles as $vehicle) {
            $owner = $vehicle->owners->pluck('fullname');

            $result[] = [
                'plate_number' => $vehicle->plate_number,
                'fullname'     => $owner[0],
                'created_at'   => $vehicle->registered->diffForHumans()
            ];
        }

        return $result;
    }

    protected function workOrderGrid()
    {
        return WorkOrder::whereIn('status_id', [1, 2, 3, 4])->with('status')->latest('created_at')->get();
    }

    protected function topBrands()
    {
        $vehicles = $query = \DB::select(\DB::raw("
            SELECT brand_series, COUNT(id) as total FROM vehicles_temp GROUP BY brand_series ORDER BY total DESC LIMIT 5
             "));

        $result = [];
        foreach ($vehicles as $vehicle) {
            $result['labels'][] = $vehicle->brand_series;
            $result['series'][] = $vehicle->total;
        }

        return $result;
    }

    protected function workOrderWidget()
    {
        $statuses = Status::get();

        $result = [];
        foreach ($statuses as $status) {
            $result[$status->id] = [
                'name'   => $status->name,
                'colour' => $status->colour,
                'total'  => WorkOrder::where('status_id', $status->id)->count(),
                'icon'   => $status->icon,
                'id'     => $status->id
            ];

        }

        return $result;
    }
}