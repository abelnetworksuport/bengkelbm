<?php namespace App\Http\Controllers;

use App\Libraries\Modules\WorkOrder as WorkOrderModules;
use App\Libraries\Modules\WorkOrderTemp as WorkOrderModulesTemp;
use App\Libraries\Reports\WorkOrder\CustomerApprovalFromDB;
use App\Libraries\Reports\WorkOrder\ReportSA;
use App\Libraries\Reports\WorkOrder\SPKTemp;
use App\Models\Master;
use App\Models\Vehicle;
use App\Models\WorkOrder;
use App\Transformers\WorkOrderRevisionTransformer;
use App\Transformers\WorkOrderTransformer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

/**
 * Created By: Sugeng
 * Date: 09/10/18
 * Time: 10.56
 */
class WorkOrderController extends BaseController
{
    public function index()
    {
        return view('work-order.index');
    }

    public function show($id)
    {
        return responder()->success(WorkOrder::find($id), new WorkOrderTransformer())->toArray();
    }

    public function revision($id)
    {
        $wo = responder()->success(WorkOrder::find($id), new WorkOrderRevisionTransformer())->toArray();
        return view('work-order.revision', ['workOrder' => $wo]);
    }

    public function revisionStore($id, Request $request)
    {
        $workOrder = new WorkOrderModules($request->all());
        return $workOrder->update($id);
    }

    public function data(DataTables $dataTables, Request $request)
    {

        $serviceAdvisor  = auth()->user()->hasRole('Service Advisor (SA)');
        $controlTower    = auth()->user()->hasRole('Control Tower (CT)');
        $kepalaBengkel   = auth()->user()->hasRole('Kepala Bengkel');
        $finalInspection = auth()->user()->hasRole('Final Inspection (FI)');
        $bodyRepair      = auth()->user()->hasRole('Body Repair');
        $sparepart       = auth()->user()->hasRole('Sparepart');
        $adminMekanik       = auth()->user()->hasRole('Admin Mekanik');

        $filter = $request->get('filter');

        $query = WorkOrder::with('owner', 'vehicle', 'status', 'serviceAdvisor')
            ->when($serviceAdvisor, function ($query) {
                return $query->where('service_advisor_id', auth()->user()->id);
            })
            ->when($filter['status_id'], function ($query) use ($filter) {
                return $query->where('status_id', $filter['status_id']);
            })
            ->when($controlTower, function ($query) {
                return $query->whereNotIn('status_id', [7, 8]);
            })
            ->when($kepalaBengkel, function ($query) {
                return $query->whereIn('status_id', [2, 3, 4, 5, 6]);
            })
            ->when($bodyRepair, function ($query) {
                return $query->whereIn('status_id', [2, 3, 4, 5, 6, 7, 8]);
            })
            ->when($sparepart, function ($query) {
                return $query->whereIn('status_id', [1, 2, 3, 5]);
            })
            ->when($adminMekanik, function ($query) {
                return $query->whereIn('status_id', [1, 2, 3, 5]);
            })
            ->when($finalInspection, function ($query) {
                return $query->whereIn('status_id', [4]);
            })
            ->when($request->get('status'), function ($query) use ($request) {
                return $query->where('status_id', $request->get('status'));
            })
            ->when($request->get('cr'), function ($query) use ($request) {
                return $query->where('car_return_from', $request->get('cr'));
            })
            ->when($request->get('car-return'), function ($query) use ($request) {
                return $query->whereNotNull('car_return_from');
            });


        return $dataTables->eloquent($query)
            ->addColumn('service_advisor', function ($q) {
                return $q->serviceAdvisor->name;
            })
            ->editColumn('car_return_from', function ($q) {
                return "<a href='javascript:' class='font-weight-semibold'>{$q->cr_code}</a>
                                <div class='text-muted font-size-xs'>
                                    Alasan: {$q->cr_type}
                                </div>";
            })
            ->editColumn('minutes_estimated', function ($q) {
                $hour    = floor(($q->minutes_estimated / 60) % 60);
                $minutes = $q->minutes_estimated % 60;

                if ($q->status_id < 8) {
                    return ($q->minutes_estimated > 0) ?
                        "<a href='javascript:' class='badge badge-primary  modal-estimated' data-content='{ \"workOrderId\": \"{$q->id}\", \"data\" : { \"hours\": \"{$hour}\", \"minutes\": \"{$minutes}\" } }'>{$hour} Jam, {$minutes} Menit</a>"
                        : "<a href='javascript:' class='badge badge-warning modal-estimated' data-content='{\"workOrderId\": \"{$q->id}\"}'>BUAT ESTIMASI</a>";
                }

                return "<span class='badge bg-success'>{$hour} Jam, {$minutes} Menit</span>";
            })
            ->editColumn('minutes_finished', function ($q) {
                return $q->minutes_finished;
            })
            ->addColumn('detail', 'work-order.partials.wo-detail')
            ->addColumn('status', function ($q) {
                if ($q->status_id < 8) {
                    $parsed = json_encode([
                        "work_order_id" => $q->id,
                        "status_id"     => $q->status_id,
                        "url"           => [
                            "store" => url("work-order/status")
                        ]
                    ]);

                    if (auth()->user()->hasRole('Service Advisor (SA)')) {
                        if ($q->status_id !== 7) {
                            return "<span class='badge {$q->status->colour}'>{$q->status->name}</span>";
                        }
                    }

                    if (auth()->user()->hasRole('Kepala Bengkel')) {
                        if ($q->status_id !== 6) {
                            return "<span class='badge {$q->status->colour}'>{$q->status->name}</span>";
                        }
                    }

                    return "<a href='javacript:' class='badge {$q->status->colour} modal-status' data-wo-status='{$parsed}'>{$q->status->name}</a>";
                }
                return "<span class='badge {$q->status->colour}'>{$q->status->name}</span>";
            })
            ->addColumn('owner', function ($q) {
                return $q->owner->fullname;
            })
            ->addColumn('registered_date', function ($q) {
                return $q->created_at->format('d F Y');
            })
            ->addColumn('brand_series', function ($q) {
                return $q->vehicle->brand_series;
            })
            ->addColumn('clock_in', function ($q) {
                return $q->created_at->diffForHumans();
            })
            ->editColumn('code', function ($q) {
                $url = route("work-order.show", $q->id);
                $cr  = ($q->car_return_from) ? "(CR)" : '';

                return "<a href=\"javacript:\" class=\"font-weight-semibold modal-detail\" data-work-order='{$url}'>{$q->code}</a> <i class='text-brown-400'>$cr</i>
                        <div class=\"text-muted font-size-sm\">
								{$q->created_at->format('d F Y')}
						</div>";
            })
            ->editColumn('plate_number', function ($q) {
                $url = route('vehicles.show', $q->vehicle_id);
                return "<a href='javascript:' class='font-weight-bold show-modal' data-event='vehicle-owner:detail' data-url='{$url}'>{$q->plate_number}</a>
                        <div class='text-muted font-size-xs'>
                            Driver: {$q->driver}
                        </div>";
            })
            ->addColumn('car_return', function ($q) {
                return "<a href='javascript:' class='badge badge-icon bg-pink'><i class='icon-arrow-left16'></i></a>";
            })
            ->filterColumn('plate_number', function ($q, $keyword) {
                $q->where("driver", "LIKE", "%{$keyword}%")->orWhere('plate_number', "%{$keyword}%");
            })
            ->addColumn('action', "work-order.partials.action")
            ->orderColumn('registered_date', 'created_at $1')
            ->rawColumns(['status', 'minutes_estimated', 'detail', 'code', 'plate_number', 'action', 'car_return', 'car_return_from', 'report'])
            ->make(true);
    }

    public function store(Request $request)
    {
        $workOrder = new WorkOrderModules($request->all());
        return $workOrder->create();
    }

    public function inspection(Request $request)
    {
        $workOrder = new WorkOrderModulesTemp($request->all());
        return $workOrder->create();
    }

    public function updateEstimatedTime(Request $request)
    {
        $form = $request->get('form');

        if ($request->get('workOrderId')) {
            $workOrder = WorkOrder::find($request->get('workOrderId'));

            if ($workOrder) {
                $workOrder->minutes_estimated = (int)$form['hours'] * 60 + (int)$form['minutes'];
                $workOrder->save();
            }

            return responder()->success()->meta([
                'title'   => "Simpan Estimasi Waktu",
                "message" => "Estimasi waktu selesai diset untuk {$form['hours']} Jam, {$form['minutes']} Menit"
            ])->respond();
        }
    }

    public function add(Request $request)
    {

        $masters = Master::whereIn('group_code', [
            \App\Models\Master::GENDER,
            \App\Models\Master::TRANSMISSION,
            \App\Models\Master::SALUTATION,
            \App\Models\Master::FUEL,
            \App\Models\Master::CAR_TYPE
        ])->orderBy('group_code')->get();

        $result = [];
        foreach ($masters as $master) {
            $result[\App\Models\Master::constantName($master->group_code)][] = [
                'id'   => $master->code,
                'text' => $master->name
            ];
        }

        if ($request->get('vehicle')) {
            $vehicle = Vehicle::with('fuel', 'transmision', 'brand', 'carType')->find($request->get('vehicle'));
            $owner   = $vehicle->load('owners.province', 'owners.district', 'owners.salutation');

            $data = responder()->success($owner, function ($data) {
                return [
                    "vehicle"      => [
                        'id'             => (int)$data->id,
                        'vin_number'     => $data->vin_number,
                        'plate_number'   => $data->plate_number,
                        'engine_number'  => $data->engine_number,
                        'brand_id'       => $data->brand_id,
                        'build_year'     => $data->build_year,
                        'brand'          => [
                            'id'   => $data->brand->id,
                            'text' => $data->brand->name
                        ],
                        "brand_series"   => $data->brand_series,
                        "colour"         => $data->colour,
                        "millage"        => $data->millage,
                        "transmision_id" => $data->transmision_id,
                        "transmision"    => [
                            'id'   => $data->transmision->id,
                            'text' => $data->transmision->text
                        ],
                        "fuel_id"        => $data->fuel_id,
                        "fuel"           => [
                            'id'   => $data->fuel->id,
                            'text' => $data->fuel->name
                        ],
                        "car_type_id"    => $data->car_type_id,
                        "car_type"       => [
                            'id'   => $data->carType->id,
                            'text' => $data->carType->name
                        ],
                        "last_service"   => $data->last_service,
                        "registered"     => $data->registered
                    ],
                    "vehicle_text" => [
                        "brand_id"       => $data->brand->name,
                        "car_type_id"    => $data->carType->name,
                        "fuel_id"        => $data->fuel->name,
                        "transmision_id" => $data->transmision->name
                    ],
                    "owner"        => [
                        'id'            => (int)$data->owners[0]->id,
                        'fullname'      => $data->owners[0]->fullname,
                        'owner_group'   => $data->owners[0]->owner_group,
                        'use_insurance' => (bool)$data->owners[0]->use_insurance,
                        'salutation_id' => $data->owners[0]->salutation_id,
                        'address'       => $data->owners[0]->address,
                        'phone_number'  => $data->owners[0]->phone_number,
                        'email'         => $data->owners[0]->email,
                        'registered'    => $data->owners[0]->registered->format('Y-m-d'),
                        'province_id'   => $data->owners[0]->province_id,
                        'province'      => [
                            'id'   => $data->owners[0]->province->id,
                            'text' => $data->owners[0]->province->name
                        ],
                        'district_id'   => $data->owners[0]->district_id,
                        'district'      => [
                            'id'   => $data->owners[0]->district->id,
                            'text' => $data->owners[0]->district->name
                        ]
                    ],
                    "owner_text"   => [
                        "district_id"   => $data->owners[0]->district->name,
                        "province_id"   => $data->owners[0]->province->name,
                        "salutation_id" => $data->owners[0]->salutation->name
                    ]
                ];
            })->toArray();

            return view('work-order.add', ['data' => $data['data'], 'master' => $result]);
        }

        return view('work-order.add-existing', ['master' => $result]);
    }

    public function print($id)
    {
        $workOrder = responder()->success(WorkOrder::find($id), new WorkOrderTransformer())->toArray();

        $time_print = date("dmYHis");
        $filename   = "estimasi-pengerjaan.{$time_print}.pdf";
        $report     = new CustomerApprovalFromDB($workOrder['data']);
        $report->render($filename, "I");
    }

    public function report($id)
    {
        $workOrder = responder()->success(WorkOrder::find($id), new WorkOrderTransformer())->toArray();

        $time_print = date("dmYHis");
        $filename   = "report-service-advisor.{$time_print}.pdf";
        $report     = new ReportSA($workOrder['data']);
        $report->render($filename, "I");
    }

    public function spk($id)
    {
        $workOrder = \App\Models\WorkOrder::with('vehicle', 'owner', 'services.service', 'serviceManuals', 'problems', 'serviceAdvisor', 'vehicle.brand')->find($id);

        $data = [
            "code"            => $workOrder->code,
            "created_at"      => $workOrder->created_at,
            "owner_name"      => $workOrder->owner_name,
            "vin_number"      => $workOrder->vehicle->vin_number,
            "engine_number"   => $workOrder->vehicle->engine_number,
            "plate_number"    => $workOrder->vehicle->plate_number,
            "brand"           => $workOrder->vehicle->brand->name,
            "brand_series"    => $workOrder->vehicle->brand_series,
            "address"         => substr($workOrder->owner->address, 0, 23),
            "colour"          => $workOrder->vehicle->colour,
            "phone_number"    => $workOrder->driver_phone,
            "driver"          => $workOrder->driver,
            "services"        => $workOrder->services,
            "service_manuals" => $workOrder->serviceManuals,
            "problems"        => $workOrder->problems,
            "millage"         => $workOrder->vehicle->millage,
            "service_advisor" => $workOrder->serviceAdvisor->name,
            "revision"        => $workOrder->revision
        ];


        $pdf = new \App\Libraries\Reports\WorkOrder\SPK($data);
        $pdf->render();
    }

    public function spkTemp(Request $request)
    {
        $vehicle = $request->get('vehicle');
        $owner   = $request->get('owner');

        $data = [
            "code"            => "AUTOGENERATE",
            "created_at"      => Carbon::now()->format('d-m-Y H:i:s'),
            "owner_name"      => $owner['fullname'],
            "vin_number"      => $vehicle['vin_number'],
            "engine_number"   => $vehicle['engine_number'],
            "plate_number"    => $vehicle['plate_number'],
            "address"         => substr($owner['address'], 0, 23),
            "colour"          => $vehicle['colour'],
            "phone_number"    => $vehicle['driver_phone'],
            "driver"          => $vehicle['driver_name'],
            "services"        => $request->get('services'),
            "problems"        => $request->get('problems'),
            "millage"         => $vehicle['millage'],
            "service_advisor" => auth()->user()->name,
        ];

        $pdf = new SPKTemp($data);
        $pdf->render();

        return response()->json(['status' => 'success', 'url' => url('download/perintah-kerja-bengkel.pdf')]);
    }
}
