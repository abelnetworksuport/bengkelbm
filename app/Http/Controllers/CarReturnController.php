<?php namespace App\Http\Controllers;

use App\Libraries\Modules\CarReturn;
use App\Models\WorkOrder;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

/**
 * Created By: Sugeng
 * Date: 23/11/18
 * Time: 07.07
 */
class CarReturnController extends BaseController
{
    public function index()
    {
        return view('car-return.index');
    }

    public function create()
    {
        return view('car-return.add');
    }

    public function store(Request $request)
    {
        return (new CarReturn())->create($request->get('form'));
    }

    public function datatable(DataTables $dataTables)
    {
        $query = WorkOrder::with('owner', 'vehicle', 'status', 'serviceAdvisor')
                          ->where('status_id', 8);

        return $dataTables->eloquent($query)
                          ->editColumn('minutes_estimated', 'car-return.partials.estimated')
                          ->editColumn('minutes_finished', 'car-return.partials.finished')
                          ->addColumn('vehicle', 'car-return.partials.vehicle')
                          ->editColumn('status', 'car-return.partials.status')
                          ->addColumn('code', 'car-return.partials.detail')
                          ->addColumn('owner', 'car-return.partials.owner')
                          ->addColumn('action', 'car-return.partials.action')
                          ->rawColumns(['minutes_estimated', 'minutes_finished', 'vehicle', 'code', 'status', 'owner', 'action'])
                          ->make('true');
    }
}