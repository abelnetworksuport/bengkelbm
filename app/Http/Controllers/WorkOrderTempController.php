<?php namespace App\Http\Controllers;

use App\Models\WorkOrderTemp;
use App\Transformers\WorkOrderTemporaryTransformer;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

/**
 * Created By: Sugeng
 * Date: 2019-02-20
 * Time: 12:31
 */
class WorkOrderTempController extends BaseController
{
    public function index()
    {
        return view('wo-temp.index');
    }

    public function datatable(DataTables $dataTables)
    {
        $serviceAdvisor = auth()->user()->hasRole('Service Advisor (SA)');

        $query = WorkOrderTemp::with('owner', 'vehicle', 'serviceAdvisor')->where('status', "1");

        return $dataTables->eloquent($query)
                          ->addColumn('vehicle', 'car-return.partials.vehicle')
                          ->addColumn('code', 'car-return.partials.detail')
                          ->addColumn('owner', 'car-return.partials.owner')
                          ->addColumn('action', 'wo-temp.partials.action')
                          ->filterColumn('owner', function ($q, $keyword) {
                              $q->where("driver", "LIKE", "%{$keyword}%")->orWhere('owner_name', "%{$keyword}%");
                          })
                          ->filterColumn('vehicle', function ($q, $keyword) {
                              $q->where("plate_number", "LIKE", "%{$keyword}%");
                          })
                          ->rawColumns(['vehicle', 'code', 'owner', 'action'])
                          ->make('true');
    }

    public function create(Request $request)
    {
        $woTempId = $request->get('id');

        $woTemp = WorkOrderTemp::with("vehicle", "owner")->find($woTempId);

        $workOrder = responder()->success($woTemp, new WorkOrderTemporaryTransformer())->toArray();
        //return $workOrder;
        return view('wo-temp.create', ['workOrder' => $workOrder]);
    }
}