<?php namespace App\Http\Controllers;
/**
 * Created By: Sugeng
 * Date: 07/10/18
 * Time: 10.26
 */
class BaseController extends Controller
{
    protected $page = [];

    public function __construct()
    {
        $this->middleware('auth');
    }
}