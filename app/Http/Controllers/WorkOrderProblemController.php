<?php namespace App\Http\Controllers;

use App\Events\WorkOrderChangedEvent;
use App\Models\WorkOrder;
use App\Models\WorkOrderProblem;
use Illuminate\Http\Request;

/**
 * Created By: Sugeng
 * Date: 03/12/18
 * Time: 12.39
 */
class WorkOrderProblemController extends BaseController
{
    public function show($wo_id)
    {
        if ($wo_id) {
            $workOrder = WorkOrder::find($wo_id);

            $problems = $workOrder->problems()->get();

            return responder()->success($problems, function ($query) {
                return [
                    "id"            => (int)$query->id,
                    "work_order_id" => (int)$query->work_order_id,
                    "problem"       => $query->name,
                    "description"   => $query->description
                ];
            })->respond();
        }
    }

    public function store(Request $request)
    {
        $problems = $request->get('form');

        if (sizeof($problems) > 0 && !is_null($request->get('workOrderId'))) {
            WorkOrderProblem::where('work_order_id', $request->get('workOrderId'))->delete();

            foreach ($problems as $problem) {
                WorkOrderProblem::create([
                    'work_order_id' => $request->get('workOrderId'),
                    'name'          => $problem['problem'],
                    'description'   => $problem['description']
                ]);
            }

            event(new WorkOrderChangedEvent(WorkOrder::find($request->get('workOrderId'))));

            return responder()->success()->meta([
                'title'   => "Edit Data Permasalahan",
                'message' => "Data permasalahan untuk WO No. #{$request->get('workOrderId')} berhasil diupdate"
            ])->respond();
        }
    }
}