<?php namespace App\Http\Controllers;

use App\Events\StatusUpdated;
use App\Events\WorkOrderChangedEvent;
use App\Models\WorkOrder;
use App\Models\WorkOrderParts;
use App\Transformers\WorkOrderPartTransformer;
use Illuminate\Http\Request;

/**
 * Created By: Sugeng
 * Date: 03/12/18
 * Time: 11.51
 */
class WorkOrderPartsController extends BaseController
{
    public function show($wo_id)
    {
        if ($wo_id) {
            $workOrder = WorkOrder::find($wo_id);

            $spareparts = $workOrder->parts()->with('sparepart')->get();

            return responder()->success($spareparts, new WorkOrderPartTransformer())->respond();
        }
    }

    public function store(Request $request)
    {
        $spareparts = $request->get('form');

        if (sizeof($spareparts) > 0 && !is_null($request->get('workOrderId'))) {
            WorkOrderParts::where('work_order_id', $request->get('workOrderId'))->delete();

            foreach ($spareparts as $sparepart) {
                $price = str_replace('.', '', $sparepart['estimated_price']);
                WorkOrderParts::create([
                    'work_order_id'   => $request->get('workOrderId'),
                    'part_name'       => $sparepart['part_name'],
                    'quantities'      => $sparepart['quantities'],
                    'qty_label'       => $sparepart['qty_text'],
                    'quantity_id'     => $sparepart['quantity_id'],
                    'estimated_price' => $price
                ]);
            }

            $wo = WorkOrder::find($request->get('workOrderId'));

            event(new StatusUpdated(str_slug("Service Advisor (SA)"), "tes", ['title' => "INFO SPAREPART", "text" => "Spare part untuk WO #{$wo->code} sudah dirubah.", "type" => 'info']));
            event(new WorkOrderChangedEvent(WorkOrder::find($request->get('workOrderId'))));

            return responder()->success()->meta([
                'title'   => "Edit Data Sparepart",
                'message' => "Data sparepart untuk WO No. #{$request->get('workOrderId')} berhasil diupdate"
            ])->respond();
        }


    }
}