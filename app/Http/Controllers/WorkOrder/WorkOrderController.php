<?php namespace App\Http\Controllers\WorkOrder;

use App\Http\Controllers\BaseController;
use App\Models\WorkOrder;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

/**
 * Created By: Sugeng
 * Date: 2019-02-24
 * Time: 10:48
 */
class WorkOrderController extends BaseController
{

    protected $roles
        = [
            'Controle Tower (CT)'   => [],
            'Kepala Bengkel'        => [],
            'Final Inspection (FI)' => [],
            'Body Repair'           => [],
            'Sparepart'             => []
        ];

    protected $workOrder;

    public function __construct(WorkOrder $workOrder)
    {
        parent::__construct();

        $this->workOrder = $workOrder;
    }

    public function index()
    {
        return view("work-order.index");
    }

    public function grid(DataTables $dataTables, Request $request)
    {

        $role = array_shift(auth()->user()->roles->pluck('name'));

        $query = $this->workOrder->with('owner', 'vehicle', 'status')
                                 ->when(array_key_exists($role, $this->roles), function ($q) use ($role) {
                                     return $q->whereIn('status_id', $this->roles[$role]);
                                 })
                                 ->when($role == 'Service Advisor (SA)', function ($q) {
                                     return $q->where('service_advisor_id', auth()->user()->id);
                                 })
                                 ->when($request->get('status'), function ($q) use ($request) {
                                     return $q->where('status_id', $request->get('status'));
                                 })
                                 ->when($request->get('cr'), function ($q) use ($request) {
                                     return $q->where('car_return_from', $request->get('cr'));
                                 })
                                 ->when($request->get('car-return'), function ($q) use ($request) {
                                     return $q->whereNotNull('car_return_from');
                                 });

        return $dataTables->eloquent($query)
            ->editColumn('minutes_estimated', 'work-order.partials.minutes_estimated')
            ->editColumn('code', 'work-order.partials.code')
            ->editColumn('car_return_from', 'work-order.partials.car_return_from')
            ->editColumn('plate_number', 'work-order.partials.plate_number')
            ->addColumn('status', 'work-order.partials.status')
            ->addColumn('owner', 'work-order.partials.owner')
            ->addColumn('car_return', 'work-order.partials.car_return')
                          ->make(true);
    }

    public function create()
    {

    }

    public function store(Request $request)
    {

    }
}