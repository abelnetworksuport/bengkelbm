<?php namespace App\Http\Controllers;

use App\Exports\EmployeeExport;
use App\Models\Employee;
use App\Transformers\EmployeeTransformer;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

/**
 * Created By: Sugeng
 * Date: 10/12/18
 * Time: 09.38
 */
class EmployeeController extends BaseController
{
    protected $employee;

    public function __construct(Employee $employee)
    {
        parent::__construct();

        $this->employee = $employee;
    }

    public function index ()
    {
        return view('employee.index');
    }

    public function store(Request $request)
    {
        $form = $request->get('form');

        if ($this->employee->where('fullname', $form['fullname'])->first()) {
            return responder()->error(417, "Karyawan {$form['fullname']} sudah ada.")->respond(417);
        }

        if ($employee = $this->employee->store($form)) {
            return responder()->success($employee)->meta([
                "title"        => "Tambah Data Karyawan",
                "message"      => "Data Karyawan {$form['fullname']} berhasil disimpan.",
                "confirmLabel" => "Tambah data karyawan",
                "cancelLabel"  => "Kembali ke daftar karyawan"
            ])->respond();
        };

        return responder()->error(417, "Terjadi kesalahan tidak dapat memproses data.")->respond(417);
    }

    public function edit($id)
    {
        if ($id) {
            return responder()->success($this->employee->find($id), new EmployeeTransformer())->respond();
        }

        return responder()->error(404, "Karyawan {$id} tidak ditemukan")->respond(404);
    }

    public function update(Request $request, $id)
    {
        if ($id) {
            if ($employee = $this->employee->find($id)) {
                $form = $request->get('form');

                if ($employee->updateData($form)) {
                    return responder()->success($employee)->meta([
                        "title"        => "Update Karyawan",
                        "message"      => "Karyawan {$form['fullname']} berhasil diupdate.",
                    ])->respond();
                }

                return responder()->error(417, "Terjadi kesalahan tidak dapat memproses data.")->respond(417);
            }

            return responder()->error(404, "ID {$id} tidak ditemukan")->respond(404);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        if ($id) {
            if ($employee = $this->employee->where('id', $id)->delete()) {
                return responder()->success()->meta([
                    "title"        => "Hapus Karyawan",
                    "message"      => "Karyawan {$id} berhasil dihapus.",
                ])->respond();
            }

            return responder()->error(404, "ID {$id} tidak ditemukan")->respond(404);
        }

        return responder()->error(417, "Terjadi kesalahan tidak dapat memproses data.")->respond(417);
    }

    public function data (DataTables $dataTables)
    {
        $query = $this->employee->select([
                                      "id",
                                      "code",
                                      "fullname",
                                      "salutation_id",
                                      "identity_card_number",
                                      "birthday",
                                      "initials",
                                      "address",
                                      "phone_number",
                                      "position_id",
                                      "is_mechanic"
                                  ])->with("position");

        return $dataTables->eloquent($query)
                          ->addColumn('position', function ($q) {
                              return "<span class='badge {$q->position->colour}'>{$q->position->name}</span>";
                          })
                            ->addColumn('is_mechanic', function ($q) {
                                return ($q->is_mechanic) ? "<span class='badge badge-icon bg-success'><i class='icon-check2'></i></span>": "<span class='badge badge-icon bg-warning'><i class='icon-cross2'></i></span>";
                            })
                          ->addColumn('action', "employee.partials.action")
                          ->rawColumns(['position', 'is_mechanic', 'action'])
                          ->make(true);
    }

    public function export()
    {
        return \Excel::download(new EmployeeExport, 'employees.xlsx');
    }
}