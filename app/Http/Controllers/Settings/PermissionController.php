<?php namespace App\Http\Controllers\Settings;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

/**
 * Created By: Sugeng
 * Date: 27/11/18
 * Time: 08.49
 */
class PermissionController extends BaseController
{
    /**
     * @var Permission
     */
    protected $permission;

    public function __construct(Permission $permission)
    {
        parent::__construct();

        $this->permission = $permission;
    }

    public function store(Request $request)
    {
        $permissions = $request->get('permissions');

        if ($role = Role::findById($request->get('roleId'))) {
            $role->syncPermissions($permissions);

            return responder()->success($role)->meta([
                "title" => "Assign Permission",
                "message" => "Permission untuk role {$role->name} berhasil disimpan"
            ]);
        }


    }
}