<?php namespace App\Http\Controllers\Settings;

use App\Http\Controllers\BaseController;
use App\Models\Module;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

/**
 * Created By: Sugeng
 * Date: 25/11/18
 * Time: 16.17
 */
class ModuleController extends BaseController
{
    /**
     * @var User
     */
    protected $module;

    public function __construct(Module $module)
    {
        parent::__construct();

        $this->module = $module;
    }

    public function index()
    {
        return view('settings.module.index');
    }

    public function show($id)
    {
        $module = $this->module->find($id);

        return responder()->success($module, function($u) {
            return [
                'id' => $u->id,
                'name' => $u->name
            ];
        })->respond();
    }

    public function store(Request $request)
    {
        if (! $this->module->where('name', $request->get('name'))->first()) {
            $module = $this->module->create([
                'name'          => $request->get('name'),
                'guard_name'    => 'web'
            ]);
        }

        return responder()->success($request->all())->meta([
            'title'         => 'Tambah Module',
            'message'       => 'Berhasil menambah Module. Anda akan menambah Module? ',
            'confirmLabel'  => "Ya, Tambah module",
            'cancelLabel'   => "Tidak, Kembali ke daftar module"
        ])->respond();
    }

    public function update(Request $request)
    {
        if (!empty($request->get('id'))) {
            $module = $this->module->find($request->get('id'));
            $module->name = $request->get('name');
            $module->save();

            return responder()->success($request->all())->meta([
                'title'         => 'Update Module',
                'message'       => 'Berhasil mengupdate module.',
            ])->respond();
        }

        return responder()->success($request->all())->meta([
            'title'         => 'Update module',
            'message'       => 'Tidak ditemukan module',
        ])->respond();
    }

    public function lists()
    {
        $modules = $this->module->get(['name', 'slug', 'permissions']);

        $result = [];
        foreach ($modules as $module) {
            $result[] = [
                'name' => $module->name,
                'slug' => $module->slug,
                'permissions' => json_decode($module->permissions)
            ];
        }

        return responder()->success($result)->respond();
    }

    public function data(DataTables $dataTables)
    {
        $model = $this->module->select('id', 'name', 'uuid', 'description', 'navigation_name');

        return $dataTables->eloquent($model)
            ->make(true);
    }
}