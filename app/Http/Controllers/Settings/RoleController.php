<?php namespace App\Http\Controllers\Settings;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Yajra\DataTables\DataTables;

/**
 * Created By: Sugeng
 * Date: 25/11/18
 * Time: 16.17
 */
class RoleController extends BaseController
{
    /**
     * @var User
     */
    protected $role;

    public function __construct(Role $role)
    {
        parent::__construct();

        $this->role = $role;
    }

    public function index()
    {
        return view('settings.role.index');
    }

    public function show($id)
    {
        $role = $this->role->find($id);

        return responder()->success($role, function($u) {
            return [
                'id' => $u->id,
                'name' => $u->name
            ];
        })->respond();
    }

    public function store(Request $request)
    {
        if (! $this->role->where('name', $request->get('name'))->first()) {
            $role = $this->role->create([
                'name'          => $request->get('name'),
                'guard_name'    => 'web'
            ]);
        }

        return responder()->success($request->all())->meta([
            'title'         => 'Tambah Role',
            'message'       => 'Berhasil menambah Role. Anda akan menambah Role? ',
            'confirmLabel'  => "Ya, Tambah role",
            'cancelLabel'   => "Tidak, Kembali ke daftar role"
        ])->respond();
    }

    public function update(Request $request)
    {
        if (!empty($request->get('id'))) {
            $role = $this->role->find($request->get('id'));
            $role->name = $request->get('name');
            $role->save();

            return responder()->success($request->all())->meta([
                'title'         => 'Update Role',
                'message'       => 'Berhasil mengupdate role.',
            ])->respond();
        }

        return responder()->success($request->all())->meta([
            'title'         => 'Update role',
            'message'       => 'Tidak ditemukan role',
        ])->respond();
    }

    public function permissions($id)
    {
        $role = $this->role->find($id);

        $permissions = $role->permissions()->pluck("name");;

        return responder()->success()->meta([
            'permissions' => $permissions,
            'role' => $role
        ])->respond();
    }

    public function data(DataTables $dataTables)
    {
        $model = $this->role->select('id', 'name');

        return $dataTables->eloquent($model)
                          ->addColumn('permission', "settings.role.partials.permissions")
                          ->addColumn('action', "settings.role.partials.action")
                          ->rawColumns(['permission', 'action'])
                          ->make(true);
    }
}