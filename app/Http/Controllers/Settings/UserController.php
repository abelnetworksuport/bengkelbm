<?php namespace App\Http\Controllers\Settings;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use App\Models\User;
use App\Transformers\UserTransformer;
use Yajra\DataTables\DataTables;

/**
 * Created By: Sugeng
 * Date: 25/11/18
 * Time: 16.17
 */
class UserController extends BaseController
{
    /**
     * @var User
     */
    protected $user;

    public function __construct(User $user)
    {
        parent::__construct();

        $this->user = $user;
    }

    public function index()
    {
        return view('settings.user.index');
    }

    public function show($id)
    {
        return responder()->success($this->user->find($id), new UserTransformer())->meta(['userId' => $id])->respond();
    }

    public function store(Request $request)
    {
        if (!$this->user->where('username', $request->get('username'))->first()) {
            $user = $this->user->create([
                'name'     => $request->get('name'),
                'username' => $request->get('username'),
                'password' => bcrypt($request->get('password')),
                'email'    => $request->get('email'),
            ]);

            if ($user) {
                $user->assignRole($request->get('role'));
            }
        }

        return responder()->success($request->all())->meta([
            'title'        => 'Tambah User',
            'message'      => 'Berhasil menambah user. Anda akan menambah User? ',
            'confirmLabel' => "Ya, Tambah user",
            'cancelLabel'  => "Tidak, Kembali ke daftar user"
        ])->respond();
    }

    public function update(Request $request)
    {
        if (!empty($request->get('id'))) {
            $user = $this->user->find($request->get('id'));
            $user->name = $request->get('name');
            $user->email = $request->get('email');
            $user->save();
            $user->roles()->detach();

            $user->assignRole($request->get('role'));

            return responder()->success($request->all())->meta([
                'title'   => 'Update User',
                'message' => 'Berhasil mengupdate user.',
            ])->respond();
        }

        return responder()->success($request->all())->meta([
            'title'   => 'Update User',
            'message' => 'Tidak ditemukan User',
        ])->respond();
    }

    public function resetPassword(Request $request)
    {
        if ($request->get('id')) {
            $form = $request->get('form');

            $user = $this->user->find($request->get('id'));
            $user->password = bcrypt($form['password']);
            $user->save();

            return responder()->success()->meta([
                'title'   => 'Reset Password User',
                'message' => 'Berhasil mereset password user.',
            ])->respond();
        }

        return responder()->error("404", "User tidak ditemukan")->respond(404);
    }

    public function data(DataTables $dataTables)
    {
        $model = $this->user->select('id', 'uuid', 'username', 'name', 'email');

        return $dataTables->eloquent($model)
                          ->addColumn('role', function ($q) {
                              $roleName = (isset($q->getRoleNames()[0])) ? $q->getRoleNames()[0] : "";

                              return "<a href='javascript:' class='badge badge-info show-modal' data-url='settings/users/{$q->id}' data-event='user::edit'>{$roleName}</a>";
                          })
                          ->addColumn('action', "settings.user.partials.action")
                          ->rawColumns(['role', 'action'])
                          ->make(true);
    }

    public function destroy($uuid)
    {
        if ($uuid) {
            if ($user = $this->user->byUUID($uuid)->delete()) {
                return responder()->success()->meta([
                    "title"        => "Hapus User",
                    "message"      => "User {$uuid} berhasil dihapus.",
                ])->respond();
            }

            return responder()->error(404, "ID {$uuid} tidak ditemukan")->respond(404);
        }

        return responder()->error(417, "Terjadi kesalahan tidak dapat memproses data.")->respond(417);
    }
}