<?php namespace App\Http\Controllers\Master;

use App\Exports\StatusExport;
use App\Http\Controllers\BaseController;
use App\Models\Status;
use App\Transformers\StatusWOTransformer;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

/**
 * Created By: Sugeng
 * Date: 2018-12-18
 * Time: 13:07
 */
class StatusController extends BaseController
{
    protected $status;

    public function __construct(Status $status)
    {
        parent::__construct();

        $this->status = $status;
    }

    public function index()
    {
        return view('master.status.index');
    }

    public function store(Request $request)
    {
        $form = $request->get('form');

        if ($this->status->where('name', $form['name'])->first()) {
            return responder()->error(417, "Status Work Order {$form['name']} sudah ada.")->respond(417);
        }

        if ($status = $this->status->store($form)) {
            return responder()->success($status)->meta([
                "title"        => "Tambah Data Status Work Order",
                "message"      => "Data Status Work Order {$form['name']} berhasil disimpan.",
                "confirmLabel" => "Tambah data karyawan",
                "cancelLabel"  => "Kembali ke daftar karyawan"
            ])->respond();
        };

        return responder()->error(417, "Terjadi kesalahan tidak dapat memproses data.")->respond(417);
    }

    public function edit($id)
    {
        if ($id) {
            return responder()->success($this->status->find($id), new StatusWOTransformer())->respond();
        }

        return responder()->error(404, "Status Work Order {$id} tidak ditemukan")->respond(404);
    }

    public function update(Request $request, $id)
    {
        if ($id) {
            if ($status = $this->status->find($id)) {
                $form = $request->get('form');

                if ($status->updateData($form)) {
                    return responder()->success($status)->meta([
                        "title"   => "Update Status Work Order",
                        "message" => "Status Work Order {$form['name']} berhasil diupdate.",
                    ])->respond();
                }

                return responder()->error(417, "Terjadi kesalahan tidak dapat memproses data.")->respond(417);
            }

            return responder()->error(404, "ID {$id} tidak ditemukan")->respond(404);
        }
    }

    public function destroy($id)
    {
        if ($id) {
            if ($status = $this->status->where('id', $id)->delete()) {
                return responder()->success()->meta([
                    "title"   => "Hapus Status Work Order",
                    "message" => "Status Work Order {$id} berhasil dihapus.",
                ])->respond();
            }

            return responder()->error(404, "ID {$id} tidak ditemukan")->respond(404);
        }

        return responder()->error(417, "Terjadi kesalahan tidak dapat memproses data.")->respond(417);
    }

    public function data(DataTables $dataTables)
    {
        $query = $this->status->select([
            "id",
            "code",
            "name",
            "colour",
        ]);

        return $dataTables->eloquent($query)
                          ->editColumn('colour', function ($q) {
                              return "<span class='badge {$q->colour}'>{$q->colour}</span>";
                          })
                          ->addColumn('action', "master.status.partials.action")
                          ->rawColumns(['colour', 'action'])
                          ->make(true);
    }

    public function export()
    {
        return \Excel::download(new StatusExport, 'statuses.xlsx');
    }
}