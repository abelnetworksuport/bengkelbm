<?php namespace App\Http\Controllers\Master;

use App\Exports\PositionExport;
use App\Http\Controllers\BaseController;
use App\Models\Position;
use App\Transformers\PositionTransformer;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

/**
 * Created By: Sugeng
 * Date: 2018-12-18
 * Time: 12:45
 */
class PositionController extends BaseController
{
    protected $position;

    public function __construct(Position $position)
    {
        parent::__construct();

        $this->position = $position;
    }

    public function index()
    {
        return view('master.position.index');
    }

    public function store(Request $request)
    {
        $form = $request->get('form');

        if ($this->position->where('name', $form['name'])->first()) {
            return responder()->error(417, "Jabatan {$form['name']} sudah ada.")->respond(417);
        }

        if ($position = $this->position->store($form)) {
            return responder()->success($position)->meta([
                "title"        => "Tambah Data Jabatan",
                "message"      => "Data Jabatan {$form['name']} berhasil disimpan.",
                "confirmLabel" => "Tambah data jabatan",
                "cancelLabel"  => "Kembali ke daftar jabatan"
            ])->respond();
        };

        return responder()->error(417, "Terjadi kesalahan tidak dapat memproses data.")->respond(417);
    }

    public function edit($id)
    {
        if ($id) {
            return responder()->success($this->position->find($id), new PositionTransformer())->respond();
        }

        return responder()->error(404, "Jabatan {$id} tidak ditemukan")->respond(404);
    }

    public function update(Request $request, $id)
    {
        if ($id) {
            if ($position = $this->position->find($id)) {
                $form = $request->get('form');

                if ($position->updateData($form)) {
                    return responder()->success($position)->meta([
                        "title"   => "Update Jabatan",
                        "message" => "Jabatan {$form['name']} berhasil diupdate.",
                    ])->respond();
                }

                return responder()->error(417, "Terjadi kesalahan tidak dapat memproses data.")->respond(417);
            }

            return responder()->error(404, "ID {$id} tidak ditemukan")->respond(404);
        }
    }

    public function destroy($id)
    {
        if ($id) {
            if ($position = $this->position->where('id', $id)->delete()) {
                return responder()->success()->meta([
                    "title"   => "Hapus Jabatan",
                    "message" => "Jabatan {$id} berhasil dihapus.",
                ])->respond();
            }

            return responder()->error(404, "ID {$id} tidak ditemukan")->respond(404);
        }

        return responder()->error(417, "Terjadi kesalahan tidak dapat memproses data.")->respond(417);
    }

    public function data(DataTables $dataTables)
    {
        $query = $this->position->select([
            "id",
            "name",
            "initials",
        ]);

        return $dataTables->eloquent($query)
                          ->addColumn('action', "master.position.partials.action")
                          ->rawColumns(['action'])
                          ->make(true);
    }

    public function export()
    {
        return \Excel::download(new PositionExport, 'positions.xlsx');
    }
}