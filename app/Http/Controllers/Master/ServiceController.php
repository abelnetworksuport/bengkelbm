<?php namespace App\Http\Controllers\Master;

use App\Exports\ServiceExport;
use App\Http\Controllers\BaseController;
use App\Models\Service;
use App\Transformers\MasterServiceTransformer;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

/**
 * Created By: Sugeng
 * Date: 2018-12-18
 * Time: 11:28
 */
class ServiceController extends BaseController
{
    protected $service;

    public function __construct(Service $service)
    {
        parent::__construct();

        $this->service = $service;
    }

    public function index()
    {
        return view('master.service.index');
    }

    public function store(Request $request)
    {
        $form = $request->get('form');

        if ($this->service->where('name', $form['name'])->first()) {
            return responder()->error(417, "Jasa/Service {$form['name']} sudah ada.")->respond(417);
        }

        if ($service = $this->service->store($form)) {
            return responder()->success($service)->meta([
                "title"        => "Tambah Data Jasa/Service",
                "message"      => "Data Jasa/Service {$form['name']} berhasil disimpan.",
                "confirmLabel" => "Tambah data karyawan",
                "cancelLabel"  => "Kembali ke daftar karyawan"
            ])->respond();
        };

        return responder()->error(417, "Terjadi kesalahan tidak dapat memproses data.")->respond(417);
    }

    public function edit($id)
    {
        if ($id) {
            return responder()->success($this->service->find($id), new MasterServiceTransformer())->respond();
        }

        return responder()->error(404, "Jasa/Service {$id} tidak ditemukan")->respond(404);
    }

    public function update(Request $request, $id)
    {
        if ($id) {
            if ($service = $this->service->find($id)) {
                $form = $request->get('form');

                if ($service->updateData($form)) {
                    return responder()->success($service)->meta([
                        "title"   => "Update Jasa/Service",
                        "message" => "Jasa/Service {$form['name']} berhasil diupdate.",
                    ])->respond();
                }

                return responder()->error(417, "Terjadi kesalahan tidak dapat memproses data.")->respond(417);
            }

            return responder()->error(404, "ID {$id} tidak ditemukan")->respond(404);
        }
    }

    public function destroy($id)
    {
        if ($id) {
            if ($service = $this->service->where('id', $id)->delete()) {
                return responder()->success()->meta([
                    "title"   => "Hapus Jasa/Service",
                    "message" => "Jasa/Service {$id} berhasil dihapus.",
                ])->respond();
            }

            return responder()->error(404, "ID {$id} tidak ditemukan")->respond(404);
        }

        return responder()->error(417, "Terjadi kesalahan tidak dapat memproses data.")->respond(417);
    }

    public function data(DataTables $dataTables)
    {
        $query = $this->service->select([
            "id",
            "name",
            "weight",
            "estimated_price",
        ]);

        return $dataTables->eloquent($query)
                          ->editColumn('estimated_price', function ($q) {
                              return number_format($q->estimated_price);
                          })
                          ->addColumn('action', "master.service.partials.action")
                          ->rawColumns(['position', 'is_mechanic', 'action'])
                          ->make(true);
    }

    public function export()
    {
        return \Excel::download(new ServiceExport, 'services.xlsx');
    }
}