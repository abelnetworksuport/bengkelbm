<?php namespace App\Http\Controllers;

use App\Events\WorkOrderChangedEvent;
use App\Models\WorkOrder;
use App\Models\WorkOrderService;
use Illuminate\Http\Request;

/**
 * Created By: Sugeng
 * Date: 30/11/18
 * Time: 19.17
 */
class WorkOrderServiceController extends BaseController
{
    public function show($wo_id)
    {
        if ($wo_id) {
            $workOrder = WorkOrder::find($wo_id);
            $workOrder->load('serviceManuals');

            $services = $workOrder->services()->with('service')->get();
            //dd($workOrder);
            //$service_manuals = $workOrder->serviceManuals()->get();

            return responder()->success($services, function ($query) {
                return [
                    "id"              => (int)$query->id,
                    "work_order_id"   => (int)$query->work_order_id,
                    "service_id"      => $query->service_id,
                    "service"         => [
                        [
                            'id'   => (isset($query->service->id)) ? $query->service->id : null,
                            'text' => (isset($query->service->name)) ? $query->service->name : null,
                        ]
                    ],
                    // "service_manuals" => [
                    //     [
                    //         'id' => (isset($service_manuals->id)) ? $service_manuals->id : null,
                    //         'text' => (isset($service_manuals->name)) ? $service_manuals->name : null,
                    //     ]
                    // ],
                    "fru"             => $query->fru,
                    "rate"            => $query->rate,
                    "service_text"    => (isset($query->service->name)) ? $query->service->name : null,
                    "estimated_price" => $query->estimated_price,
                    "notes"           => $query->notes
                ];
            })->respond();
        }
    }

    public function store(Request $request)
    {
        $services = $request->get('form');

        if (sizeof($services) > 0 && !is_null($request->get('workOrderId'))) {
            WorkOrderService::where('work_order_id', $request->get('workOrderId'))->delete();

            foreach ($services as $service) {
                $price = str_replace('.', '', $service['estimated_price']);

                WorkOrderService::create([
                    'work_order_id'   => $request->get('workOrderId'),
                    'service_id'      => $service['service_id'],
                    'estimated_price' => $price,
                    'rate'            => $service['rate'],
                    'fru'             => $service['fru'],
                    'notes'           => $service['notes']
                ]);
            }

            event(new WorkOrderChangedEvent(WorkOrder::find($request->get('workOrderId'))));

            return responder()->success()->meta([
                'title'   => "Edit Data Jasa & Service",
                'message' => "Data jasa & service untuk WO No. #{$request->get('workOrderId')} berhasil diupdate"
            ])->respond();
        }
    }
}
