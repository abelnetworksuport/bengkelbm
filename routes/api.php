<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::get('vehicle/{id}', function($id) {
    $vehicle = \App\Models\Vehicle::find($id);

    return response()->json(['status' => 'success', 'data' => $vehicle]);
})->name('api.vehicle.show');

Route::get('brands', function(Request $request) {
    $brands = DB::table('car_brands')->select(DB::raw('name as text, id'))
        ->where('name', 'LIKE', "%{$request->get('term')}%")
        ->take(25)
        ->get();

    return response()->json([ 'data' => $brands ]);
});

Route::get('province', function(Request $request) {
    $provinces = DB::table('provinces')->select(DB::raw('name as text, id'))->where('name', 'LIKE', "%{$request->get('term')}%")->get();

    return response()->json([ 'data' => $provinces ]);
});

Route::get('district', function(Request $request) {
    $districts = DB::table('districts')->select(DB::raw('name as text, id'))
        ->where('province_id', $request->get('incl'))
        ->where('name', 'LIKE', "%{$request->get('term')}%")
        ->take(10)
        ->get();

    return response()->json([ 'data' => $districts ]);
});

Route::get('services', function(Request $request) {
    $services = DB::table('services')->select(DB::raw('name as text, id'))
        ->where('name', 'LIKE', "%{$request->get('term')}%")
        ->take(25)
        ->get();

    return response()->json([ 'data' => $services ]);
});

Route::get('spareparts', function(Request $request) {
    $spareparts = \App\Models\WorkOrderParts::where('part_name', 'LIKE', "%{$request->get('term')}%")
                      ->groupBy('part_name')
                      ->take(25)
                      ->get();

    if ($spareparts) {
        return responder()->success($spareparts, function ($parts) {
            return [
                "text" => $parts->part_name
            ];
        })->respond();
    }

//    $spareparts = DB::table('work_order_parts')->select(DB::raw('name as text, id'))
//        ->where('name', 'LIKE', "%{$request->get('term')}%")->get();
//
//    return response()->json([ 'data' => $spareparts ]);
});

Route::get('owners/{id}', function ($id) {
    $owners = \App\Models\Owner::with('province', 'district')->find($id);
    $owners->load('vehicles', 'province', 'district', 'workOrders');

    if ($owners) {
        return response()->json(['status' => 'success', 'data' => $owners ]);
    }
});

Route::get('mechanics', function(Request $request) {
    $mechanics = DB::table('employees')->select(DB::raw('fullname as text, id'))
        ->where('is_mechanic', '1')
        ->where('fullname', 'LIKE', "%{$request->get('term')}%")
        ->take(10)
        ->get();

    return response()->json([ 'data' => $mechanics ]);
});

Route::get('positions', function(Request $request) {
    $positions = DB::table('positions')->select(DB::raw('CONCAT(name, "(", initials, ")") as text, id'))
                   ->where('name', 'LIKE', "%{$request->get('term')}%")->get();

    return response()->json([ 'data' => $positions ]);
});

Route::get('units', function(Request $request) {
    $units = DB::table('quantity_name')->select(DB::raw('name as text, id'))
        ->where('name', 'LIKE', "%{$request->get('term')}%")->get();

    return response()->json([ 'data' => $units ]);
});

Route::get('roles', function (Request $request) {
    $roles = DB::table('roles')->select(DB::raw('name as text, id'))
               ->where('name', 'LIKE', "%{$request->get('term')}%")->get();

    return response()->json(['data' => $roles]);
})->name('api:roles');


Route::get('search', function(Request $request) {
    $search = DB::select(DB::raw("
        SELECT CONCAT(v.plate_number, ' - (Owner: ', o.fullname, ', CHASSIS: ', v.vin_number, ')') as text, v.id 
        FROM vehicles_temp v 
        INNER JOIN vehicle_owners vo ON v.id=vo.vehicle_id
        INNER JOIN owners o ON vo.owner_id=o.id
        WHERE (v.plate_number LIKE :term1 OR o.fullname LIKE :term2 OR v.vin_number LIKE :term3)
        LIMIT 25
    "), ['term1' => '%' . $request->get('term') . '%', 'term2' => '%' . $request->get('term') . '%', 'term3' => $request->get('term') . '%' ]);

    return response()->json([ 'data' => $search ]);
});

Route::get('permissions', function() {
    $permissions = DB::table('permissions')
                ->pluck("name", "name");

    return response()->json([ 'data' => json_encode($permissions) ]);
});

Route::get("vehicle-stats", function() {
    $year = 2017;

    $query = \DB::select(\DB::raw("
            SELECT DATE_FORMAT(registered, '%m') as periode, COUNT(id) as total 
            FROM vehicles_temp 
            WHERE YEAR(registered)=:year 
            GROUP BY DATE_FORMAT(registered, '%Y-%m') 
            ORDER BY registered
        "), ['year' => $year]);

    $months = array_fill(0, 12, 0);

    foreach ($query as $item) {
        $months[(int) $item->periode - 1] = $item->total;
    }

    return response()->json(['name' => "Data Total Kendaraan Tahun {$year}", "data" => $months ]);
});