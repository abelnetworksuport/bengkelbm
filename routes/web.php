<?php

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/dashboard', 'DashboardController@index')->name('dashboard');

Route::post("test", function (\Illuminate\Http\Request $request) {
    $pdf = new \App\Libraries\Reports\WorkOrder\CustomerApproval($request->all());
    $pdf->render();

    return response()->json(['status' => 'success', 'url' => url('download/estimasi-wo.pdf')]);
})->name('estimasi');

Route::post("work-order/revision/print", function (\Illuminate\Http\Request $request) {
    $pdf = new \App\Libraries\Reports\WorkOrder\CustomerApprovalRevisionTemp($request->all());
    $pdf->render();

    return response()->json(['status' => 'success', 'url' => url('download/estimasi-wo-revisi.pdf')]);
})->name('estimasi.revisi');

Route::post("work-order/problems/print", function (\Illuminate\Http\Request $request) {
    $pdf = new \App\Libraries\Reports\WorkOrder\SPKTemp($request->all());
    $pdf->render();

    return response()->json(['status' => 'success', 'url' => url('download/surat-keluhan-kendaraan.pdf')]);
})->name('estimasi.revisi');

Route::get('settings/users/data', "Settings\UserController@data")->name("users.data");
Route::patch('settings/users/reset', "Settings\UserController@resetPassword")->name("users.reset");
Route::resource('settings/users', "Settings\UserController")->only('index', 'show', 'edit', 'update', 'store', 'destroy');

Route::get('settings/roles/data', "Settings\RoleController@data")->name("roles.data");
Route::get('settings/roles/{id}/permissions', "Settings\RoleController@permissions")->name('roles.permission');
Route::post('settings/roles/{id}/permissions', "Settings\RoleController@storePermissions")->name('roles.permission:store');
Route::resource('settings/roles', "Settings\RoleController")->only('index', 'show', 'edit', 'update', 'store', 'destroy');

Route::get('settings/modules/data', "Settings\ModuleController@data")->name('api:modules');
Route::get('settings/modules/lists', "Settings\ModuleController@lists")->name('modules.lists');
Route::post('settings/modules/update', "Settings\ModuleController@update")->name('modules.update');
Route::resource('settings/modules', "Settings\ModuleController")->only(['index', 'store', 'show']);

Route::get('settings/permissions/data', "Settings\PermissionController@data")->name('api:permissions');
Route::get('settings/permissions/lists', "Settings\PermissionController@lists")->name('permissions.lists');
Route::post('settings/permissions/update', "Settings\PermissionController@update")->name('permissions.update');
Route::resource('settings/permissions', "Settings\PermissionController")->only(['index', 'store', 'show']);

Route::get("vehicles", "VehicleController@index");
Route::get("vehicles/autocomplete", "VehicleController@autocomplete")->name('vehicle.autocomplete');
Route::get("vehicles/data", "VehicleController@data")->name('vehicles.data');
Route::get("vehicles/show/{id}", "VehicleController@show")->name('vehicles.show');
Route::get("vehicles/add/{id}", "VehicleController@add")->name('vehicles.add');
Route::get("vehicles/history/{id}", "VehicleController@history")->name('vehicles.history');
Route::post("vehicle/store", "VehicleController@store")->name('vehicles.store');
Route::post("vehicles/update", "VehicleController@update")->name('vehicles.update');
Route::get("vehicles/export", "VehicleController@export")->name('vehicles.export');

Route::get("owners", "OwnerController@index");
Route::get("owners/autocomplete", "OwnerController@autocomplete")->name('owners.autocomplete');
Route::get("owners/data", "OwnerController@data")->name('owners.data');
Route::get("owners/show/{id}", "OwnerController@show")->name('owners.show');
Route::get("owners/{id}/edit", "OwnerController@edit")->name('owners.edit');
Route::get("owners/add", "OwnerController@add")->name('owners.add');
Route::post("owners/store", "OwnerController@store")->name('owners.store');
Route::patch("owners/{id}", "OwnerController@update")->name('owners.update');
Route::post("owners/{id}/vehicle", "OwnerController@vehicle")->name('owners.vehicle');
Route::get("owners/export", "OwnerController@export")->name('owners.export');

Route::get("owners/vehicles/{id}", "OwnerController@vehicles")->name('owners.vehicles');

Route::get("work-order", "WorkOrderController@index")->name('work-order.index');
Route::get("work-order/show/{id}", "WorkOrderController@show")->name('work-order.show');
Route::get("work-order/data/{status?}", "WorkOrderController@data")->name('work-order.data');
Route::get("work-order/add", "WorkOrderController@add")->name('work-order.add');
Route::post("work-order/store", "WorkOrderController@store")->name('work-order.store');
Route::post("work-order/inspection", "WorkOrderController@inspection")->name('work-order.inspection');


Route::get("work-order/problems/{id}", "WorkOrderProblemController@show")->name('work-order::problems.index');
Route::get("work-order/services/{id}", "WorkOrderServiceController@show")->name('work-order::services.index');
Route::get("work-order/spareparts/{id}", "WorkOrderPartsController@show")->name('work-order::parts.index');
Route::get("work-order/mechanics/{id}", "WorkOrderMechanicController@show")->name('work-order::mechanics.index');

Route::patch('work-order/estimated-time', "WorkOrderController@updateEstimatedTime")->name("work-order:estimated");
Route::post("work-order/problems", "WorkOrderProblemController@store")->name('work-order::problems');
Route::post("work-order/services", "WorkOrderServiceController@store")->name('work-order::services');
Route::post("work-order/spareparts", "WorkOrderPartsController@store")->name('work-order::parts');
Route::post("work-order/mechanics", "WorkOrderMechanicController@store")->name('work-order::mechanics');
Route::post("work-order/status", "WorkOrderStatusController@store")->name('work-order::status');
Route::get("work-order/print/{id}", "WorkOrderController@print")->name('work-order::print');
Route::get("work-order/report/{id}", "WorkOrderController@report")->name('work-order::report');
Route::get("work-order/spk/{id}", "WorkOrderController@spk")->name('work-order::spk');
Route::get("work-order/revision/{id}", "WorkOrderController@revision")->name('work-order::revision');
Route::post('work-order/revision/{id}', "WorkOrderController@revisionStore")->name('work-order::revision.store');
Route::post("work-order/spktemp", "WorkOrderController@spkTemp")->name('work-order::spk-temp');

Route::get("car-return/data", "CarReturnController@datatable")->name('car-return.data');
Route::resource('car-return', 'CarReturnController')->only(['create', 'index', 'store']);

Route::get("work-order-temp/data", "WorkOrderTempController@datatable")->name('work-order-temp.data');
Route::resource('work-order-temp', 'WorkOrderTempController')->only(['create', 'index', 'store']);


Route::get("employees/data", "EmployeeController@data")->name('employees.data');
Route::get("employees/export", "EmployeeController@export")->name('employees.export');
Route::resource("employees", "EmployeeController");


Route::group(['namespace' => 'Master', 'prefix' => 'master'], function () {
    Route::get("services/data", "ServiceController@data")->name('service.data');
    Route::get("services/export", "ServiceController@export")->name('service.export');
    Route::resource("services", "ServiceController");

    Route::get("positions/data", "PositionController@data")->name('position.data');
    Route::get("positions/export", "PositionController@export")->name('position.export');
    Route::resource("positions", "PositionController");

    Route::get("statuses/data", "StatusController@data")->name('status.data');
    Route::get("statuses/export", "StatusController@export")->name('status.export');
    Route::resource("statuses", "StatusController");
});

Route::group(['namespace' => 'Reports', 'prefix' => 'report'], function () {
    Route::get("work-order", "WorkOrderReportController@index")->name('report::wo.data');
});

Route::get("testing", function () {
    $workOrder = \App\Models\WorkOrder::with('vehicle', 'owner', 'services.service', 'problems', 'serviceAdvisor')->find(32);

    $data = [
        "code"            => $workOrder->code,
        "created_at"      => $workOrder->created_at,
        "owner_name"      => $workOrder->owner_name,
        "vin_number"      => $workOrder->vehicle->vin_number,
        "engine_number"   => $workOrder->vehicle->engine_number,
        "plate_number"    => $workOrder->vehicle->plate_number,
        "address"         => substr($workOrder->owner->address, 0, 23),
        "colour"          => $workOrder->vehicle->colour,
        "phone_number"    => $workOrder->owner->phone_number,
        "driver"          => $workOrder->driver,
        "services"        => $workOrder->services,
        "problems"        => $workOrder->problems,
        "millage"         => $workOrder->vehicle->millage,
        "service_advisor" => $workOrder->serviceAdvisor->fullname
    ];


    $pdf = new \App\Libraries\Reports\WorkOrder\SPK($data);
    $pdf->render();
});

Route::get('download/{filename}', function ($filename)
{
    echo $filename;
    $path = storage_path("app/public/files/{$filename}");

    if (!File::exists($path)) {
        abort(404);
    }

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
});

Route::get("t1", function () {
    $serviceAdvisor = auth()->user()->hasRole('Service Advisor (SA)');

    dd($serviceAdvisor);
});

Route::get('t2', function (\Spatie\Permission\Models\Role $role) {
    $data = $role->find(2);

    return str_slug($data->name);
});

Route::get('t3', function () {
    event(new App\Events\StatusUpdated("superadmin", "tester", ['title' => "Changed Status", "type" => "warning", "text" => "ini pesanya"]));
    return "Event has been sent!";

//    $options = array(
//        'cluster' => 'ap1',
//        'useTLS' => true
//    );
//    $pusher = new Pusher\Pusher(
//        '53ef5d03ae6029d9c955',
//        'c9c9647308e1b940f750',
//        '702736',
//        $options
//    );
//
//    $data['message'] = 'hello world';
//    $pusher->trigger('pub-channel', 'changed-status', $data);
});

Route::resource('wo', 'WorkOrdersController');
