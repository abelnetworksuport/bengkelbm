<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('work_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code');
            $table->unsignedInteger('vehicle_id');
            $table->unsignedInteger('owner_id');
            $table->unsignedInteger('service_advisor_id');
            $table->string('driver');
            $table->unsignedInteger('driver_salutation');
            $table->string('latest_millage');
            $table->float('service_price');
            $table->float('part_price');
            $table->integer('minutes_estimasted');
            $table->integer('minutes_finished')->nullable();
            $table->string('car_return_from')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('work_orders');
    }
}
