<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOwnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('owners', function (Blueprint $table) {
            $table->increments('id');
            $table->string('customer_code')->nullable();
            $table->string('fullname');
            $table->string('owner_group')->nullable();
            $table->enum('use_insurance', ['1', '0'])->default('0');
            $table->string('insurance_name')->nullable();
            $table->unsignedInteger('owner_type')->nullable();
            $table->unsignedInteger('salutation_id')->nullable();
            $table->unsignedInteger('gender_id')->nullable();
            $table->string('address');
            $table->unsignedInteger('province_id')->nullable();
            $table->unsignedInteger('district_id')->nullable();
            $table->integer('zip_code')->nullable();
            $table->string('phone_number')->nullable();
            $table->string('mobile_phone')->nullable();
            $table->string('email')->nullable();
            $table->date("registered")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('owners');
    }
}
