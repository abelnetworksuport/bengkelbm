<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarReturnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('car_returns', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('new_work_order_id');
            $table->unsignedInteger('old_work_order_id');
            $table->unsignedInteger('issued_by');
            $table->string('notes');
            $table->date('issued_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('car_returns');
    }
}
