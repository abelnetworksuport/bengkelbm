<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('vin_number');
            $table->string('plate_number');
            $table->string('engine_number');
            $table->unsignedInteger('brand_id');
            $table->string('brand_series')->nullable();
            $table->integer('build_year')->nullable();
            $table->string('colour')->nullable();
            $table->integer('millage')->nullable();
            $table->unsignedInteger('transmision_id')->nullable();
            $table->unsignedInteger('fuel_id')->nullable();
            $table->date('last_service')->nullable();
            $table->date('registered')->nullable();
            $table->string('notes')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicles');
    }
}
