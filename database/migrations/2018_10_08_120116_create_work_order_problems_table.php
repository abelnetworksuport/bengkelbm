<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkOrderProblemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('work_order_problems', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->unsignedInteger('work_order_id');
            $table->integer('working_estimated')->nullable();
            $table->integer('working_finished')->nullable();
            $table->string('solution')->nullable();
            $table->string('notes')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('work_order_problems');
    }
}
