<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code')->unique();
            $table->string('fullname');
            $table->unsignedInteger('salutation_id');
            $table->string('identity_card_number')->nullable();
            $table->date('birthday');
            $table->string('initials');
            $table->string('address');
            $table->string('email')->nullable();
            $table->string('phone_number');
            $table->unsignedInteger('position_id');
            $table->date('start_working')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
