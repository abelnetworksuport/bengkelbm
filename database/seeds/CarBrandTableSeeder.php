<?php

use Illuminate\Database\Seeder;

class CarBrandTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->cars() as $car) {
            DB::table('car_brands')->insert([
                'name' => trim($car)
            ]);
        }
    }

    protected function cars()
    {
        $brands = "Abarth | Acura | Alfa Romeo | AMC | Aston Martin | Autobianchi | Audi | Austin | Bentley | BAW | BMW | Bugatti | Buick | BYD | Cadillac | Caterham | Chang'an | Chery | Chevrolet | Cizeta | Chrysler | Citroën | DAF | Daihatsu | Daimler AG | GM Daewoo | DeLorean Motor | De Tomaso | Dodge | Dongfeng | DeSoto | Datsun | Dacia | DFSK | Esemka | ERF | Elvi | FAW | Ferrari | Fiat | Fisker Automotive | Ford | Foton | Fargo | Foday | GAZ | Geely | General Motors | Great Wall | Grup Volkswagen | GMC | GEA | Hindustan | Hino | Holden | Honda | Hummer | Hyundai | Hennessey | Infiniti | Isuzu | Iveco | Izhmash | Inokom | Jaguar | Jeep | Kia Motors | Kamaz | Koenigsegg | Kancil | Lada | Lamborghini | Lancia | Land Rover | Lexus | Lincoln | Lotus | Maruti Suzuki | Mahindra & Mahindra Limited | Maserati | Maybach | Mazda | Mercedes-Benz | Mercury | MG | Mini | Mitsubishi | Morgan | Morris | Marmon | Marussia Motors | Nash | Nissan | Noble | NSU | Nissan Diesel | Oldsmobile | Opel | OSCA | Paccar | Panoz | Pagani | Panoz | Peterbilt | Peugeot | Plymouth | Pontiac | Porsche | Premier | Proto | Proton | Puch | Perodua | Ram | Renault | Renault-Samsung | Rolls-Royce | Rover | RUF | Saab | Saehan | Saleen | Saturn | Sbarro | Scania AB | Scion | SEAT | Shelby | Škoda | Smart | Spyker | SsangYong | Steyr | Subaru | Suzuki | Standard Motor | SRT | Tata | Tesla | Timor | Texmaco | Toyota | TVR | UAZ | UD Trucks | Vauxhall | Vector | Volkswagen | Volvo | Westfield | Wuling Motors | Yamaha | Zagato | Zastava | ZiL | Zenvo";

        return explode('|', $brands);
    }
}
