<?php

use Illuminate\Database\Seeder;

class ServiceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->lists() as $name => $price)
        {
            DB::table('services')->insert([
                'name' => $name,
                'estimated_price' => $price
            ]);
        }
    }

    protected function lists()
    {
        return [
            'Maintenance Service' => 1120000,
            'Ganti break disk (Front L/R)' => 720000,
            'Ganti break pad (Front L/R)' => 750000,
            'Ganti Oli power steering' => 160000,
            'Ganti Oli Mesin' => 100000,
            'Ganti Filter Oli' => 75000,
            'Ganti Filter Fuel' => 75000
        ];
    }
}
