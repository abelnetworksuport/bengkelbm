<?php

use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->roles() as $role)
        {
            DB::table('roles')->insert([
                'name' => $role['name'],
                'slug' => $role['slug'],
                'permissions' => json_encode($role['permissions'])
            ]);
        }
    }

    private function roles()
    {
        return [
            [
                'name' => 'Superuser',
                'slug' => 'superuser',
                'permissions' => [
                    '*' => true
                ]
            ],
            [
                'name' => 'Service Advisor (SA)',
                'slug' => 'service-advisor',
                'permissions' => [
                    'sa-*' => true
                ]
            ],
            [
                'name' => 'Kepala Bengkel',
                'slug' => 'kepala-bengkel',
                'permissions' => [
                    'kb-*' => true
                ]
            ]
        ];
    }
}
