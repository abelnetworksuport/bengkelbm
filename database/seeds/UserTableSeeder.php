<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $superuser = \App\Models\User::create([
            'name' => 'Mr. Robot',
            'username' => 'toor',
            'email' => 'me@sugeng.me',
            'password' => bcrypt('secret')
        ]);

        $superuser->roles()->attach(1);
    }
}
