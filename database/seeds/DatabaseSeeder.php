<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RoleTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(MasterTableSeeder::class);
        $this->call(ProvinceTableSeeder::class);
        $this->call(CarBrandTableSeeder::class);
        $this->call(ServiceTableSeeder::class);
    }
}
