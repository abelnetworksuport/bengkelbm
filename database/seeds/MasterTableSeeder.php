<?php

use Illuminate\Database\Seeder;

class MasterTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $masters = $this->data();

        $counter = 0;
        foreach ($masters as $group => $master) {
            $counter++;
            foreach ($master as $code => $value) {
                DB::table('masters')->insert([
                    'group_code' => $this->formatGroupCode($counter),
                    'group_name' => strtoupper($group),
                    'code' => $code,
                    'name' => $value
                ]);
            }
        }
    }

    private function formatGroupCode($counter) {
        return str_pad($counter, 3, '0', STR_PAD_LEFT);
    }


    private function data()
    {
        $masters = [];
        $masters['gender'] = [
            'P' => 'Pria',
            'W' => 'Wanita'
        ];

        $masters['salutation'] = [
            'P' => 'Tuan/Bapak',
            'W' => 'Nyonya/Ibu/Nona',
        ];

        $masters['transmission'] = [
            'A' => 'Automatic',
            'M' => 'Manual',
            'H' => 'Hybrid'
        ];

        $masters['fuel'] = [
            'B' => 'BENSIN',
            'G' => 'GAS',
            'S' => 'SOLAR',
            'L' => 'LISTRIK',
            'H' => 'HYBRID'
        ];

        return $masters;
    }
}
