<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * App\Models\CarReturn
 *
 * @property int $id
 * @property int $new_work_order_id
 * @property int $old_work_order_id
 * @property int $issued_by
 * @property string $description
 * @property string $issued_date
 * @property string $created_at
 * @property string $updated_at
 * @property string|null $type
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarReturn newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarReturn newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarReturn query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarReturn whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarReturn whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarReturn whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarReturn whereIssuedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarReturn whereIssuedDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarReturn whereNewWorkOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarReturn whereOldWorkOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarReturn whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarReturn whereUpdatedAt($value)
 */
	class CarReturn extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Sparepart
 *
 * @property int $id
 * @property string $name
 * @property float $price
 * @property string $active
 * @property string $created_at
 * @property string $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sparepart newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sparepart newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sparepart query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sparepart whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sparepart whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sparepart whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sparepart whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sparepart wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sparepart whereUpdatedAt($value)
 */
	class Sparepart extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Owner
 *
 * @property int $id
 * @property string $customer_code
 * @property string $fullname
 * @property int $salutation_id
 * @property int $gender_id
 * @property string $address
 * @property int $province_id
 * @property int $district_id
 * @property int $zip_code
 * @property string $phone_number
 * @property string $mobile_phone
 * @property string $email
 * @property string $created_at
 * @property string $updated_at
 * @property string|null $owner_group
 * @property string $use_insurance
 * @property string|null $insurance_name
 * @property int|null $owner_type
 * @property \Illuminate\Support\Carbon|null $registered
 * @property-read \App\Models\District|null $district
 * @property-read \App\Models\Master|null $gender
 * @property-read \App\Models\Province|null $province
 * @property-read \App\Models\Master|null $salutation
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Vehicle[] $vehicles
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\WorkOrder[] $workOrders
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Owner newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Owner newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Owner query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Owner whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Owner whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Owner whereCustomerCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Owner whereDistrictId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Owner whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Owner whereFullname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Owner whereGenderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Owner whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Owner whereInsuranceName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Owner whereMobilePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Owner whereOwnerGroup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Owner whereOwnerType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Owner wherePhoneNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Owner whereProvinceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Owner whereRegistered($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Owner whereSalutationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Owner whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Owner whereUseInsurance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Owner whereZipCode($value)
 */
	class Owner extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\WorkOrderServiceManualTemp
 *
 * @property int $id
 * @property int $work_order_id
 * @property int $name
 * @property float $estimated_price
 * @property string $notes
 * @property string $created_at
 * @property string $updated_at
 * @property string $created_by
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderServiceManualTemp newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderServiceManualTemp newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderServiceManualTemp query()
 */
	class WorkOrderServiceManualTemp extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\WorkOrderServiceTemp
 *
 * @property int $id
 * @property int $work_order_id
 * @property int $service_id
 * @property float $estimated_price
 * @property float $actual_price
 * @property string $assigned_mechanic
 * @property int $status
 * @property string $notes
 * @property string $created_at
 * @property string $updated_at
 * @property-read \App\Models\Service $service
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderServiceTemp newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderServiceTemp newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderServiceTemp query()
 */
	class WorkOrderServiceTemp extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Position
 *
 * @property int $id
 * @property string $name
 * @property string $initials
 * @property string $active
 * @property string $created_at
 * @property string $updated_at
 * @property string|null $colour
 * @property-read \App\Models\Employee $employees
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Position newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Position newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Position query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Position whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Position whereColour($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Position whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Position whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Position whereInitials($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Position whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Position whereUpdatedAt($value)
 */
	class Position extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Service
 *
 * @property int $id
 * @property string $name
 * @property float $weight
 * @property int $estimated_price
 * @property int $user_created
 * @property int $user_updated
 * @property string $created_at
 * @property string $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Service newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Service newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Service query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Service whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Service whereEstimatedPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Service whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Service whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Service whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Service whereUserCreated($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Service whereUserUpdated($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Service whereWeight($value)
 */
	class Service extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Master
 *
 * @property int $id
 * @property string $group_code
 * @property string $group_name
 * @property string $code
 * @property string $name
 * @property string $created_at
 * @property string $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Master newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Master newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Master query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Master whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Master whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Master whereGroupCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Master whereGroupName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Master whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Master whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Master whereUpdatedAt($value)
 */
	class Master extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\WorkOrderMechanic
 *
 * @property int $id
 * @property int $work_order_id
 * @property int $employee_id
 * @property int $working_minutes
 * @property string $created_at
 * @property string $updated_at
 * @property int $mechanic_id
 * @property string|null $notes
 * @property-read \App\Models\Employee $mechanicName
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderMechanic newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderMechanic newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderMechanic query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderMechanic whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderMechanic whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderMechanic whereMechanicId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderMechanic whereNotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderMechanic whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderMechanic whereWorkOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderMechanic whereWorkingMinutes($value)
 */
	class WorkOrderMechanic extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Province
 *
 * @property string $id
 * @property string $name
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Province[] $districts
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Province newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Province newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Province query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Province whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Province whereName($value)
 */
	class Province extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\User
 *
 * @property int $id
 * @property string $uuid
 * @property string $name
 * @property string $username
 * @property string $email
 * @property string|null $email_verified_at
 * @property string $password
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Permission[] $permissions
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Role[] $roles
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User byUUID($uuid)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User permission($permissions)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User role($roles, $guard = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUsername($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUuid($value)
 */
	class User extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Role
 *
 * @property int $id
 * @property string $name
 * @property string $guard_name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $users
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Role newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Role newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Role query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Role whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Role whereGuardName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Role whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Role whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Role whereUpdatedAt($value)
 */
	class Role extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\WorkOrderService
 *
 * @property int $id
 * @property int $work_order_id
 * @property int $service_id
 * @property float $estimated_price
 * @property float $actual_price
 * @property string $assigned_mechanic
 * @property int $status
 * @property string $notes
 * @property string $created_at
 * @property string $updated_at
 * @property string|null $name
 * @property float|null $discount_flat
 * @property int|null $discount_percentage
 * @property-read \App\Models\Service $service
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderService newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderService newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderService query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderService whereActualPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderService whereAssignedMechanic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderService whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderService whereDiscountFlat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderService whereDiscountPercentage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderService whereEstimatedPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderService whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderService whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderService whereNotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderService whereServiceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderService whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderService whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderService whereWorkOrderId($value)
 */
	class WorkOrderService extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\WorkOrderProblemTemp
 *
 * @property int $id
 * @property string $name
 * @property int $work_order_id
 * @property int $working_estimated
 * @property int $working_finished
 * @property string $solution
 * @property string $description
 * @property string $created_at
 * @property string $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderProblemTemp newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderProblemTemp newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderProblemTemp query()
 */
	class WorkOrderProblemTemp extends \Eloquent {}
}

namespace App\Models{
/**
 * Created By: Sugeng
 * Date: 07/12/18
 * Time: 10.40
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Module newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Module newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Module query()
 */
	class Module extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Status
 *
 * @property int $id
 * @property string $code
 * @property string $name
 * @property string $condition
 * @property string $active
 * @property string $created_at
 * @property string $updated_at
 * @property string|null $colour
 * @property string|null $icon
 * @property string|null $roles
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Status newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Status newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Status query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Status whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Status whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Status whereColour($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Status whereCondition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Status whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Status whereIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Status whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Status whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Status whereRoles($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Status whereUpdatedAt($value)
 */
	class Status extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Employee
 *
 * @property int $id
 * @property string $code
 * @property string $fullname
 * @property int $salutation_id
 * @property string $identity_card_number
 * @property string $birthday
 * @property string $initials
 * @property string $address
 * @property string $email
 * @property string $phone_number
 * @property int $position_id
 * @property string $start_working
 * @property string $created_at
 * @property string $updated_at
 * @property string $is_mechanic
 * @property-read \App\Models\Master $gender
 * @property-read \App\Models\Position $position
 * @property-read \App\Models\Master $salutation
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereBirthday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereFullname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereIdentityCardNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereInitials($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereIsMechanic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee wherePhoneNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee wherePositionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereSalutationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereStartWorking($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereUpdatedAt($value)
 */
	class Employee extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\WorkOrderParts
 *
 * @property int $id
 * @property int $work_order_id
 * @property int $sparepart_id
 * @property float $estimated_price
 * @property float $actual_price
 * @property int $quantities
 * @property string $qty_label
 * @property int $service_status
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 * @property string|null $part_name
 * @property float|null $discount_flat
 * @property int|null $discount_percentage
 * @property int|null $quantity_id
 * @property-read \App\Models\Sparepart $sparepart
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderParts newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderParts newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderParts query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderParts whereActualPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderParts whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderParts whereDiscountFlat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderParts whereDiscountPercentage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderParts whereEstimatedPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderParts whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderParts wherePartName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderParts whereQtyLabel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderParts whereQuantities($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderParts whereQuantityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderParts whereServiceStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderParts whereSparepartId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderParts whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderParts whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderParts whereWorkOrderId($value)
 */
	class WorkOrderParts extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\WorkOrder
 *
 * @property int $id
 * @property string $code
 * @property int $vehicle_id
 * @property int $owner_id
 * @property int $service_advisor_id
 * @property string $driver
 * @property int $driver_salutation
 * @property string $latest_millage
 * @property float $service_price
 * @property float $part_price
 * @property int $minutes_estimasted
 * @property int $minutes_finished
 * @property string $car_return_from
 * @property string $created_at
 * @property string $updated_at
 * @property int revision
 * @property string|null $plate_number
 * @property string|null $owner_name
 * @property int|null $status_id
 * @property string|null $driver_phone
 * @property float|null $service_discount_flat
 * @property int|null $service_discount_percentage
 * @property float|null $service_discount
 * @property float|null $part_discount
 * @property float|null $part_discount_flat
 * @property int|null $part_discount_percentage
 * @property int $minutes_estimated
 * @property int|null $revision
 * @property string|null $cr_code
 * @property string|null $cr_type
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\WorkOrderMechanic[] $mechanics
 * @property-read \App\Models\Owner $owner
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\WorkOrderParts[] $parts
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\WorkOrderProblem[] $problems
 * @property-read \App\Models\User $serviceAdvisor
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\WorkOrderServiceManual[] $serviceManuals
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\WorkOrderService[] $services
 * @property-read \App\Models\Status|null $status
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\WorkOrderStatus[] $statusLogs
 * @property-read \App\Models\Vehicle $vehicle
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrder newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrder newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrder query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrder whereCarReturnFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrder whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrder whereCrCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrder whereCrType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrder whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrder whereDriver($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrder whereDriverPhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrder whereDriverSalutation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrder whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrder whereLatestMillage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrder whereMinutesEstimated($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrder whereMinutesFinished($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrder whereOwnerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrder whereOwnerName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrder wherePartDiscount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrder wherePartDiscountFlat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrder wherePartDiscountPercentage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrder wherePartPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrder wherePlateNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrder whereRevision($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrder whereServiceAdvisorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrder whereServiceDiscount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrder whereServiceDiscountFlat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrder whereServiceDiscountPercentage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrder whereServicePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrder whereStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrder whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrder whereVehicleId($value)
 */
	class WorkOrder extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\WorkOrderServiceManual
 *
 * @property int $id
 * @property int $work_order_id
 * @property int $name
 * @property float $estimated_price
 * @property string $notes
 * @property string $created_at
 * @property string $updated_at
 * @property string $created_by
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderServiceManual newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderServiceManual newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderServiceManual query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderServiceManual whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderServiceManual whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderServiceManual whereEstimatedPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderServiceManual whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderServiceManual whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderServiceManual whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderServiceManual whereWorkOrderId($value)
 */
	class WorkOrderServiceManual extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\WorkOrderPartsTemp
 *
 * @property int $id
 * @property int $work_order_id
 * @property int $sparepart_id
 * @property float $estimated_price
 * @property float $actual_price
 * @property int $quantities
 * @property string $qty_label
 * @property int $service_status
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 * @property int $work_order_temp_id
 * @property string|null $part_name
 * @property float|null $discount_flat
 * @property int|null $discount_percentage
 * @property int|null $quantity_id
 * @property-read \App\Models\Sparepart $sparepart
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderPartsTemp newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderPartsTemp newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderPartsTemp query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderPartsTemp whereActualPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderPartsTemp whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderPartsTemp whereDiscountFlat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderPartsTemp whereDiscountPercentage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderPartsTemp whereEstimatedPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderPartsTemp whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderPartsTemp wherePartName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderPartsTemp whereQtyLabel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderPartsTemp whereQuantities($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderPartsTemp whereQuantityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderPartsTemp whereServiceStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderPartsTemp whereSparepartId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderPartsTemp whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderPartsTemp whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderPartsTemp whereWorkOrderTempId($value)
 */
	class WorkOrderPartsTemp extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\WorkOrderTemp
 *
 * @property int $id
 * @property string $code
 * @property int $vehicle_id
 * @property int $owner_id
 * @property int $service_advisor_id
 * @property string $driver
 * @property int $driver_salutation
 * @property string $latest_millage
 * @property float $service_price
 * @property float $part_price
 * @property int $minutes_estimasted
 * @property int $minutes_finished
 * @property string $car_return_from
 * @property string $created_at
 * @property string $updated_at
 * @property int revision
 * @property string|null $plate_number
 * @property string|null $owner_name
 * @property int|null $status_id
 * @property string|null $driver_phone
 * @property float|null $service_discount_flat
 * @property int|null $service_discount_percentage
 * @property float|null $service_discount
 * @property float|null $part_discount
 * @property float|null $part_discount_flat
 * @property int|null $part_discount_percentage
 * @property array|null $problems
 * @property array|null $services
 * @property array|null $spareparts
 * @property string $status
 * @property-read \App\Models\Owner $owner
 * @property-read \App\Models\User $serviceAdvisor
 * @property-read \App\Models\Vehicle $vehicle
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderTemp newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderTemp newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderTemp query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderTemp whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderTemp whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderTemp whereDriver($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderTemp whereDriverPhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderTemp whereDriverSalutation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderTemp whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderTemp whereLatestMillage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderTemp whereOwnerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderTemp whereOwnerName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderTemp wherePartDiscount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderTemp wherePartDiscountFlat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderTemp wherePartDiscountPercentage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderTemp wherePartPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderTemp wherePlateNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderTemp whereProblems($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderTemp whereServiceAdvisorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderTemp whereServiceDiscount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderTemp whereServiceDiscountFlat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderTemp whereServiceDiscountPercentage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderTemp whereServicePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderTemp whereServices($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderTemp whereSpareparts($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderTemp whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderTemp whereStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderTemp whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderTemp whereVehicleId($value)
 */
	class WorkOrderTemp extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\WorkOrderStatus
 *
 * @property int $id
 * @property int $work_order_id
 * @property string $timestamp
 * @property int $status
 * @property int $changed_by
 * @property string $notes
 * @property string $created_at
 * @property string $updated_at
 * @property-read \App\Models\Status $statusName
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderStatus newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderStatus newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderStatus query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderStatus whereChangedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderStatus whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderStatus whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderStatus whereNotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderStatus whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderStatus whereTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderStatus whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderStatus whereWorkOrderId($value)
 */
	class WorkOrderStatus extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\CarBrand
 *
 * @property int $id
 * @property string $name
 * @property string $build_country
 * @property string $created_at
 * @property string $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarBrand newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarBrand newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarBrand query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarBrand whereBuildCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarBrand whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarBrand whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarBrand whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarBrand whereUpdatedAt($value)
 */
	class CarBrand extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\WorkOrderProblem
 *
 * @property int $id
 * @property string $name
 * @property int $work_order_id
 * @property int $working_estimated
 * @property int $working_finished
 * @property string $solution
 * @property string $description
 * @property string $created_at
 * @property string $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderProblem newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderProblem newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderProblem query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderProblem whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderProblem whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderProblem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderProblem whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderProblem whereSolution($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderProblem whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderProblem whereWorkOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderProblem whereWorkingEstimated($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkOrderProblem whereWorkingFinished($value)
 */
	class WorkOrderProblem extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Vehicle
 *
 * @property int $id
 * @property string $vin_number
 * @property string $plate_number
 * @property string $engine_number
 * @property int $brand_id
 * @property string $brand_series
 * @property int $build_year
 * @property string $colour
 * @property int $millage
 * @property int $transmision_id
 * @property int $fuel_id
 * @property string $last_service
 * @property string $registered
 * @property string $notes
 * @property string $created_at
 * @property string $updated_at
 * @property int|null $car_type_id
 * @property string|null $customer_name
 * @property string|null $customer_group
 * @property string|null $insurance_name
 * @property-read \App\Models\CarBrand|null $brand
 * @property-read \App\Models\CarBrand $brands
 * @property-read \App\Models\Master|null $carType
 * @property-read \App\Models\Master|null $fuel
 * @property-read mixed $register_date
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Owner[] $owners
 * @property-read \App\Models\Master|null $transmision
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\WorkOrder[] $workOrders
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vehicle newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vehicle newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vehicle query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vehicle whereBrandId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vehicle whereBrandSeries($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vehicle whereBuildYear($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vehicle whereCarTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vehicle whereColour($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vehicle whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vehicle whereCustomerGroup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vehicle whereCustomerName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vehicle whereEngineNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vehicle whereFuelId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vehicle whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vehicle whereInsuranceName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vehicle whereLastService($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vehicle whereMillage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vehicle whereNotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vehicle wherePlateNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vehicle whereRegistered($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vehicle whereTransmisionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vehicle whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vehicle whereVinNumber($value)
 */
	class Vehicle extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\District
 *
 * @property int $id
 * @property int $province_id
 * @property string $name
 * @property-read \App\Models\Province|null $province
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\District newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\District newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\District query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\District whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\District whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\District whereProvinceId($value)
 */
	class District extends \Eloquent {}
}

